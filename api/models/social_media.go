package models

type ApplicantSocialMedia struct {
	ID          int64  `json:"id"`
	ApplicantID int64  `json:"applicant_id"`
	Name        string `json:"name"`
	Url         string `json:"url"`
}

type CreateApplicantSocialMedia struct {
	Name string `json:"name" binding:"required"`
	Url  string `json:"url" binding:"required"`
}

type ApplicantSocialMedias struct {
	SocialMedias []*CreateApplicantSocialMedia `json:"social_medias" binding:"required"`
}

type GetAllApplicantSocialMedias struct {
	SocialMedias []*ApplicantSocialMedia `json:"social_medias"`
	Count        int64                   `json:"count"`
}

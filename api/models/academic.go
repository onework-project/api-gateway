package models

type CreateOrUpdateApplicantAcademic struct {
	TestName     string  `json:"test_name" binding:"required,min=2"`
	Organization string  `json:"organization" binding:"required,min=2"`
	Score        float64 `json:"score" binding:"required"`
	PdfUrl       string  `json:"pdf_url"`
}

type ApplicantAcademic struct {
	ID           int64   `json:"id"`
	ApplicantID  int64   `json:"applicant_id"`
	TestName     string  `json:"test_name"`
	Organization string  `json:"organization"`
	Score        float64 `json:"score"`
	PdfUrl       *string  `json:"pdf_url"`
}

type GetAllApplicantsAcademics struct {
	Academics []*ApplicantAcademic `json:"academics"`
	Count     int64                `json:"count"`
}

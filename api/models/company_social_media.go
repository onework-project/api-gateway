package models

type CompanySocialMedia struct {
	ID        int64  `json:"id"`
	CompanyID int64  `json:"company_id"`
	Name      string `json:"name"`
	Url       string `json:"url"`
}

type CreateCompanySocialMedia struct {
	Name string `json:"name" binding:"required"`
	Url  string `json:"url" binding:"required"`
}

type GetAllCompaniesSocialMedias struct {
	SocialMedias []*CompanySocialMedia `json:"social_medias"`
	Count        int64                 `json:"count"`
}

type CompanySocialMedias struct {
	SocialMedias []CreateApplicantSocialMedia `json:"social_medias" binding:"required"`
}

type GetAllCompaniesSm struct {
	Limit     int64 `json:"limit" binding:"required" default:"10"`
	Page      int64 `json:"page" binding:"required" default:"1"`
	CompanyID int64 `json:"company_id"`
}

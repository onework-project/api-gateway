package models

type Step struct {
	ID           int64 `json:"id"`
	ApplicantID  int64 `json:"applicant_id"`
	GeneralInfo  bool  `json:"general_info"`
	Work         bool  `json:"work"`
	Education    bool  `json:"education"`
	AcademicTest bool  `json:"academic_test"`
	Licence      bool  `json:"licences_certificate"`
	Award        bool  `json:"award"`
	Research     bool  `json:"research"`
}

type UpdateApplicantStep struct {
	Field string `json:"field" binding:"required"`
	Step  bool   `json:"step" binding:"required"`
}

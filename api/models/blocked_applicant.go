package models

type BlockedApplicant struct {
	ID            int64     `json:"id"`
	CompanyID     int64     `json:"company_id"`
	ApplicantID   int64     `json:"applicant_id"`
	ApplicantInfo Applicant `json:"applicant_info"`
	CreatedAt     string    `json:"created_at"`
}

type GetAllBlockedApplicantsParams struct {
	Limit     int64 `json:"limit" binding:"required" default:"10"`
	Page      int64 `json:"page" binding:"required" default:"1"`
	CompanyID int64 `json:"company_id"`
}

type GetAllsCompaniesBlockedApplicantsstruct struct {
	BlockedApplicants []*BlockedApplicant `json:"blocked_applicants"`
	Count             int64               `json:"count"`
}

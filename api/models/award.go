package models

type CreateApplicantAward struct {
	AwardName    string `json:"award_name" binding:"required"`
	Organization string `json:"organization" binding:"required"`
	PdfUrl       string `json:"pdf_url"`
}

type ApplicantAward struct {
	ID           int64  `json:"id"`
	ApplicantID  int64  `json:"applicant_id"`
	AwardName    string `json:"award_name"`
	Organization string `json:"organization"`
	PdfUrl       *string `json:"pdf_url"`
}

type GetAllApplicantsAwards struct {
	Awards []*ApplicantAward `json:"awards"`
	Count  int64             `json:"count"`
}

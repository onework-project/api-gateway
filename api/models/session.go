package models

type SessionEmail struct {
	Email string `json:"email" binding:"required"`
}

type SessionUUIDReq struct {
	ID string `json:"id" binding:"required"`
}

type GetAllSessionsParams struct {
	Limit int64  `json:"limit" binding:"required" default:"10"`
	Page  int64  `json:"page" binding:"required" default:"1"`
	Email string `json:"email" binding:"email"`
}

type GetAllSessions struct {
	Sessions []*Session `json:"sessions"`
	Count    int64      `json:"count"`
}

type Session struct {
	ID        string `json:"id"`
	Email     string `json:"email"`
	UserAgent string `json:"user_agent"`
	ClientIP  string `json:"client_ip"`
	IsBlocked bool   `json:"is_blocked"`
	CreatedAt string `json:"created_at"`
}

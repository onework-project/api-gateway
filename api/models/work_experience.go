package models

type CreateOrUpdateApplicantWork struct {
	CompanyName    string `json:"company_name" binding:"required"`
	WorkPosition   string `json:"work_position" binding:"required"`
	CompanyWebsite string `json:"company_website"`
	EmploymentType string `json:"employment_type" binding:"required"`
	StartYear      string `json:"start_year" binding:"required"`
	StartMonth     string `json:"start_month" binding:"required"`
	EndYear        string `json:"end_year"`
	EndMonth       string `json:"end_month"`
	Description    string `json:"description"`
}

type ApplicantWork struct {
	ID             int64   `json:"id"`
	ApplicantID    int64   `json:"applicant_id"`
	CompanyName    string  `json:"company_name"`
	WorkPosition   string  `json:"work_position"`
	CompanyWebsite *string `json:"company_website"`
	EmploymentType string  `json:"employment_type"`
	StartYear      string  `json:"start_year"`
	StartMonth     string  `json:"start_month"`
	EndYear        *string `json:"end_year"`
	EndMonth       *string `json:"end_month"`
	Description    *string `json:"description"`
}

type GetAllApplicantsWorks struct {
	Works []*ApplicantWork `json:"works"`
	Count int64            `json:"count"`
}

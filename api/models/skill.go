package models

type ApplicantSkill struct {
	ID          int64  `json:"id"`
	ApplicantID int64  `json:"applicant_id"`
	Skill       string `json:"skill"`
}

type CreateApplicantSkill struct {
	Skill string `json:"skill" binding:"required"`
}

type ApplicantSkills struct {
	Skills []CreateApplicantSkill `json:"skills" binding:"required"`
}

type GetAllApplicantsSkills struct {
	Skills []*ApplicantSkill `json:"skills"`
	Count  int64             `json:"count"`
}

package models

type VacancyLanguage struct {
	ID        int64  `json:"id"`
	VacancyID int64  `json:"vacancy_id"`
	Language  string `json:"language"`
	Level     string `json:"level"`
}

type CreateOrUpdateVacancyLanguage struct {
	Language string `json:"language" binding:"required"`
	Level    string `json:"level" binding:"required"`
}

type GetAllCompaniesLanguages struct {
	Languages []*VacancyLanguage `json:"languages"`
	Count     int64              `json:"count"`
}

type GetAllVacanciesParams struct {
	Limit     int64 `json:"limit" binding:"required" default:"10"`
	Page      int64 `json:"page" binding:"required" default:"1"`
	VacancyID int64 `json:"vacancy_id"`
}

type VacancyLanguages struct {
	Languages []CreateOrUpdateVacancyLanguage `json:"languages" binding:"required"`
}

package models

type ApplicantBlockedCompany struct {
	ID          int64       `json:"id"`
	ApplicantID int64       `json:"applicant_id"`
	CompanyID   int64       `json:"company_id"`
	CompanyInfo CompanyInfo `json:"company_info"`
	CreatedAt   string      `json:"created_at"`
}

type CompanyInfo struct {
	Image        string `json:"image_url"`
	CompanyName  string `json:"company_name"`
	Email        string `json:"email"`
	PhoneNumber  string `json:"phone_number"`
	Country      string `json:"country"`
	City         string `json:"city"`
	WorkersCount string `json:"workers_count"`
	Description  string `json:"description"`
	CreatedAt    string `json:"created_at"`
	Slug         string `json:"slug"`
}

type GetAllApplicantsBlockedCompanies struct {
	BlockedCompanies []*ApplicantBlockedCompany `json:"blocked_companies"`
	Count            int64                      `json:"count"`
}

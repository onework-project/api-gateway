package models

type RenewRefreshTokenReq struct {
	RefreshToken string `json:"refresh_token" binding:"required"`
}

type AccessTokenRes struct {
	AccessToken string `json:"access_token"`
}

type TokenRes struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

package models

type RegisterRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,min=6,max=16"`
	UserType string `json:"user_type" binding:"required" default:"applicant"`
}

type UserInfo struct {
	Email    string `json:"email"`
	Type     string `json:"user_type"`
	CreateAt string `json:"created_at"`
}

type LoginRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,min=6,max=16"`
	FcmToken string `json:"fcm_token" binding:"required"`
}

type LoginResponse struct {
	AccessToken  string  `json:"access_token"`
	RefreshToken string  `json:"refresh_token"`
	User         UserRes `json:"user"`
}

type ApplicantProfileRes struct {
	Application ApplicationProfileRes `json:"application"`
	User        ApplicantUserRes      `json:"user"`
}

type MemberProfileRes struct {
	CompanyInfo     *Company             `json:"company_info"`
	Company         OrganizationLoginRes `json:"organization"`
	User            UserRes              `json:"user"`
	UserCompanyType *string              `json:"company_role"`
}

type OrganizationLoginRes struct {
	BlockedApplicants []*BlockedApplicant `json:"blocked_applicants"`
	Members           []*MemberRes        `json:"members"`
}

type MemberRes struct {
	ID         int64    `json:"id"`
	CompanyID  int64    `json:"company_id"`
	MemberID   int64    `json:"member_id"`
	MemberInfo UserInfo `json:"member_info"`
	UserType   string   `json:"company_role"`
}

type ApplicationProfileRes struct {
	AcademicTests   []*ApplicantAcademic  `json:"academic_tests"`
	Awards          []*ApplicantAward     `json:"awards"`
	CheckedSteps    Step                  `json:"checked_steps"`
	Educations      []*ApplicantEducation `json:"educations"`
	Languages       []*ApplicantLanguage  `json:"languages"`
	Licences        []*ApplicantLicence   `json:"licences"`
	Researches      []*ApplicantResearch  `json:"researches"`
	Skills          []*ApplicantSkill     `json:"skills"`
	WorkExperiences []*ApplicantWork      `json:"work_experiences"`
}

type ApplicantUserRes struct {
	ID                   int64                      `json:"id"`
	FullName             string                     `json:"full_name"`
	Bio                  string                     `json:"bio"`
	Language             string                     `json:"lang"`
	LastStep             string                     `json:"last_step"`
	DateOfBirth          string                     `json:"date_of_birth"`
	Email                string                     `json:"email"`
	InvisibleAge         bool                       `json:"invisible_age"`
	City                 string                     `json:"city"`
	Country              string                     `json:"country"`
	PhoneNumber          *string                    `json:"phone_number"`
	Profession           string                     `json:"profession"`
	Role                 string                     `json:"role"`
	ImageUrl             *string                    `json:"image_url"`
	Slug                 string                     `json:"slug"`
	SocialMedias         []*ApplicantSocialMedia    `json:"social_medias"`
	BlockedOrganizations []*ApplicantBlockedCompany `json:"blocked_organizations"`
}

type VerifyRequest struct {
	Email string `json:"email" binding:"required,email"`
	Code  string `json:"code" binding:"required"`
}

type VerifyEmailRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Code     string `json:"code" binding:"required"`
	FcmToken string `json:"fcm_token" binding:"required"`
}

type EmailRequest struct {
	Email string `json:"email" binding:"required,email"`
}

type UpdatePasswordRequest struct {
	Password string `json:"password"  binding:"required"`
}

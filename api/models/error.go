package models

type ResponseError struct {
	Status string `json:"status"`
	Error  string `json:"error"`
}

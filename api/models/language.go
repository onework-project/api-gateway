package models

type ApplicantLanguage struct {
	ID          int64  `json:"id"`
	ApplicantID int64  `json:"applicant_id"`
	Language    string `json:"language"`
	Level       string `json:"level"`
}

type CreateOrUpdateApplicantLanguage struct {
	Language string `json:"language" binding:"required"`
	Level    string `json:"level" binding:"required"`
}

type Languages struct {
	Languages []CreateOrUpdateApplicantLanguage `json:"languages" binding:"required"`
}

type GetAllApplicantsLanguages struct {
	Languages []*ApplicantLanguage `json:"languages"`
	Count     int64                `json:"count"`
}

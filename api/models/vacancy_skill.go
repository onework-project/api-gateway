package models

type VacancySkill struct {
	ID        int64  `json:"id"`
	VacancyID int64  `json:"vacancy_id"`
	Skill     string `json:"skill"`
}

type CreateVacancySkill struct {
	Skill string `json:"skill" binding:"required"`
}

type VacancySkills struct {
	Skills []CreateVacancySkill `json:"skills" binding:"required"`
}

type GetAllVacanciesSkills struct {
	Skills []*VacancySkill `json:"skills"`
	Count  int64           `json:"count"`
}

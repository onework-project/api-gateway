package models

type CreateOrUpdateCompany struct {
	ImageUrl     string `json:"image_url" binding:"required"`
	CompanyName  string `json:"company_name" binding:"required"`
	Email        string `json:"email" binding:"required,email"`
	PhoneNumber  string `json:"phone_number"`
	Country      string `json:"country" binding:"required"`
	City         string `json:"city" binding:"required"`
	WorkersCount string `json:"workers_count" binding:"required"`
	Description  string `json:"description" binding:"required"`
}

type Company struct {
	ID           int64   `json:"id"`
	ImageUrl     string  `json:"image_url"`
	CompanyName  string  `json:"company_name"`
	Email        string  `json:"email"`
	PhoneNumber  *string `json:"phone_number"`
	Country      string  `json:"country"`
	City         string  `json:"city"`
	WorkersCount string  `json:"workers_count"`
	Posts        int64   `json:"posts"`
	Description  string  `json:"description"`
	CreatedAt    string  `json:"created_at"`
	UpdatedAt    string  `json:"updated_at"`
	Slug         string  `json:"slug"`
}

type GetAllCompaniesParams struct {
	Limit       int64  `json:"limit" binding:"required" default:"10"`
	Page        int64  `json:"page" binding:"required" default:"1"`
	CompanyName string `json:"company_name"`
	Location    string `json:"location"`
}

type GetAllCompaniesRes struct {
	Companies []*Company `json:"companies"`
	Count     int64      `json:"count"`
}

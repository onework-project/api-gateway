package models

type ApplicantResearch struct {
	ID               int64  `json:"id"`
	ApplicantID      int64  `json:"applicant_id"`
	OrganizationName string `json:"organization_name"`
	Position         string `json:"position"`
	Description      string `json:"description"`
	SuperViser       string `json:"superviser"`
	SuperViserEmail  string `json:"superviser_email"`
}

type CreateOrUpdateApplicantResearch struct {
	OrganizationName string `json:"organization_name" binding:"required"`
	Position         string `json:"position" binding:"required"`
	Description      string `json:"description" binding:"required"`
	SuperViser       string `json:"superviser" binding:"required"`
	SuperViserEmail  string `json:"superviser_email" binding:"email"`
}

type GetAllApplicantsResearches struct {
	Researches []*ApplicantResearch `json:"researches"`
	Count      int64                `json:"count"`
}

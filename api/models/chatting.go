package models

type ChatRes struct {
	ID            int64             `json:"id"`
	MemberID      int64             `json:"member_id"`
	CompanyInfo   ChatCompanyInfo   `json:"company_info"`
	ApplicantID   int64             `json:"applicant_id"`
	ApplicantInfo ChatApplicantInfo `json:"applicant_info"`
	CreatedAt     string            `json:"created_at"`
}

type ChatApplicantInfo struct {
	ID       int64  `json:"id"`
	Fullname string `json:"full_name"`
}

type ChatCompanyInfo struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type GetAllChatsParams struct {
	Limit  int64 `json:"limit" binding:"required" default:"10"`
	Page   int64 `json:"page" binding:"required" default:"1"`
	UserID int64 `json:"user_id"`
}

type GetAllChats struct {
	Chats []*ChatRes `json:"chats"`
	Count int64      `json:"count"`
}

type UpdateChatMessage struct {
	Message string `json:"message" binding:"required"`
}

type ChatMessage struct {
	ID        int64  `json:"id"`
	UserID    int64  `json:"user_id"`
	ChatID    int64  `json:"chat_id"`
	Message   string `json:"message"`
	CreatedAt string `json:"created_at"`
}

type GetAllChatMessagesParams struct {
	Limit  int64  `json:"limit" binding:"required" default:"10"`
	Page   int64  `json:"page" binding:"required" default:"1"`
	ChatID int64  `json:"chat_id"`
	Search string `json:"search"`
}

type GetAllChatsMessages struct {
	Messages []*ChatMessage `json:"messages"`
	Count    int64          `json:"count"`
}

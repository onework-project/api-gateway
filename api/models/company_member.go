package models

type Member struct {
	ID          int64       `json:"id"`
	CompanyID   int64       `json:"company_id"`
	CompanyInfo CompanyInfo `json:"company_info"`
	MemberID    int64       `json:"member_id"`
	MemberInfo  UserInfo    `json:"member_info"`
	UserType    string      `json:"company_role"`
}

type ExchangeType struct {
	MemberID int64 `json:"member_id"`
}

type GetAllMembersReq struct {
	Limit     int64 `json:"limit" binding:"required" default:"10"`
	Page      int64 `json:"page" binding:"required" default:"1"`
	CompanyID int64 `json:"company_id"`
}

type GetAllMembersRes struct {
	Members []*Member `json:"members"`
	Count   int64     `json:"count"`
}

package models

type CreateOrUpdateApplicantEducation struct {
	University     string `json:"university" binding:"required,min=10"`
	DegreeName     string `json:"degree_name" binding:"required,min=5"`
	DegreeLevel    string `json:"degree_level" binding:"required,min=5"`
	StartYear      string `json:"start_year" binding:"required"`
	EndYear        string `json:"end_year" binding:"required"`
	ApplicantGrade int64  `json:"applicant_grade"`
	MaxGrade       int64  `json:"max_grade"`
	PdfUrl         string `json:"pdf_url"`
}

type ApplicantEducation struct {
	ID             int64  `json:"id"`
	ApplicantID    int64  `json:"applicant_id"`
	University     string `json:"university"`
	DegreeName     string `json:"degree_name"`
	DegreeLevel    string `json:"degree_level"`
	StartYear      string `json:"start_year"`
	EndYear        string `json:"end_year"`
	ApplicantGrade int64  `json:"applicant_grade"`
	MaxGrade       int64  `json:"max_grade"`
	PdfUrl         *string `json:"pdf_url"`
}

type GetAllApplicantsEducations struct {
	Educations []*ApplicantEducation `json:"educations"`
	Count      int64                 `json:"count"`
}

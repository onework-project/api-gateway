package models

type ApplicationCheck struct {
	Experience struct {
		VacancyExperience   int64 `json:"vacancy_experience"`
		ApplicantExperience int64 `json:"applicant_experience"`
		IsMatched           bool  `json:"is_matched"`
	} `json:"experience"`
	Languages struct {
		NotHaveLanguages map[string]string `json:"not_have_languages"`
		IsMatched        bool              `json:"is_matched"`
	} `json:"languages"`
	Education struct {
		RequiredEducation *string `json:"required_education"`
		IsMatched         bool    `json:"is_matched"`
	} `json:"education"`
	IsGood bool `json:"is_good"`
}

type ApplicationMe struct {
	CheckedSteps       Step                  `json:"checked_steps"`
	Skills             []*ApplicantSkill     `json:"skils"`
	LastStep           string                `json:"last_step"`
	Languages          []*ApplicantLanguage  `json:"languages"`
	User               ApplicantProfileInfo  `json:"user"`
	Education          []*ApplicantEducation `json:"educations"`
	AcademicTest       []*ApplicantAcademic  `json:"applicant_tests"`
	LicenceCertificate []*ApplicantLicence   `json:"licences_certificates"`
	WorkExperiences    []*ApplicantWork      `json:"work_experiences"`
	Award              []*ApplicantAward     `json:"awards"`
	Research           []*ApplicantResearch  `json:"researches"`
	Type               string                `json:"role"`
}

type Application struct {
	ID              int64            `json:"id"`
	Status          string           `json:"status"`
	RejectionReason *string          `json:"rejection_reason"`
	ApplicantId     int64            `json:"applicant_id"`
	ApplicantData   GetApplicantInfo `json:"applicant_info"`
	VacancyId       int64            `json:"vacancy_id"`
	VacancyData     GetVacancyInfo   `json:"vacancy_info"`
}

type UpdateApplicationStatusReq struct {
	VacancyID int64  `json:"vacancy_id" binding:"required"`
	Status    string `json:"status" binding:"required"`
}

type GiveRejectionReq struct {
	VacancyID       int64  `json:"vacancy_id" binding:"required"`
	RejectionReason string `json:"rejection_reason" binding:"required"`
}

type GetAllApplications struct {
	Applications []*Application `json:"applications"`
	Count        int64          `json:"count"`
}

type GetAllApplicationsParams struct {
	Limit       int64 `json:"limit" binding:"required" default:"10"`
	Page        int64 `json:"page" binding:"required" default:"1"`
	ApplicantID int64 `json:"applicant_id"`
	VacancyID   int64 `json:"vacancy_id"`
}

type GetVacancyInfo struct {
	ID             int64       `json:"id"`
	CompanyID      int64       `json:"company_id"`
	CompanyInfo    CompanyInfo `json:"company_info"`
	Title          string      `json:"title"`
	Deadline       string      `json:"deadline"`
	EmploymentForm string      `json:"employment_form"`
	EmploymentType string      `json:"employment_type"`
	Country        string      `json:"country"`
	City           string      `json:"city"`
	SalaryMax      float64     `json:"salary_max"`
	SalaryMin      float64     `json:"salary_min"`
	Currency       string      `json:"currency"`
	SalaryPeriod   string      `json:"salary_period"`
	Education      *string     `json:"education"`
	Experience     int64       `json:"experienc"`
	Description    string      `json:"description"`
	IsActive       bool        `json:"is_active"`
	ViewsCount     int64       `json:"views_count"`
	Slug           string      `json:"slug"`
}

type GetApplicantInfo struct {
	ID           int64   `json:"id"`
	FirstName    string  `json:"first_name"`
	LastName     string  `json:"last_name"`
	Speciality   string  `json:"speciality"`
	Country      string  `json:"country"`
	City         string  `json:"city"`
	DateOfBirth  string  `json:"date_of_birth"`
	InvisibleAge bool    `json:"invisible_age"`
	PhoneNumber  *string `json:"phone_number"`
	Email        string  `json:"email"`
	Bio          string  `json:"bio"`
	ImageUrl     *string `json:"image_url"`
	Slug         string  `json:"slug"`
}

type CreateApplicationReq struct {
	VacancyID int64 `json:"vacancy_id"`
}

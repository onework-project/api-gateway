package models

type Applicant struct {
	ID           int64   `json:"id"`
	FirstName    string  `json:"first_name"`
	LastName     string  `json:"last_name"`
	Speciality   string  `json:"speciality"`
	Country      string  `json:"country"`
	City         string  `json:"city"`
	DateOfBirth  string  `json:"date_of_birth"`
	InvisibleAge bool    `json:"invisible_age"`
	PhoneNumber  *string `json:"phone_number"`
	Email        string  `json:"email"`
	Bio          string  `json:"bio"`
	ImageUrl     *string `json:"image_url"`
	LastStep     string  `json:"last_step"`
	CreatedAt    string  `json:"created_at"`
	UpdatedAt    string  `json:"updated_at"`
	Slug         string  `json:"slug"`
}

type ApplicantProfileInfo struct {
	ID                   int64                      `json:"id"`
	FullName             string                     `json:"full_name"`
	Bio                  string                     `json:"bio"`
	LastStep             string                     `json:"last_step"`
	DateOfBirth          string                     `json:"date_of_birth"`
	Email                string                     `json:"email"`
	InvisibleAge         bool                       `json:"invisible_age"`
	City                 string                     `json:"city"`
	Country              string                     `json:"country"`
	PhoneNumber          string                     `json:"phone_number"`
	Profession           string                     `json:"profession"`
	ImageUrl             string                     `json:"image_url"`
	SocialMedias         []*ApplicantSocialMedia    `json:"social_medias"`
	BlockedOrganizations []*ApplicantBlockedCompany `json:"blocked_organizations"`
}

type CreateOrUpdateApplicant struct {
	FirstName    string `json:"first_name" binding:"required,min=2,max=50"`
	LastName     string `json:"last_name" binding:"required,min=2,max=50"`
	Speciality   string `json:"speciality" binding:"required,min=10,max=50"`
	Country      string `json:"country" binding:"required"`
	City         string `json:"city" binding:"required"`
	DateOfBirth  string `json:"date_of_birth"  binding:"required"`
	InvisibleAge bool   `json:"invisible_age" default:"false"`
	PhoneNumber  string `json:"phone_number"`
	Bio          string `json:"bio" binding:"required,min=10"`
	ImageUrl     string `json:"image_url"`
}

type ApplicantStep struct {
	Step string `json:"step" binding:"required" default:"work" enums:"general_info,work,education,academic_tests,licences_certificates,awards,research,completed"`
}

type GetAllApplicants struct {
	Applicants []*Applicant `json:"applicants"`
	Count      int64        `json:"count"`
}

type GetAllApplicantsThingsParams struct {
	Limit       int64 `json:"limit" binding:"required" default:"10"`
	Page        int64 `json:"page" binding:"required" default:"1"`
	ApplicantID int64 `json:"applicant_id"`
}

type GetAllApplicantsParams struct {
	Limit      int64    `json:"limit" binding:"required" default:"10"`
	Page       int64    `json:"page" binding:"required" default:"1"`
	Languages  []string `json:"languages"`
	Educations []string `json:"educations"`
	Licences   []string `json:"licences"`
	Skills     []string `json:"skills"`
}

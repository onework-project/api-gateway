package models

type CreateOrUpdateApplicantLicence struct {
	Name         string `json:"licence_name" binding:"required"`
	Organization string `json:"organization" binding:"required"`
	PdfUrl       string `json:"pdf_url"`
}

type ApplicantLicence struct {
	ID           int64  `json:"id"`
	ApplicantID  int64  `json:"applicant_id"`
	Name         string `json:"licence_name"`
	Organization string `json:"organization"`
	PdfUrl       *string `json:"pdf_url"`
}

type GetAllApplicantsLicences struct {
	Licences []*ApplicantLicence `json:"licences"`
	Count    int64               `json:"count"`
}

package models

type VacancyMarket struct {
	ID        int64  `json:"id"`
	VacancyID int64  `json:"vacancy_id"`
	Market    string `json:"market"`
}

type CreateVacancyMarket struct {
	Market string `json:"market" binding:"required"`
}

type VacancyMarkets struct {
	Markets []CreateVacancyMarket `json:"markets" binding:"required"`
}

type GetAllMarketsRes struct {
	Markets []*VacancyMarket `json:"markets"`
	Count   int64            `json:"count"`
}

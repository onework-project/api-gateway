package models

type CreateUserReq struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

type ChangeLanguageReq struct {
	Language string `json:"lang" binding:"required"`
}

type UserRes struct {
	ID        int64  `json:"id"`
	Email     string `json:"email"`
	Type      string `json:"role"`
	CompanyID int64  `json:"company_id"`
	Language  string `json:"lang"`
	CreatedAt string `json:"created_at"`
}

type UpdateOrGetEmailReq struct {
	Email string `json:"email" binding:"required,email"`
}

type GetAllUsersResponse struct {
	Users []*UserRes `json:"users"`
	Count int64      `json:"count"`
}

package models

type Vacancy struct {
	ID             int64              `json:"id"`
	CompanyID      int64              `json:"company_id"`
	CompanyInfo    CompanyInfo        `json:"company_info"`
	Title          string             `json:"title"`
	Deadline       string             `json:"deadline"`
	EmploymentForm string             `json:"employment_form"`
	EmploymentType string             `json:"employment_type"`
	Country        string             `json:"country"`
	City           string             `json:"city"`
	SalaryMax      float64            `json:"salary_max"`
	SalaryMin      float64            `json:"salary_min"`
	Currency       string             `json:"currency"`
	SalaryPeriod   string             `json:"salary_period"`
	Education      *string            `json:"education"`
	Experience     int64              `json:"experienc"`
	Description    string             `json:"description"`
	IsActive       bool               `json:"is_active"`
	ViewsCount     int64              `json:"views_count"`
	Languages      []*VacancyLanguage `json:"languages"`
	Skills         []*VacancySkill    `json:"skills"`
	Markets        []*VacancyMarket   `json:"marketes"`
	CreatedAt      string             `json:"created_at"`
	UpdatedAt      string             `json:"updated_at"`
	Slug           string             `json:"slug"`
}

type CreateOrUpdateVacancy struct {
	Title          string  `json:"title" binding:"required"`
	Deadline       string  `json:"deadline" binding:"required"`
	EmploymentForm string  `json:"employment_form" binding:"required"`
	EmploymentType string  `json:"employment_type" binding:"required"`
	Country        string  `json:"country" binding:"required"`
	City           string  `json:"city" binding:"required"`
	SalaryMax      float64 `json:"salary_max" binding:"required"`
	SalaryMin      float64 `json:"salary_min" binding:"required"`
	Currency       string  `json:"currency" binding:"required"`
	SalaryPeriod   string  `json:"salary_period" binding:"required"`
	Education      string  `json:"education"`
	Experience     int64   `json:"experience" binding:"required"`
	Description    string  `json:"description" binding:"required"`
}

type VacancyIdReq struct {
	CompanyID int64 `json:"company_id" binding:"required"`
}

type GetAllVacanciesParamsReq struct {
	Limit             int64  `json:"limit" binding:"required" default:"10"`
	Page              int64  `json:"page" binding:"required" default:"1"`
	CompanyID         int64  `json:"company_id"`
	Title             string `json:"title"`
	EmploymentForm    string `json:"employment_form" enums:"remote,in-office,hybrid"`
	EmploymentType    string `json:"employment_type" enums:"full-time,part-time,freelance,contract,internship,apprenticeship,seasonal"`
	Market            string `json:"market"`
	Location          string `json:"location"`
	TimeRange         string `json:"time_range" enums:"past_day,past_week,past_month"`
	YearsOfExperience int64  `json:"years_of_experience"`
}

type GetAllVacanciesRes struct {
	Vacancies []*Vacancy `json:"vacancies"`
	Count     int64      `json:"count"`
}

type ActivateOrDeactivateReq struct {
	Argument bool `json:"argument" binding:"required"`
}

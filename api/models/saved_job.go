package models

type ApplicantSavedJob struct {
	ID          int64       `json:"id"`
	ApplicantID int64       `json:"applicant_id"`
	VacancyID   int64       `json:"vacancy_id"`
	VacancyInfo VacancyInfo `json:"vacancy_info"`
	CreatedAt   string      `json:"created_at"`
}

type VacancyInfo struct {
	CompanyID      int64       `jsVacancyIDon:"company_id"`
	CompanyInfo    CompanyInfo `json:"company_info"`
	Title          string      `json:"title"`
	Deadline       string      `json:"deadline"`
	EmploymentForm string      `json:"employment_form"`
	EmploymentType string      `json:"employment_type"`
	Country        string      `json:"country"`
	City           string      `json:"city"`
	SalaryMin      float64     `json:"salary_min"`
	SalaryMax      float64     `json:"salary_max"`
	Currency       string      `json:"currency"`
	SalaryPeriod   string      `json:"salary_period"`
	Education      string      `json:"education"`
	Experience     int64       `json:"experience"`
	Description    string      `json:"description"`
	ViewsCount     int64       `json:"views_count"`
	CreatedAt      string      `json:"created_at"`
	UpdatedAt      string      `json:"updated_at"`
	Slug           string      `json:"slug"`
}

type CreateApplicantSavedJob struct {
	VacancyID int64 `json:"vacancy_id" binding:"required"`
}

type GetAllApplicantsSavedJobs struct {
	SavedJobs []*ApplicantSavedJob `json:"saved_jobs"`
	Count     int64                `json:"count"`
}

package api

import (
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	v1 "gitlab.com/onework-project/api-gateway/api/v1"
	"gitlab.com/onework-project/api-gateway/config"
	grpcPkg "gitlab.com/onework-project/api-gateway/pkg/grpc_client"
	"gitlab.com/onework-project/api-gateway/pkg/logging"
	"go.opentelemetry.io/otel/trace"
)

type RoutetOptions struct {
	Cfg        *config.Config
	GrpcClient grpcPkg.GrpcClientI
	Logger     *logging.Logger
	CfgAws     *s3.Client
	Tracer     trace.Tracer
}

// TODO: add getall skill and market and languages and countries and cities table and endpoints
// TODO: add getall permissions
// New @title           OneWork Project Apis
// @version         2.0
// @description     OneWork Project Swagger Documentation
// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
// @description Type "Bearer" followed by a space and JWT token.
func New(opt *RoutetOptions) *gin.Engine {
	router := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "*")
	router.Use(cors.New(corsConfig))

	handler := v1.New(&v1.HandlerV1Options{
		Cfg:        opt.Cfg,
		GrpcClient: opt.GrpcClient,
		Logger:     opt.Logger,
		CfgAws:     opt.CfgAws,
		Tracer:     opt.Tracer,
	})

	router.POST("/images", handler.AuthMiddleWare("images", "upload"), handler.UploadImage)
	router.POST("/pdfs", handler.AuthMiddleWare("pdfs", "upload"), handler.UploadPdf)
	router.DELETE("/delete/pdfs", handler.AuthMiddleWare("pdfs", "delete"), handler.DeletePdf)
	router.DELETE("/delete/images", handler.AuthMiddleWare("images", "delete"), handler.DeleteImage)

	applicant := router.Group("/applicants")
	{
		applicant.POST("", handler.AuthMiddleWare("applicants", "create"), handler.CreateApplicant)
		applicant.GET("/:id", handler.GetApplicant)
		applicant.PUT("/:id", handler.AuthMiddleWare("applicants", "update"), handler.UpdateApplicant)
		applicant.PUT("", handler.AuthMiddleWare("applicants", "update-step"), handler.UpdateApplicantStepField)
		applicant.DELETE("/:id", handler.AuthMiddleWare("applicants", "delete"), handler.DeleteApplicant)
		applicant.GET("", handler.AuthMiddleWare("applicants", "getall"), handler.GetAllApplicants)
		media := applicant.Group("/medias")
		{
			media.POST("", handler.AuthMiddleWare("applicant_medias", "create"), handler.CreateApplicantSocialMedia)
			media.GET("", handler.GetAllApplicantSocialMedias)
		}
		skill := applicant.Group("/skills")
		{
			skill.POST("", handler.AuthMiddleWare("applicant_skills", "create"), handler.CreateApplicantSkill)
			skill.GET("", handler.GetAllApplicantsSkills)
		}
		language := applicant.Group("/languages")
		{
			language.POST("", handler.AuthMiddleWare("applicant_languages", "create"), handler.CreateApplicantLanguage)
			language.GET("", handler.GetAllApplicantsLanguages)
		}
		academic := applicant.Group("/academics")
		{
			academic.POST("", handler.AuthMiddleWare("applicant_academics", "create"), handler.CreateApplicantAcademic)
			academic.GET("/:id", handler.GetApplicantAcademic)
			academic.PUT("/:id", handler.AuthMiddleWare("applicant_academics", "update"), handler.UpdateApplicantAcademic)
			academic.DELETE("/:id", handler.AuthMiddleWare("applicant_academics", "delete"), handler.DeleteApplicantAcademic)
			academic.GET("", handler.GetAllApplicantsAcademics)
		}
		award := applicant.Group("/awards")
		{
			award.POST("", handler.AuthMiddleWare("applicant_awards", "create"), handler.CreateApplicantAward)
			award.GET("/:id", handler.GetApplicantAward)
			award.PUT("/:id", handler.AuthMiddleWare("applicant_awards", "update"), handler.UpdateApplicantAward)
			award.DELETE("/:id", handler.AuthMiddleWare("applicant_awards", "delete"), handler.DeleteApplicantAward)
			award.GET("", handler.GetAllApplicantsAwards)
		}
		education := applicant.Group("/educations")
		{
			education.POST("", handler.AuthMiddleWare("applicant_educations", "create"), handler.CreateApplicantEducation)
			education.GET("/:id", handler.GetApplicantEducation)
			education.PUT("/:id", handler.AuthMiddleWare("applicant_educations", "update"), handler.UpdateApplicantEducation)
			education.DELETE("/:id", handler.AuthMiddleWare("applicant_educations", "delete"), handler.DeleteApplicantEducation)
			education.GET("", handler.GetAllApplicantsEducations)
		}
		licence := applicant.Group("/licences")
		{
			licence.POST("", handler.AuthMiddleWare("applicant_licences", "create"), handler.CreateApplicantLicence)
			licence.GET("/:id", handler.GetApplicantLicence)
			licence.PUT("/:id", handler.AuthMiddleWare("applicant_licences", "update"), handler.UpdateApplicantLicence)
			licence.DELETE("/:id", handler.AuthMiddleWare("applicant_licences", "delete"), handler.DeleteApplicantLicence)
			licence.GET("", handler.GetAllApplicantsLicences)
		}
		research := applicant.Group("/researches")
		{
			research.POST("", handler.AuthMiddleWare("applicant_researches", "create"), handler.CreateApplicantResearch)
			research.GET("/:id", handler.GetApplicantResearch)
			research.PUT("/:id", handler.AuthMiddleWare("applicant_researches", "update"), handler.UpdateApplicantResearch)
			research.DELETE("/:id", handler.AuthMiddleWare("applicant_researches", "delete"), handler.DeleteApplicantResearch)
			research.GET("", handler.GetAllApplicantsResearchs)
		}
		work := applicant.Group("/works")
		{
			work.POST("", handler.AuthMiddleWare("applicant_works", "create"), handler.CreateWork)
			work.GET("/:id", handler.GetWork)
			work.PUT("/:id", handler.AuthMiddleWare("applicant_works", "update"), handler.UpdateWork)
			work.DELETE("/:id", handler.AuthMiddleWare("applicant_works", "delete"), handler.DeleteWork)
			work.GET("", handler.GetAllWorks)
		}
		blockedCompanies := applicant.Group("/blocked-companies")
		{
			blockedCompanies.POST("/:id", handler.AuthMiddleWare("applicant_blocked_company", "create"), handler.CreateApplicantBlockedCompany)
			blockedCompanies.DELETE("/:id", handler.AuthMiddleWare("applicant_blocked_company", "delete"), handler.DeleteApplicantBlockedCompany)
			blockedCompanies.GET("", handler.GetAllApplicantsBlockedCompanies)
		}
		savedJob := applicant.Group("/saved-jobes")
		{
			savedJob.POST("/:id", handler.AuthMiddleWare("applicant_saved_jobes", "create"), handler.CreateApplicantSavedJob)
			savedJob.DELETE("/:id", handler.AuthMiddleWare("applicant_saved_jobes", "delete"), handler.DeleteApplicantSavedJob)
			savedJob.GET("", handler.GetAllApplicantsSavedJobs)
		}
		step := applicant.Group("/steps")
		{
			step.PUT("", handler.AuthMiddleWare("applicant_steps", "update"), handler.UpdateApplicantStep)
		}
	}

	company := router.Group("/companies")
	{
		company.POST("", handler.AuthMiddleWare("companies", "create"), handler.CreateCompany)
		company.GET("/:id", handler.GetCompany)
		company.PUT("/:id", handler.AuthMiddleWare("companies", "update"), handler.UpdateCompany)
		company.DELETE("/:id", handler.AuthMiddleWare("companies", "delete"), handler.DeleteCompany)
		company.GET("", handler.GetAllCompanies)
		blockedApplicant := company.Group("/blocked-applicants")
		{
			blockedApplicant.POST("/:id", handler.AuthMiddleWare("companies_blocked_applicants", "create"), handler.CreateCompanyBlockedApplicant)
			blockedApplicant.DELETE("/:id", handler.AuthMiddleWare("companies_blocked_applicants", "delete"), handler.DeleteCompanyBlockedApplicant)
			blockedApplicant.GET("", handler.GetAllCompaniesBlockedApplicants)
		}
		media := company.Group("/medias")
		{
			media.POST("", handler.AuthMiddleWare("companies_media", "create"), handler.CreateCompanySocialMedia)
			media.GET("", handler.GetAllCompanySocialMedias)
		}
	}

	member := router.Group("/members")
	{
		member.POST("/:id", handler.AuthMiddleWare("members", "add"), handler.AddMember)
		member.GET("/:id", handler.AuthMiddleWare("members", "get"), handler.GetMember)
		member.PUT("/:id", handler.AuthMiddleWare("members", "update"), handler.ExchangeMemberType)
		member.DELETE("/:id", handler.AuthMiddleWare("members", "delete"), handler.DeleteMember)
		member.GET("", handler.AuthMiddleWare("members", "getall"), handler.GetAllMembers)
	}

	vacancy := router.Group("/vacancies")
	{
		vacancy.POST("", handler.AuthMiddleWare("vacancies", "create"), handler.CreateVacancy)
		vacancy.GET("/:id", handler.GetVacancy)
		vacancy.GET("/active", handler.GetAllActiveVacancies)
		vacancy.GET("/disactive", handler.AuthMiddleWare("vacancies", "get-all-disactive"), handler.GetAllDisactiveVacancy)
		vacancy.PUT("/activate-or-deactivate/:id", handler.AuthMiddleWare("vacancies", "activate-or-deactivate"), handler.ActivateOrDeactivate)
		vacancy.PUT("/:id", handler.AuthMiddleWare("vacancies", "update"), handler.UpdateVacancy)
		vacancy.DELETE("/:id", handler.AuthMiddleWare("vacancies", "delete"), handler.DeleteVacancy)
		language := vacancy.Group("/languages")
		{
			language.POST("/:id", handler.AuthMiddleWare("vacancies_languages", "create"), handler.CreateVacancyLanguage)
			language.GET("", handler.GetAllVacanciesLanguages)
		}
		market := vacancy.Group("/markets")
		{
			market.POST("/:id", handler.AuthMiddleWare("vacancies_markets", "create"), handler.CreateVacancyMarket)
			market.GET("", handler.GetAllVacanciesMarkets)
		}
		skill := vacancy.Group("/skills")
		{
			skill.POST("/:id", handler.AuthMiddleWare("vacancies_skills", "create"), handler.CreateVacancySkill)
			skill.GET("", handler.GetAllVacanciesSkills)
		}
	}
	register := router.Group("/auth")
	{
		register.POST("/sign-up", handler.SignUp)
		register.POST("/login", handler.Login)
		register.POST("/verify", handler.VerifyEmail)
		register.POST("/logout", handler.AuthMiddleWare("auth", "logout"), handler.Logout)
	}

	application := router.Group("/applications")
	{
		application.POST("/:vacancy_id", handler.AuthMiddleWare("applications", "create"), handler.CreateApplication)
		application.GET("/:id", handler.AuthMiddleWare("applications", "get"), handler.GetApplication)
		application.PUT("/:id", handler.AuthMiddleWare("applications", "update"), handler.UpdateStatus)
		application.PUT("/reject/:id", handler.AuthMiddleWare("applications", "reject"), handler.GiveRejection)
		application.DELETE("/:id", handler.AuthMiddleWare("applications", "delete"), handler.DeleteApplication)
		application.GET("", handler.AuthMiddleWare("applications", "getall"), handler.GetAllApplication)
		application.GET("/check/:id", handler.AuthMiddleWare("applications", "check"), handler.CheckBeforeApplication)
	}
	email := router.Group("/email")
	{
		email.PUT("/update", handler.AuthMiddleWare("email", "update"), handler.UpdateEmail)
		email.POST("/verify", handler.AuthMiddleWare("email", "verify"), handler.VerifyUpdateEmail)
	}

	passwords := router.Group("/password")
	{
		passwords.POST("/recovery", handler.ForgotPassword)
		passwords.POST("/update", handler.AuthMiddleWare("password", "update"), handler.UpdatePassword)
		passwords.POST("/verify", handler.VerifyForgotPassoword)
	}

	users := router.Group("/users")
	{
		users.POST("/create", handler.AuthMiddleWare("users", "create"), handler.CreateUser)
		users.GET("/:id", handler.AuthMiddleWare("users", "get"), handler.GetUser)
		users.GET("/by-email", handler.AuthMiddleWare("users", "get-by-email"), handler.GetByEmail)
		users.DELETE("/:id", handler.AuthMiddleWare("users", "delete"), handler.DeleteUser)
		users.GET("", handler.AuthMiddleWare("users", "get-all"), handler.GetAllUsers)
	}
	api := router.Group("/api")
	{
		api.PUT("/lang", handler.AuthMiddleWare("language", "update"), handler.ChangeLanguage)
		api.GET("/profile/applicant", handler.AuthMiddleWare("profile-applicant", "get"), handler.GetApplicantProfile)
		api.GET("/profile/member", handler.AuthMiddleWare("profile-member", "get"), handler.GetMemberProfile)
	}

	chat := router.Group("/chats")
	{
		chat.POST("/:id", handler.AuthMiddleWare("chats", "create"), handler.CreateChat)
		chat.DELETE("/:id", handler.AuthMiddleWare("chats", "delete"), handler.DeleteChat)
		chat.GET("", handler.AuthMiddleWare("chats", "getall"), handler.GetAllChat)
		chat.GET("/:id", handler.AuthMiddleWare("chats", "get"), handler.GetChat)
	}
	message := router.Group("/messages")
	{
		message.GET("", handler.AuthMiddleWare("messages", "getall"), handler.GetAllMessages)
	}

	session := router.Group("/sessions")
	{
		session.PUT("/block", handler.AuthMiddleWare("session", "block"), handler.BlockSession)
		session.PUT("/unblock", handler.AuthMiddleWare("session", "unblock"), handler.UnBlockSession)
		session.POST("/terminate", handler.AuthMiddleWare("session", "terminate"), handler.TerminateSession)
		session.GET("", handler.AuthMiddleWare("session", "getall"), handler.GetAllSessions)
	}
	router.POST("/token/refresh", handler.RefreshToken)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}

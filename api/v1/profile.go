package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
)

// @Security Bearer
// @Router /api/profile/applicant [get]
// @Summary getApplicantProfile
// @Description Get Applicant Type User profile all information
// @Tags profile
// @ID GetApplicantProfile
// @Produce json
// @Success 200 {object} models.ApplicantProfileRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) GetApplicantProfile(ctx *gin.Context) {
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	if payload.UserType != UserTypeApplicant {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	res := models.ApplicantProfileRes{}
	h.GetApplicationInfo(&res, payload.UserID)
	ctx.JSON(http.StatusOK, res)
}

// @Security Bearer
// @Router /api/profile/member [get]
// @Summary getMemberProfile
// @Description Get Member Profile Information
// @Tags profile
// @ID GetMemberProfile
// @Produce json
// @Success 200 {object} models.MemberProfileRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) GetMemberProfile(ctx *gin.Context) {
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	if payload.UserType != UserTypeMember {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	res := models.MemberProfileRes{}
	h.GetMemberInfo(&res, payload.UserID)
	if res.User.CompanyID > 0 {
		h.GetCompanyInfo(&res, res.User.CompanyID)
	}
	ctx.JSON(http.StatusOK, res)
}

// @Security Bearer
// @Router /api/lang [put]
// @Summary changeLanguage
// @Description Change language = {uz, ru, eng} in body
// @Tags profile
// @ID ChangeLanguage
// @Accept json
// @Produce json
// @Param data body models.ChangeLanguageReq true "Data"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) ChangeLanguage(ctx *gin.Context) {
	var inp models.ChangeLanguageReq
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	_, err = h.grpcClient.UserService().UpdateLanguage(context.Background(), &auth_service.UpdateLanguageReq{
		Lang: inp.Language,
		Id:   payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

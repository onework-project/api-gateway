package v1

import (
	"context"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
)

const (
	authorizationHeaderKey  = "Authorization"
	authorizationTypeBearer = "bearer"
	authorizationPayloadKey = "authorization_payload"
)

type Payload struct {
	Id        string `json:"id"`
	UserID    int64  `json:"user_id"`
	Email     string `json:"email"`
	UserType  string `json:"user_type"`
	IssuedAt  string `json:"issued_at"`
	ExpiredAt string `json:"expired_at"`
}

func (h *handlerV1) AuthMiddleWare(resource, action string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authorizationHeader := ctx.GetHeader(authorizationHeaderKey)

		if len(authorizationHeader) == 0 {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.ResponseError{
				Status: "fail",
				Error:  ErrUnauthorized.Error(),
			})
			return
		}

		fields := strings.Fields(authorizationHeader)
		if len(fields) < 2 {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.ResponseError{
				Status: "fail",
				Error:  ErrUnauthorized.Error(),
			})
			return
		}
		authorizationType := strings.ToLower(fields[0])
		if authorizationType != authorizationTypeBearer {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.ResponseError{
				Status: "fail",
				Error:  ErrUnauthorized.Error(),
			})
			return
		}

		accessToken := fields[1]
		payload, err := h.grpcClient.AuthService().VerifyToken(context.Background(), &auth_service.VerifyTokenReq{
			AccessToken: accessToken,
			Resource:    resource,
			Action:      action,
		})
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.ResponseError{
				Status: "fail",
				Error:  ErrUnauthorized.Error(),
			})
			return
		}
		if !payload.HasPermission {
			ctx.AbortWithStatusJSON(http.StatusForbidden, models.ResponseError{
				Status: "fail",
				Error:  ErrNotAllowed.Error(),
			})
			return
		}
		ctx.Set(authorizationPayloadKey, payload)
		ctx.Next()
	}
}

func (h *handlerV1) GetAuthPayload(ctx *gin.Context) (*Payload, error) {
	i, exist := ctx.Get(authorizationPayloadKey)
	if !exist {
		return nil, ErrNotFound
	}
	payload, ok := i.(*auth_service.VerifyTokenRes)
	if !ok {
		return nil, ErrUnknownUser
	}
	return &Payload{
		Id:        payload.Id,
		UserID:    payload.UserId,
		Email:     payload.Email,
		UserType:  payload.UserType,
		IssuedAt:  payload.IssuedAt,
		ExpiredAt: payload.ExpiredAt,
	}, nil
}

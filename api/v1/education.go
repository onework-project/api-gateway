package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/educations [post]
// @Summary createApplicantEducation
// @Description After creating applicant add education if has it. and last_step will be education
// @Tags applicants_education
// @ID CreateApplicantEducation
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateApplicantEducation true "Data"
// @Success 201 {object} models.ApplicantEducation
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicantEducation(ctx *gin.Context) {
	var inp models.CreateOrUpdateApplicantEducation
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	education, err := h.grpcClient.ApplicantEducationService().Create(context.Background(), &pba.Education{
		ApplicantId:    payload.UserID,
		University:     inp.University,
		DegreeName:     inp.DegreeName,
		DegreeLevel:    inp.DegreeLevel,
		StartYear:      inp.StartYear,
		EndYear:        inp.EndYear,
		ApplicantGrade: inp.ApplicantGrade,
		MaxGrade:       inp.MaxGrade,
		PdfUrl:         inp.PdfUrl,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.ApplicantService().UpdateStepField(context.Background(), &pba.UpdateStepReq{
		Email: payload.Email,
		Arg:   ApplicantStepEducation,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantEducation(education))
}

// @Router /applicants/educations/{id} [get]
// @Summary getApplicantEducation
// @Description In order to get applicant education give education's "id"
// @Tags applicants_education
// @ID GetApplicantEducation
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ApplicantEducation
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetApplicantEducation(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	education, err := h.grpcClient.ApplicantEducationService().Get(context.Background(), &pba.GetReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, parseApplicantEducation(education))
}

// @Security Bearer
// @Router /applicants/educations/{id} [put]
// @Summary updateApplicantEducation
// @Description In order to update applicant education give education's "id" in path and fields to update in body
// @Tags applicants_education
// @ID UpdateApplicantEducation
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.CreateOrUpdateApplicantEducation true "Data"
// @Success 200 {object} models.ApplicantEducation
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateApplicantEducation(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.CreateOrUpdateApplicantEducation
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	education, err := h.grpcClient.ApplicantEducationService().Update(context.Background(), &pba.Education{
		Id:             id,
		ApplicantId:    payload.UserID,
		University:     inp.University,
		DegreeName:     inp.DegreeName,
		DegreeLevel:    inp.DegreeLevel,
		StartYear:      inp.StartYear,
		EndYear:        inp.EndYear,
		ApplicantGrade: inp.ApplicantGrade,
		MaxGrade:       inp.MaxGrade,
		PdfUrl:         inp.PdfUrl,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, parseApplicantEducation(education))
}

// @Security Bearer
// @Router /applicants/educations/{id} [delete]
// @Summary deleteApplicantEducation
// @Description In order to delete education give education's id
// @Tags applicants_education
// @ID DeleteApplicantEducation
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteApplicantEducation(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	_, err = h.grpcClient.ApplicantEducationService().Delete(context.Background(), &pba.DeleteOrGetReq{
		Id:      id,
		ThingId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /applicants/educations [get]
// @Summary getAllApplicantsEducations
// @Description getAllApplicantsEducations
// @Tags applicants_education
// @ID GetAllApplicantsEducations
// @Accept json
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsEducations
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsEducations(ctx *gin.Context) {
	params, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	Educations, err := h.grpcClient.ApplicantEducationService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       params.Limit,
		Page:        params.Page,
		ApplicantId: params.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsEducations{
		Educations: make([]*models.ApplicantEducation, 0),
		Count:      Educations.Count,
	}
	for _, v := range Educations.Educations {
		a := parseApplicantEducation(v)
		response.Educations = append(response.Educations, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantEducation(a *pba.Education) models.ApplicantEducation {
	return models.ApplicantEducation{
		ID:             a.Id,
		ApplicantID:    a.ApplicantId,
		University:     a.University,
		DegreeName:     a.DegreeName,
		DegreeLevel:    a.DegreeLevel,
		StartYear:      a.StartYear,
		EndYear:        a.EndYear,
		ApplicantGrade: a.ApplicantGrade,
		MaxGrade:       a.MaxGrade,
		PdfUrl:         setNull(a.PdfUrl),
	}
}

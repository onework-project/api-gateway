package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/company_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// @Security Bearer
// @Router /applications/check/{id} [get]
// @Summary checkBeforeApplication
// @Description Before creating application make a request to check application, that applicant is good for vacancy
// @Tags application
// @ID CheckBeforeApplication
// @Produce json
// @Param id path int true "VacancyId"
// @Success 200 {object} models.Application
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) CheckBeforeApplication(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	var response models.ApplicationCheck
	h.CheckAll(&response, id, payload.UserID)

	ctx.JSON(http.StatusOK, response)
}

// @Security Bearer
// @Router /applications/{vacancy_id} [post]
// @Summary createApplication
// @Description Applicant type iday user vacancy ga topshirishi uchun api. Bu api ga muroaat qilishdan oldin applicant vacacniga togri keladimi yoqmi tekshirish kerak bo'ladi.
// @Tags application
// @ID CreateApplication
// @Produce json
// @Param vacancy_id path int true "VacancyId"
// @Success 201 {object} models.Application
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplication(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("vacancy_id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	application, err := h.grpcClient.CompanyApplicationService().Create(context.Background(), &company_service.Application{
		ApplicantId: payload.UserID,
		VacancyId:   id,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicationModel(application))
}

// @Security Bearer
// @Router /applications/{id} [get]
// @Summary getApplication
// @Description Get Application api applicantni apply qilgandan keyin bilib turishi uchun, path "id" ya'ni applicationni id isi berilishi lozim.
// @Tags application
// @ID GetApplication
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.Application
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) GetApplication(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	application, err := h.grpcClient.CompanyApplicationService().Get(context.Background(), &company_service.GetOrDeleteApplicationReq{Id: id})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
			return
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicationModel(application))
}

// @Security Bearer
// @Router /applications/{id} [put]
// @Summary updateStatusApplication
// @Description applicant ni statusini o'zgartirib turish uchun,  status = applied,screening,interview,accepted
// @Tags application
// @ID UpdateStatus
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.UpdateApplicationStatusReq true "Data"
// @Success 200 {object} models.Application
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) UpdateStatus(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.UpdateApplicationStatusReq
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	application, err := h.grpcClient.CompanyApplicationService().UpdateStatus(context.Background(), &company_service.ApplicationUpdateStatus{
		Id:        id,
		VacancyId: inp.VacancyID,
		Status:    inp.Status,
	})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
			return
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicationModel(application))
}

// @Security Bearer
// @Router /applications/reject/{id} [put]
// @Summary giveRejectionApplication
// @Description reject berish uchun status = rejected and reason
// @Tags application
// @ID GiveRejection
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.GiveRejectionReq true "Data"
// @Success 200 {object} models.Application
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) GiveRejection(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.GiveRejectionReq
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	application, err := h.grpcClient.CompanyApplicationService().UpdateStatusRejection(context.Background(), &company_service.RejectApplicationReq{
		Id:              id,
		VacancyId:       inp.VacancyID,
		RejectionReason: inp.RejectionReason,
	})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
			return
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicationModel(application))
}

// @Security Bearer
// @Router /applications/{id} [delete]
// @Summary deleteApplication
// @Description applicationni o'chirib yuborish uchun.
// @Tags application
// @ID DeleteApplication
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) DeleteApplication(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.UpdateApplicationStatusReq
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	_, err = h.grpcClient.CompanyApplicationService().Delete(context.Background(), &company_service.ApplicationIDReq{
		Id:        id,
		VacancyId: inp.VacancyID,
	})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
			return
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Security Bearer
// @Router /applications [get]
// @Summary getAllApplication
// @Description hamma applicationlarni olish uchun kerak boladi.
// @Tags application
// @ID GetAllApplication
// @Accept json
// @Produce json
// @Param params query models.GetAllApplicationsParams false "Params"
// @Success 200 {object} models.GetAllApplications
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) GetAllApplication(ctx *gin.Context) {
	params, err := validateGetAllApplicationsParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	application, err := h.grpcClient.CompanyApplicationService().GetAll(context.Background(), &company_service.GetAllApplicationParams{
		Limit:       int32(params.Limit),
		Page:        int32(params.Page),
		ApplicantId: params.ApplicantID,
		VacancyId:   params.VacancyID,
	})
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	response := models.GetAllApplications{
		Applications: make([]*models.Application, 0),
		Count:        int64(application.Count),
	}
	for _, v := range application.Applications {
		a := parseApplicationModel(v)
		response.Applications = append(response.Applications, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicationModel(application *company_service.Application) models.Application {
	return models.Application{
		ID:              application.Id,
		Status:          application.Status,
		RejectionReason: setNull(application.RejectionReason),
		ApplicantId:     application.ApplicantId,
		ApplicantData: models.GetApplicantInfo{
			ID:           application.Applicant.Id,
			FirstName:    application.Applicant.FirstName,
			LastName:     application.Applicant.LastName,
			Speciality:   application.Applicant.Speciality,
			Country:      application.Applicant.Country,
			City:         application.Applicant.City,
			DateOfBirth:  application.Applicant.DateOfBirth,
			InvisibleAge: application.Applicant.InvisibleAge,
			PhoneNumber:  setNull(application.Applicant.PhoneNumber),
			Email:        application.Applicant.Email,
			Bio:          application.Applicant.About,
			ImageUrl:     setNull(application.Applicant.ImageUrl),
			Slug:         application.Applicant.Slug,
		},
		VacancyId: application.VacancyId,
		VacancyData: models.GetVacancyInfo{
			ID:        application.VacancyId,
			CompanyID: application.Vacancy.CompanyId,
			CompanyInfo: models.CompanyInfo{
				Image:        application.Vacancy.CompanyInfo.ImageUrl,
				CompanyName:  application.Vacancy.CompanyInfo.CompanyName,
				Email:        application.Vacancy.CompanyInfo.Email,
				PhoneNumber:  application.Vacancy.CompanyInfo.PhoneNumber,
				Country:      application.Vacancy.CompanyInfo.Country,
				City:         application.Vacancy.CompanyInfo.RegionState,
				WorkersCount: application.Vacancy.CompanyInfo.WorkersCount,
				Description:  application.Vacancy.CompanyInfo.Description,
				CreatedAt:    application.Vacancy.CompanyInfo.CreatedAt,
				Slug:         application.Vacancy.CompanyInfo.Slug,
			},
			Title:          application.Vacancy.Title,
			Deadline:       application.Vacancy.Deadline,
			EmploymentForm: application.Vacancy.EmploymentForm,
			EmploymentType: application.Vacancy.EmploymentType,
			Country:        application.Vacancy.Country,
			City:           application.Vacancy.City,
			SalaryMin:      float64(application.Vacancy.SalaryMin),
			SalaryMax:      float64(application.Vacancy.SalaryMax),
			Currency:       application.Vacancy.Currency,
			SalaryPeriod:   application.Vacancy.SalaryPeriod,
			Education:      setNull(application.Vacancy.Education),
			Experience:     application.Vacancy.Experience,
			Description:    application.Vacancy.Description,
			ViewsCount:     application.Vacancy.ViewsCount,
			IsActive:       application.Vacancy.IsActive,
			Slug:           application.Vacancy.Slug,
		},
	}
}

package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
)

// @Security Bearer
// @Router /vacancies/languages/{id} [post]
// @Summary createVacancyLanguage
// @Description While Creating vacancy, create minimum 1 language for vacancy
// @Tags vacancy_language
// @ID CreateVacancyLanguage
// @Accept json
// @Produce json
// @Param id path int true "VacancyID"
// @Param data body models.VacancyLanguages true "Data"
// @Success 201 {object} models.GetAllCompaniesLanguages
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) CreateVacancyLanguage(ctx *gin.Context) {
	var (
		inp       models.VacancyLanguages
		languages pbc.CompanyLanguages
	)
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	for _, v := range inp.Languages {
		languages.Languages = append(languages.Languages, &pbc.CLanguage{
			VacancyId: id,
			Language:  v.Language,
			Level:     v.Level,
		})
	}
	languages2, err := h.grpcClient.CompanyLanguageService().Create(context.Background(), &languages)
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	languages3 := models.GetAllCompaniesLanguages{
		Count: int64(len(languages2.Languages)),
	}
	for _, v := range languages2.Languages {
		languages3.Languages = append(languages3.Languages, &models.VacancyLanguage{
			ID:        v.Id,
			VacancyID: v.VacancyId,
			Language:  v.Language,
			Level:     v.Level,
		})
	}
	ctx.JSON(http.StatusCreated, languages3)
}

// @Router /vacancies/languages [get]
// @Summary getAllVacancyLanguage
// @Description Get All Vacancies Languages by giving in Parametrs Vacancy "id"
// @Tags vacancy_language
// @ID GetAllVacanciesLanguages
// @Accept json
// @Produce json
// @Param data query models.GetAllVacanciesParams false "Data"
// @Success 200 {object} models.GetAllCompaniesLanguages
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllVacanciesLanguages(ctx *gin.Context) {
	param, err := validateGetAllVacanciesParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	languages, err := h.grpcClient.CompanyLanguageService().GetAll(context.Background(), &pbc.GetAllCLanguagesParams{
		Limit:     int32(param.Limit),
		Page:      int32(param.Page),
		VacancyId: param.VacancyID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllCompaniesLanguages{
		Languages: make([]*models.VacancyLanguage, 0),
		Count:     int64(languages.Count),
	}
	for _, v := range languages.Languages {
		s := parseVacancyLanguage(v)
		response.Languages = append(response.Languages, &s)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseVacancyLanguage(language *pbc.CLanguage) models.VacancyLanguage {
	return models.VacancyLanguage{
		ID:        language.Id,
		VacancyID: language.VacancyId,
		Language:  language.Language,
		Level:     language.Level,
	}
}

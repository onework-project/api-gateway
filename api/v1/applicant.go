package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
	"gitlab.com/onework-project/api-gateway/genproto/company_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	ApplicantStepWork      = "work"
	ApplicantStepEducation = "education"
	ApplicantStepAcademic  = "academic_tests"
	ApplicantStepLicence   = "licences_certificates"
	ApplicantStepAward     = "awards"
	ApplicantStepResearch  = "research"
	ApplicantStepCompleted = "completed"
)

// @Security Bearer
// @Router /applicants [post]
// @Summary createApplicant
// @Description Applicant typeda registratsiyadan otgandan keyin applicantni umumiy ma'lumotlari shu api ga yuborilishi shart
// @Tags applicants
// @ID CreateApplicant
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateApplicant true "Data"
// @Success 201 {object} models.Applicant
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicant(ctx *gin.Context) {
	cx, span := h.tracer.Start(ctx, "api.CreateApplicant")
	defer span.End()

	var inp models.CreateOrUpdateApplicant
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	applicant, err := h.grpcClient.ApplicantService().Create(cx, &pba.CreateApplicantReq{
		Id:           payload.UserID,
		FirstName:    inp.FirstName,
		LastName:     inp.LastName,
		Speciality:   inp.Speciality,
		Country:      inp.Country,
		City:         inp.City,
		DateOfBirth:  inp.DateOfBirth,
		InvisibleAge: inp.InvisibleAge,
		PhoneNumber:  inp.PhoneNumber,
		Email:        payload.Email,
		About:        inp.Bio,
		ImageUrl:     inp.ImageUrl,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.ApplicantStepService().Create(cx, &pba.IdRequest{
		Id: payload.UserID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantModel(applicant))
}

// @Router /applicants/{id} [get]
// @Summary getApplicant
// @Description bitta applicantni umumiy ma'lumotini olish uchun uni "id" isi berilish "path" da shart
// @Tags applicants
// @ID GetApplicant
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.Applicant
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetApplicant(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	applicant, err := h.grpcClient.ApplicantService().Get(context.Background(), &pba.IdRequest{
		Id: id,
	})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
			return
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantModel(applicant))
}

// @Security Bearer
// @Router /applicants/{id} [put]
// @Summary updateApplicant
// @Description path da applicantni "id" isi berilishi va update qilinishi kerak bo'lgan fieldlar 'body' da berilishi lozim
// @Tags applicants
// @ID UpdateApplicant
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.CreateOrUpdateApplicant true "Data"
// @Success 200 {object} models.Applicant
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) UpdateApplicant(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	var inp models.CreateOrUpdateApplicant
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	applicant, err := h.grpcClient.ApplicantService().Update(context.Background(), &pba.Applicant{
		Id:           id,
		FirstName:    inp.FirstName,
		LastName:     inp.LastName,
		Speciality:   inp.Speciality,
		Country:      inp.Country,
		City:         inp.City,
		DateOfBirth:  inp.DateOfBirth,
		InvisibleAge: inp.InvisibleAge,
		PhoneNumber:  inp.PhoneNumber,
		About:        inp.Bio,
		ImageUrl:     inp.ImageUrl,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantModel(applicant))
}

// @Security Bearer
// @Router /applicants [put]
// @Summary updateApplicantStepField
// @Description Bu api applicantni aynan qaysi etapligini bilish uchun, ya'ni applicant ma'lumotini toliq to'ldirmasdan chiqib ketsa aynan osha yerdan oxirgi etap ini saqlab qoladi. Va ya'na qaytdan app ga kirganda oxirgi etapidan boshlab ketaveradi.
// @Tags applicants
// @ID UpdateApplicantStepField
// @Accept json
// @Produce json
// @Param step query models.ApplicantStep true "Step"
// @Success 200 {object} models.Applicant
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateApplicantStepField(ctx *gin.Context) {
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	step := ApplicantStepWork
	if ctx.Query("step") != "" {
		step = ctx.Query("step")
	}

	applicant, err := h.grpcClient.ApplicantService().UpdateStepField(context.Background(), &pba.UpdateStepReq{
		Email: payload.Email,
		Arg:   step,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantModel(applicant))
}

// @Security Bearer
// @Router /applicants/{id} [delete]
// @Summary deleteApplicant
// @Description applicant type dagi foydalanuvchi ozini malumot butunlay ochirib tashlamoqchi bolsa, aynan shu api ga murojaat qilishi lozim, applicant "id" isi pathda berilish lozim.
// @Tags applicants
// @ID DeleteApplicant
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) DeleteApplicant(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	_, err = h.grpcClient.UserService().Delete(context.Background(), &auth_service.GetByIdRequest{
		Id: id,
	})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	_, err = h.grpcClient.ApplicantService().Delete(context.Background(), &pba.IdRequest{
		Id: id,
	})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
			return
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	_, err = h.grpcClient.ApplicantStepService().Delete(context.Background(), &pba.IdRequest{Id: id})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
			return
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	_, err = h.grpcClient.CompanyApplicationService().AutoDelete(context.Background(), &company_service.GetOrDeleteApplicationReq{
		Id: id,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete application")
	}
	_, err = h.grpcClient.CompanyBlockedApplicantService().AutoDelete(context.Background(), &company_service.DeleteBlockedApp{
		Id: id,
	})

	if err != nil {
		h.logger.WithError(err).Error("failed to delete blocked applicant")
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Security Bearer
// @Router /applicants [get]
// @Summary getAllApplicant
// @Description getAllApplicant
// @Tags applicants
// @ID GetAllApplicants
// @Accept json
// @Produce json
// @Param params query models.GetAllApplicantsParams false "Params"
// @Success 200 {object} models.GetAllApplicants
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) GetAllApplicants(ctx *gin.Context) {
	params, err := validateGetAllApplicantsParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	applicants, err := h.grpcClient.ApplicantService().GetAll(context.Background(), &pba.GetAllApplicantsParams{
		Limit:      params.Limit,
		Page:       params.Page,
		Languages:  params.Languages,
		Educations: params.Educations,
		Licences:   params.Licences,
		Skills:     params.Skills,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	response := models.GetAllApplicants{
		Applicants: make([]*models.Applicant, 0),
		Count:      applicants.Count,
	}
	for _, v := range applicants.Applicants {
		a := parseApplicantModel(v)
		response.Applicants = append(response.Applicants, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantModel(a *pba.Applicant) models.Applicant {
	return models.Applicant{
		ID:           a.Id,
		FirstName:    a.FirstName,
		LastName:     a.LastName,
		Speciality:   a.Speciality,
		Country:      a.Country,
		City:         a.City,
		DateOfBirth:  a.DateOfBirth,
		InvisibleAge: a.InvisibleAge,
		PhoneNumber:  setNull(a.PhoneNumber),
		Email:        a.Email,
		Bio:          a.About,
		ImageUrl:     setNull(a.ImageUrl),
		LastStep:     a.LastStep,
		CreatedAt:    a.CreatedAt,
		UpdatedAt:    a.UpdatedAt,
		Slug:         a.Slug,
	}
}

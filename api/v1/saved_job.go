package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/saved-jobes/{id} [post]
// @Summary createApplicantSavedJob
// @Description To create saved job give vacancy id in path
// @Tags applicants_saved_job
// @ID CreateApplicantSavedJob
// @Produce json
// @Param id path int true "VacancyId"
// @Success 201 {object} models.ApplicantSavedJob
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicantSavedJob(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	savedJob, err := h.grpcClient.ApplicantSavedJobService().Create(context.Background(), &pba.SavedJob{
		ApplicantId: payload.UserID,
		VacancyId:   id,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantSavedJob(savedJob))
}

// @Security Bearer
// @Router /applicants/saved-jobes/{id} [delete]
// @Summary deleteApplicantSavedJob
// @Description In order to delete saved job give vacancyId
// @Tags applicants_saved_job
// @ID DeleteApplicantSavedJob
// @Produce json
// @Param id path int true "VacancyID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteApplicantSavedJob(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	_, err = h.grpcClient.ApplicantSavedJobService().Delete(context.Background(), &pba.DeleteOrGetReq{
		Id:      id,
		ThingId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /applicants/saved-jobes [get]
// @Summary getAllApplicantSavedJob
// @Description In order to get all applicant's saved jobs give applicant id
// @Tags applicants_saved_job
// @ID GetAllApplicantsSavedJobs
// @Accept json
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsSavedJobs
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsSavedJobs(ctx *gin.Context) {
	param, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	savedJobs, err := h.grpcClient.ApplicantSavedJobService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       param.Limit,
		Page:        param.Page,
		ApplicantId: param.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsSavedJobs{
		SavedJobs: make([]*models.ApplicantSavedJob, 0),
		Count:     savedJobs.Count,
	}
	for _, v := range savedJobs.SavedJobs {
		s := parseApplicantSavedJob(v)
		response.SavedJobs = append(response.SavedJobs, &s)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantSavedJob(savedJob *pba.SavedJob) models.ApplicantSavedJob {
	return models.ApplicantSavedJob{
		ID:          savedJob.Id,
		ApplicantID: savedJob.ApplicantId,
		VacancyID:   savedJob.VacancyId,
		VacancyInfo: models.VacancyInfo{
			CompanyID: savedJob.VacancyInfo.CompanyId,
			CompanyInfo: models.CompanyInfo{
				Image:        savedJob.VacancyInfo.CompanyInfo.ImageUrl,
				CompanyName:  savedJob.VacancyInfo.CompanyInfo.CompanyName,
				Email:        savedJob.VacancyInfo.CompanyInfo.Email,
				PhoneNumber:  savedJob.VacancyInfo.CompanyInfo.PhoneNumber,
				Country:      savedJob.VacancyInfo.CompanyInfo.Country,
				City:         savedJob.VacancyInfo.CompanyInfo.RegionState,
				WorkersCount: savedJob.VacancyInfo.CompanyInfo.WorkersCount,
				Description:  savedJob.VacancyInfo.CompanyInfo.Description,
				CreatedAt:    savedJob.VacancyInfo.CompanyInfo.CreatedAt,
				Slug:         savedJob.VacancyInfo.CompanyInfo.Slug,
			},
			Title:          savedJob.VacancyInfo.Title,
			Deadline:       savedJob.VacancyInfo.Deadline,
			EmploymentForm: savedJob.VacancyInfo.EmploymentForm,
			EmploymentType: savedJob.VacancyInfo.EmploymentType,
			Country:        savedJob.VacancyInfo.Country,
			City:           savedJob.VacancyInfo.City,
			SalaryMin:      float64(savedJob.VacancyInfo.SalaryMin),
			SalaryMax:      float64(savedJob.VacancyInfo.SalaryMax),
			Currency:       savedJob.VacancyInfo.Currency,
			SalaryPeriod:   savedJob.VacancyInfo.SalaryPeriod,
			Education:      savedJob.VacancyInfo.Education,
			Experience:     savedJob.VacancyInfo.Experience,
			Description:    savedJob.VacancyInfo.Description,
			ViewsCount:     savedJob.VacancyInfo.ViewsCount,
			CreatedAt:      savedJob.VacancyInfo.CreatedAt,
			UpdatedAt:      savedJob.VacancyInfo.UpdatedAt,
			Slug:           savedJob.VacancyInfo.Slug,
		},
		CreatedAt: savedJob.CreatedAt,
	}
}

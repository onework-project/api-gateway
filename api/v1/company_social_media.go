package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
)

// @Security Bearer
// @Router /companies/medias [post]
// @Summary createCompanySocialMedia
// @Description After Creating company, Company can add social media
// @Tags companies_social_media
// @ID CreateCompanySocialMedia
// @Accept json
// @Produce json
// @Param data body models.CompanySocialMedias true "Data"
// @Success 201 {object} models.GetAllCompaniesSocialMedias
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) CreateCompanySocialMedia(ctx *gin.Context) {
	var (
		inp          models.ApplicantSocialMedias
		socialMedias pbc.CompanySocialMedias
	)
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	user := h.getUserInfo(ctx)

	for _, v := range inp.SocialMedias {
		socialMedias.SocialMedias = append(socialMedias.SocialMedias, &pbc.ComSocialMedia{
			CompanyId: user.CompanyId,
			Name:      v.Name,
			Url:       v.Url,
		})
	}
	socialMedias2, err := h.grpcClient.CompanySocialMediaService().Create(context.Background(), &socialMedias)
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	socialMedias3 := models.GetAllCompaniesSocialMedias{
		Count: int64(len(socialMedias2.SocialMedias)),
	}
	for _, v := range socialMedias2.SocialMedias {
		socialMedias3.SocialMedias = append(socialMedias3.SocialMedias, &models.CompanySocialMedia{
			ID:        v.Id,
			CompanyID: v.CompanyId,
			Name:      v.Name,
			Url:       v.Url,
		})
	}

	ctx.JSON(http.StatusCreated, socialMedias3)
}

// @Router /companies/medias [get]
// @Summary getAllCompanySocialMedia
// @Description In order to get all social media that are in company. Give company id to get all social medias available
// @Tags companies_social_media
// @ID GetAllCompanySocialMedias
// @Produce json
// @Param params query models.GetAllCompaniesSm false "Param"
// @Success 200 {object} models.GetAllCompaniesSocialMedias
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllCompanySocialMedias(ctx *gin.Context) {
	params, err := validateGetAllCompaniesSm(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	socialMedias, err := h.grpcClient.CompanySocialMediaService().GetAll(context.Background(), &pbc.GetAllSMsParams{
		Limit:     int32(params.Limit),
		Page:      int32(params.Page),
		CompanyId: params.CompanyID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	medias := models.GetAllCompaniesSocialMedias{
		SocialMedias: make([]*models.CompanySocialMedia, 0),
		Count:        int64(socialMedias.Count),
	}
	for _, v := range socialMedias.SocialMedias {
		s := parseCompanySocialMedia(v)
		medias.SocialMedias = append(medias.SocialMedias, &s)
	}

	ctx.JSON(http.StatusOK, medias)
}

func parseCompanySocialMedia(s *pbc.ComSocialMedia) models.CompanySocialMedia {
	return models.CompanySocialMedia{
		ID:        s.Id,
		CompanyID: s.CompanyId,
		Name:      s.Name,
		Url:       s.Url,
	}
}

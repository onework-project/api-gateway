package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
)

// @Security Bearer
// @Router /companies/blocked-applicants/{id} [post]
// @Summary createCompanyBlockedApplicant
// @Description Member type can block applicant while chatting in websocket by giving applicant's id in path
// @Tags company_blocked_applicant
// @ID CreateCompanyBlockedApplicant
// @Produce json
// @Param id path int true "ApplicantId"
// @Success 201 {object} models.BlockedApplicant
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateCompanyBlockedApplicant(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	user := h.getUserInfo(ctx)

	blockedApplicant, err := h.grpcClient.CompanyBlockedApplicantService().Create(context.Background(), &pbc.BlockedApplicant{
		ApplicantId: id,
		CompanyId:   user.CompanyId,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseCompanyBlockedApplicant(blockedApplicant))
}

// @Security Bearer
// @Router /companies/blocked-applicants/{id} [delete]
// @Summary deleteCompanyBlockedApplicant
// @Description Delete Blocked Appicant by giving blocked Applicant "id" not applicant's "id".
// @Tags company_blocked_applicant
// @ID DeleteCompanyBlockedApplicant
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteCompanyBlockedApplicant(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	user := h.getUserInfo(ctx)
	_, err = h.grpcClient.CompanyBlockedApplicantService().Delete(context.Background(), &pbc.BlockedApplIDReq{
		Id:        id,
		CompanyId: user.CompanyId,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /companies/blocked-applicants [get]
// @Summary getAllCompanyBlockedApplicant
// @Description Get All blocked applicants by giving company id
// @Tags company_blocked_applicant
// @ID GetAllCompaniesBlockedApplicants
// @Accept json
// @Produce json
// @Param data query models.GetAllBlockedApplicantsParams false "Data"
// @Success 200 {object} models.GetAllsCompaniesBlockedApplicantsstruct
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllCompaniesBlockedApplicants(ctx *gin.Context) {
	params, err := validateGetAllBlockedApplParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	blockedCompanies, err := h.grpcClient.CompanyBlockedApplicantService().GetAll(context.Background(), &pbc.GetAllBlockedApplsParams{
		Limit:     int32(params.Limit),
		Page:      int32(params.Page),
		CompanyId: int32(params.CompanyID),
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllsCompaniesBlockedApplicantsstruct{
		BlockedApplicants: make([]*models.BlockedApplicant, 0),
		Count:             int64(blockedCompanies.Count),
	}
	for _, v := range blockedCompanies.BlockedApplicants {
		s := parseCompanyBlockedApplicant(v)
		response.BlockedApplicants = append(response.BlockedApplicants, &s)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseCompanyBlockedApplicant(blockedCompany *pbc.BlockedApplicant) models.BlockedApplicant {
	return models.BlockedApplicant{
		ID:          blockedCompany.Id,
		ApplicantID: blockedCompany.ApplicantId,
		CompanyID:   blockedCompany.CompanyId,
		ApplicantInfo: models.Applicant{
			ID:           blockedCompany.ApplicantInfo.Id,
			FirstName:    blockedCompany.ApplicantInfo.FirstName,
			LastName:     blockedCompany.ApplicantInfo.LastName,
			Speciality:   blockedCompany.ApplicantInfo.Speciality,
			Country:      blockedCompany.ApplicantInfo.Country,
			City:         blockedCompany.ApplicantInfo.City,
			DateOfBirth:  blockedCompany.ApplicantInfo.DateOfBirth,
			InvisibleAge: blockedCompany.ApplicantInfo.InvisibleAge,
			PhoneNumber:  setNull(blockedCompany.ApplicantInfo.PhoneNumber),
			Email:        blockedCompany.ApplicantInfo.Email,
			Bio:          blockedCompany.ApplicantInfo.About,
			ImageUrl:     setNull(blockedCompany.ApplicantInfo.ImageUrl),
			LastStep:     blockedCompany.ApplicantInfo.LastStep,
			CreatedAt:    blockedCompany.ApplicantInfo.CreatedAt,
			UpdatedAt:    blockedCompany.ApplicantInfo.UpdatedAt,
			Slug:         blockedCompany.ApplicantInfo.Slug,
		},
		CreatedAt: blockedCompany.CreatedAt,
	}
}

package v1_test

import (
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api"
	"gitlab.com/onework-project/api-gateway/config"
	grpcPkg "gitlab.com/onework-project/api-gateway/pkg/grpc_client"
	"gitlab.com/onework-project/api-gateway/pkg/logging"
)

var (
	router   *gin.Engine
	grpcConn grpcPkg.GrpcClientI
)

func TestMain(m *testing.M) {
	var err error
	cfg := config.Load("./../..")

	logging.Init()
	log := logging.GetLogger()

	grpcConn, err = grpcPkg.New(&cfg, nil)
	if err != nil {
		log.Fatalf("failed to get grpc connection %v", err)
	}

	ginEngine := api.New(&api.RoutetOptions{
		Cfg:        &cfg,
		GrpcClient: grpcConn,
		Logger:     &log,
	})

	router = ginEngine

	os.Exit(m.Run())
}

package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/works [post]
// @Summary createWorkExperience
// @Description This api is for creating work experience employment_types = {'full-time', 'part-time', 'freelance', 'contract', 'internship', 'apprenticeship', 'seasonal'}
// @Tags applicants_work_experience
// @ID CreateWork
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateApplicantWork true "WorkExperienceFields"
// @Success 201 {object} models.ApplicantWork
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateWork(ctx *gin.Context) {
	var req models.CreateOrUpdateApplicantWork
	if err := ctx.ShouldBindJSON(&req); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	work, err := h.grpcClient.ApplicantWorkExperienceService().Create(context.Background(), &pba.WorkExp{
		ApplicantId:    payload.UserID,
		CompanyName:    req.CompanyName,
		WorkPosition:   req.WorkPosition,
		CompanyWebsite: req.CompanyWebsite,
		EmploymentType: req.EmploymentType,
		StartYear:      req.StartYear,
		StartMonth:     req.StartMonth,
		EndYear:        req.EndYear,
		EndMonth:       req.EndMonth,
		Description:    req.Description,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.ApplicantService().UpdateStepField(context.Background(), &pba.UpdateStepReq{
		Email: payload.Email,
		Arg:   ApplicantStepWork,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantWork(work))
}

// @Router /applicants/works/{id} [get]
// @Summary getWorkExperience
// @Description This api is for getting work experience info by giving "id"
// @Tags applicants_work_experience
// @ID GetWork
// @Produce json
// @Param id path int true "WorkExperienceID"
// @Success 200 {object} models.ApplicantWork
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetWork(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	work, err := h.grpcClient.ApplicantWorkExperienceService().Get(context.Background(), &pba.GetReq{Id: id})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantWork(work))
}

// @Security Bearer
// @Router /applicants/works/{id} [put]
// @Summary updateWorkExperience
// @Description This api is for updating Applicant Work Experience By giving body fields and "id" of work
// @Tags applicants_work_experience
// @ID UpdateWork
// @Accept json
// @Produce json
// @Param id path int true "WorkExperienceID"
// @Param data body models.CreateOrUpdateApplicantWork true "WorkExperienceFields"
// @Success 200 {object} models.ApplicantWork
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateWork(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var req models.CreateOrUpdateApplicantWork
	if err := ctx.ShouldBindJSON(&req); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	work, err := h.grpcClient.ApplicantWorkExperienceService().Update(context.Background(), &pba.WorkExp{
		Id:             id,
		ApplicantId:    payload.UserID,
		CompanyName:    req.CompanyName,
		WorkPosition:   req.WorkPosition,
		CompanyWebsite: req.CompanyWebsite,
		EmploymentType: req.EmploymentType,
		StartYear:      req.StartYear,
		StartMonth:     req.StartMonth,
		EndYear:        req.EndYear,
		EndMonth:       req.EndMonth,
		Description:    req.Description,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantWork(work))
}

// @Security Bearer
// @Router /applicants/works/{id} [delete]
// @Summary deleteWorkExperience
// @Description This api is for deleting applicant work experience by giving "id" in path
// @Tags applicants_work_experience
// @ID DeleteWork
// @Produce json
// @Param id path int true "WorkExperienceID"
// @Success 200 {object} models.ApplicantWork
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteWork(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	_, err = h.grpcClient.ApplicantWorkExperienceService().Delete(context.Background(), &pba.DeleteOrGetReq{
		Id:      id,
		ThingId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /applicants/works [get]
// @Summary getAllWorkExperiences
// @Description This api is for getting all work experience of applicant
// @Tags applicants_work_experience
// @ID GetAllWorks
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Parameters"
// @Success 200 {object} models.GetAllApplicantsWorks
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllWorks(ctx *gin.Context) {
	params, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	works, err := h.grpcClient.ApplicantWorkExperienceService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       params.Limit,
		Page:        params.Page,
		ApplicantId: params.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsWorks{
		Works: make([]*models.ApplicantWork, 0),
		Count: works.Count,
	}
	for _, v := range works.WorkExps {
		a := parseApplicantWork(v)
		response.Works = append(response.Works, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantWork(work *pba.WorkExp) models.ApplicantWork {
	return models.ApplicantWork{
		ID:             work.Id,
		ApplicantID:    work.ApplicantId,
		CompanyName:    work.CompanyName,
		WorkPosition:   work.WorkPosition,
		CompanyWebsite: setNull(work.CompanyWebsite),
		EmploymentType: work.EmploymentType,
		StartYear:      work.StartYear,
		StartMonth:     work.StartMonth,
		EndYear:        setNull(work.EndYear),
		EndMonth:       setNull(work.EndMonth),
		Description:    setNull(work.Description),
	}
}

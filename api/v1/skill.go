package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/skills [post]
// @Summary createApplicantSkill
// @Description Create Applicant Skill minimum 1
// @Tags applicants_skill
// @ID CreateApplicantSkill
// @Accept json
// @Produce json
// @Param data body models.ApplicantSkills true "Data"
// @Success 201 {object} models.ApplicantSkills
// @Failure 500 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) CreateApplicantSkill(ctx *gin.Context) {
	var (
		inp    models.ApplicantSkills
		skills pba.ApplicantSkills
	)
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	for _, v := range inp.Skills {
		skills.Skills = append(skills.Skills, &pba.Skill{
			ApplicantId: payload.UserID,
			Name:        v.Skill,
		})
	}
	skills2, err := h.grpcClient.ApplicantSkillService().Create(context.Background(), &skills)
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	skills3 := models.GetAllApplicantsSkills{
		Count: int64(len(skills2.Skills)),
	}
	for _, v := range skills2.Skills {
		skills3.Skills = append(skills3.Skills, &models.ApplicantSkill{
			ID:          v.Id,
			ApplicantID: v.ApplicantId,
			Skill:       v.Name,
		})
	}
	ctx.JSON(http.StatusCreated, skills3)
}

// @Router /applicants/skills [get]
// @Summary getAllApplicantSkill
// @Description In order to get all applicant's skills give applicant id
// @Tags applicants_skill
// @ID GetAllApplicantsSkills
// @Accept json
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsSkills
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsSkills(ctx *gin.Context) {
	param, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	skills, err := h.grpcClient.ApplicantSkillService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       param.Limit,
		Page:        param.Page,
		ApplicantId: param.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsSkills{
		Skills: make([]*models.ApplicantSkill, 0),
		Count:  skills.Count,
	}
	for _, v := range skills.Skills {
		s := parseApplicantSkill(v)
		response.Skills = append(response.Skills, &s)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantSkill(skill *pba.Skill) models.ApplicantSkill {
	return models.ApplicantSkill{
		ID:          skill.Id,
		ApplicantID: skill.ApplicantId,
		Skill:       skill.Name,
	}
}

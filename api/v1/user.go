package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
	"gitlab.com/onework-project/api-gateway/genproto/chat_service"
	"gitlab.com/onework-project/api-gateway/genproto/company_service"
)

// @Security Bearer
// @Router /users/create [post]
// @Summary Create Users
// @Description Create Superadmin here
// @Tags users
// @ID CreateUser
// @Accept json
// @Produce json
// @Param data body models.CreateUserReq true "Data"
// @Success 201 {object} models.UserRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) CreateUser(ctx *gin.Context) {
	var inp models.CreateUserReq
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	user, err := h.grpcClient.UserService().Create(context.Background(), &auth_service.CreateUser{
		Email:    inp.Email,
		Password: inp.Password,
		Type:     UserTypeSuperAdmin,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseUserModel(user))
}

// @Security Bearer
// @Router /users/{id} [get]
// @Summary Get Users
// @Description Get User info here by giving "id"
// @Tags users
// @ID GetUser
// @Produce json
// @Param id  path int true "ID"
// @Success 200 {object} models.UserRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetUser(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	user, err := h.grpcClient.UserService().Get(context.Background(), &auth_service.GetByIdRequest{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseUserModel(user))
}

// @Security Bearer
// @Router /users/by-email [get]
// @Summary Get User By Email
// @Description Get User info by email
// @Tags users
// @ID GetByEmail
// @Accept json
// @Produce json
// @Param email  query models.UpdateOrGetEmailReq true "UserEmail"
// @Success 200 {object} models.UserRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetByEmail(ctx *gin.Context) {
	email := ctx.Query("email")
	if email == "" {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	user, err := h.grpcClient.UserService().GetByEmail(context.Background(), &auth_service.GetByEmailRequest{
		Email: email,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseUserModel(user))
}

// @Security Bearer
// @Router /users/{id} [delete]
// @Summary Delete Users
// @Description Superadmin And Member Can delete his account fully from this api
// @Tags users
// @ID DeleteUser
// @Produce json
// @Param id  path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) DeleteUser(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	user, err := h.grpcClient.UserService().Get(context.Background(), &auth_service.GetByIdRequest{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}
	if payload.UserType == UserTypeSuperAdmin {
		_, err = h.grpcClient.UserService().Delete(context.Background(), &auth_service.GetByIdRequest{
			Id: id,
		})
		if err != nil {
			errorHandler(err, ctx)
			return
		}

		ctx.JSON(http.StatusOK, models.ResponseSuccess{
			Success: Deleted,
		})
		return
	}
	members, err := h.grpcClient.CompanyMemberService().GetAllMembers(context.Background(), &company_service.GetAllMembersParams{
		Limit:     1000,
		Page:      1,
		CompanyId: user.CompanyId,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	if len(members.Members) == 1 {
		_, err = h.grpcClient.CompanyService().Delete(context.Background(), &company_service.CompanyIDReq{Id: user.CompanyId})
		if err != nil {
			errorHandler(err, ctx)
			return
		}
	} else {
		var adminId int64
		for _, v := range members.Members {
			if v.UserType == UserTypeAdmin {
				adminId = v.MemberId
				break
			}
		}
		_, err = h.grpcClient.CompanyMemberService().ExchangeMemberType(context.Background(), &company_service.ExchangeType{
			OwnerId: user.Id,
			AdminId: adminId,
		})
		if err != nil {
			errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
			return
		}
		_, err = h.grpcClient.CompanyMemberService().DeleteMember(context.Background(), &company_service.MemberId{Id: user.Id})
		if err != nil {
			errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
			return
		}
	}

	_, err = h.grpcClient.UserService().Delete(context.Background(), &auth_service.GetByIdRequest{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}
	_, err = h.grpcClient.ChatService().AutoDelete(context.Background(), &chat_service.AutoDeleteIdReq{UserId: user.Id})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete chat")
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Security Bearer
// @Router /users [get]
// @Summary Get All Users
// @Description Get All Users info
// @Tags users
// @ID GetAllUsers
// @Accept json
// @Produce json
// @Param data  query models.GetAllParams false "Data"
// @Success 200 {object} models.GetAllUsersResponse
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) GetAllUsers(ctx *gin.Context) {
	params, err := validateGetAllParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	users, err := h.grpcClient.UserService().GetAll(context.Background(), &auth_service.GetAllUsersRequest{
		Limit:  int32(params.Limit),
		Page:   int32(params.Page),
		Search: params.Search,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	resp := models.GetAllUsersResponse{
		Users: make([]*models.UserRes, 0),
		Count: int64(users.Count),
	}
	for _, v := range users.Users {
		user := parseUserModel(v)
		resp.Users = append(resp.Users, &user)
	}

	ctx.JSON(http.StatusOK, resp)
}

func parseUserModel(user *auth_service.User) models.UserRes {
	return models.UserRes{
		ID:        user.Id,
		Email:     user.Email,
		Type:      user.Type,
		CompanyID: user.CompanyId,
		Language:  user.Language,
		CreatedAt: user.CreatedAt,
	}
}

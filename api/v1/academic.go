package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/academics [post]
// @Summary createApplicantAcademic
// @Description If applicant has academic, than applicant should fill this fields.
// @Tags applicants_academic
// @ID CreateApplicantAcademic
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateApplicantAcademic true "Data"
// @Success 201 {object} models.ApplicantAcademic
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicantAcademic(ctx *gin.Context) {
	var inp models.CreateOrUpdateApplicantAcademic
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	academic, err := h.grpcClient.ApplicantAcademicService().Create(context.Background(), &pba.Academic{
		TestName:     inp.TestName,
		Organization: inp.Organization,
		Score:        float32(inp.Score),
		PdfUrl:       inp.PdfUrl,
		ApplicantId:  payload.UserID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.ApplicantService().UpdateStepField(context.Background(), &pba.UpdateStepReq{
		Email: payload.Email,
		Arg:   ApplicantStepAcademic,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantAcademic(academic))
}

// @Router /applicants/academics/{id} [get]
// @Summary getApplicantAcademic
// @Description Give Applicant's Academic "id" in table.
// @Tags applicants_academic
// @ID GetApplicantAcademic
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ApplicantAcademic
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetApplicantAcademic(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	academic, err := h.grpcClient.ApplicantAcademicService().Get(context.Background(), &pba.GetReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantAcademic(academic))
}

// @Security Bearer
// @Router /applicants/academics/{id} [put]
// @Summary updateApplicantAcademic
// @Description Applicantni  academic "id" isi path dat berilsin va bodyda update qilinishi kerak bolgan fieldlar
// @Tags applicants_academic
// @ID UpdateApplicantAcademic
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.CreateOrUpdateApplicantAcademic true "Data"
// @Success 200 {object} models.ApplicantAcademic
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateApplicantAcademic(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.CreateOrUpdateApplicantAcademic
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	academic, err := h.grpcClient.ApplicantAcademicService().Update(context.Background(), &pba.Academic{
		Id:           id,
		ApplicantId:  payload.UserID,
		TestName:     inp.TestName,
		Organization: inp.Organization,
		Score:        float32(inp.Score),
		PdfUrl:       inp.PdfUrl,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantAcademic(academic))
}

// @Security Bearer
// @Router /applicants/academics/{id} [delete]
// @Summary deleteApplicantAcademic
// @Description Academicni o'chirish uchun, academicni "id" isini berish shart
// @Tags applicants_academic
// @ID DeleteApplicantAcademic
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteApplicantAcademic(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	_, err = h.grpcClient.ApplicantAcademicService().Delete(context.Background(), &pba.DeleteOrGetReq{
		Id:      id,
		ThingId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /applicants/academics [get]
// @Summary getAllApplicantAcademic
// @Description Parametrlarda limit va page ni berish kerak, va ayni bitta applicant ni academic larini olish uchun bolsa applicant "id" fieldga applicantni "id" isini berish lozim.
// @Tags applicants_academic
// @ID GetAllApplicantsAcademics
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsAcademics
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsAcademics(ctx *gin.Context) {
	params, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	academics, err := h.grpcClient.ApplicantAcademicService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       params.Limit,
		Page:        params.Page,
		ApplicantId: params.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsAcademics{
		Academics: make([]*models.ApplicantAcademic, 0),
		Count:     academics.Count,
	}
	for _, v := range academics.Academics {
		a := parseApplicantAcademic(v)
		response.Academics = append(response.Academics, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantAcademic(a *pba.Academic) models.ApplicantAcademic {
	return models.ApplicantAcademic{
		ID:           a.Id,
		ApplicantID:  a.ApplicantId,
		TestName:     a.TestName,
		Organization: a.Organization,
		Score:        float64(a.Score),
		PdfUrl:       setNull(a.PdfUrl),
	}
}

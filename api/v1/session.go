package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
)

// @Security Bearer
// @Router /sessions/block [put]
// @Summary blockSession
// @Description Superadmin can block session with user email
// @Tags session
// @ID BlockSession
// @Accept json
// @Produce json
// @Param data body models.SessionEmail true "SessionEmail"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) BlockSession(ctx *gin.Context) {
	var session models.SessionEmail
	if err := ctx.ShouldBindJSON(&session); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	_, err := h.grpcClient.SessionService().BlockSession(context.Background(), &auth_service.EmailReq{Email: session.Email})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

// @Security Bearer
// @Router /sessions/unblock [put]
// @Summary unblockSession
// @Description Super admin can un block session by giving email
// @Tags session
// @ID UnBlockSession
// @Accept json
// @Produce json
// @Param data body models.SessionEmail true "SessionEmail"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) UnBlockSession(ctx *gin.Context) {
	var session models.SessionEmail
	if err := ctx.ShouldBindJSON(&session); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	_, err := h.grpcClient.SessionService().UnBlockSession(context.Background(), &auth_service.EmailReq{Email: session.Email})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

// @Security Bearer
// @Router /sessions/terminate [post]
// @Summary terminateSession
// @Description Terminate The Session
// @Tags session
// @ID TerminateSession
// @Accept json
// @Produce json
// @Param data body models.SessionUUIDReq true "SessionID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) TerminateSession(ctx *gin.Context) {
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	var inp models.SessionUUIDReq
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	if payload.UserType != UserTypeSuperAdmin {
		sessions, err := h.grpcClient.SessionService().GetAllSessions(context.Background(), &auth_service.GetAllSessionsParamsReq{
			Limit: 1000,
			Page:  1,
			Email: payload.Email,
		})
		if err != nil {
			errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
			return
		}
		if sessions.Count == 0 {
			errResponse(ctx, http.StatusForbidden, ErrNotAllowed)
			return
		}
	}

	_, err = h.grpcClient.SessionService().TerminateSession(context.Background(), &auth_service.UUIDReq{
		Id: inp.ID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

// @Security Bearer
// @Router /sessions [get]
// @Summary Get All Sessions
// @Description Get All Sessions
// @Tags session
// @ID GetAllSessions
// @Accept json
// @Produce json
// @Param data  query models.GetAllSessionsParams false "Data"
// @Success 200 {object} models.GetAllSessions
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllSessions(ctx *gin.Context) {
	params, err := validateGetAllSessionsParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	sessions, err := h.grpcClient.SessionService().GetAllSessions(context.Background(), &auth_service.GetAllSessionsParamsReq{
		Limit: params.Limit,
		Page:  params.Page,
		Email: params.Email,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	resp := models.GetAllSessions{
		Sessions: make([]*models.Session, 0),
		Count:    int64(sessions.Count),
	}
	for _, v := range sessions.Sessions {
		session := models.Session{
			ID:        v.Id,
			Email:     v.Email,
			UserAgent: v.UserAgent,
			ClientIP:  v.ClientIp,
			IsBlocked: v.IsBlocked,
			CreatedAt: v.CreatedAt,
		}
		resp.Sessions = append(resp.Sessions, &session)
	}

	ctx.JSON(http.StatusOK, resp)
}

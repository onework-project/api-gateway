package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/licences [post]
// @Summary createApplicantLicence
// @Description After creating applicant add licence if applicant has last_step after creating will be "licences_certificates"
// @Tags applicants_licence
// @ID CreateApplicantLicence
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateApplicantLicence true "Data"
// @Success 201 {object} models.ApplicantLicence
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicantLicence(ctx *gin.Context) {
	var inp models.CreateOrUpdateApplicantLicence
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	licence, err := h.grpcClient.ApplicantLicenceService().Create(context.Background(), &pba.License{
		Name:         inp.Name,
		Organization: inp.Organization,
		PdfUrl:       inp.PdfUrl,
		ApplicantId:  payload.UserID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.ApplicantService().UpdateStepField(context.Background(), &pba.UpdateStepReq{
		Email: payload.Email,
		Arg:   ApplicantStepLicence,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantLicence(licence))
}

// @Router /applicants/licences/{id} [get]
// @Summary getApplicantLicence
// @Description In order get applicant's licence give licence's id
// @Tags applicants_licence
// @ID GetApplicantLicence
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ApplicantLicence
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetApplicantLicence(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	licence, err := h.grpcClient.ApplicantLicenceService().Get(context.Background(), &pba.GetReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantLicence(licence))
}

// @Security Bearer
// @Router /applicants/licences/{id} [put]
// @Summary updateApplicantLicence
// @Description In order to update applcicant's licence and certificates give licence's id and body to update
// @Tags applicants_licence
// @ID UpdateApplicantLicence
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.CreateOrUpdateApplicantLicence true "Data"
// @Success 200 {object} models.ApplicantLicence
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateApplicantLicence(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.CreateOrUpdateApplicantLicence
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	licence, err := h.grpcClient.ApplicantLicenceService().Update(context.Background(), &pba.License{
		Id:           id,
		ApplicantId:  payload.UserID,
		Name:         inp.Name,
		Organization: inp.Organization,
		PdfUrl:       inp.PdfUrl,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantLicence(licence))
}

// @Security Bearer
// @Router /applicants/licences/{id} [delete]
// @Summary deleteApplicantLicence
// @Description In order to delete applicant licence give licence's id
// @Tags applicants_licence
// @ID DeleteApplicantLicence
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteApplicantLicence(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	_, err = h.grpcClient.ApplicantLicenceService().Delete(context.Background(), &pba.DeleteOrGetReq{
		Id:      id,
		ThingId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /applicants/licences [get]
// @Summary getAllApplicantLicence
// @Description getAllApplicantLicence
// @Tags applicants_licence
// @ID GetAllApplicantsLicences
// @Accept json
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsLicences
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsLicences(ctx *gin.Context) {
	params, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	licences, err := h.grpcClient.ApplicantLicenceService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       params.Limit,
		Page:        params.Page,
		ApplicantId: params.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsLicences{
		Licences: make([]*models.ApplicantLicence, 0),
		Count:    licences.Count,
	}
	for _, v := range licences.Licenses {
		a := parseApplicantLicence(v)
		response.Licences = append(response.Licences, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantLicence(a *pba.License) models.ApplicantLicence {
	return models.ApplicantLicence{
		ID:           a.Id,
		ApplicantID:  a.ApplicantId,
		Name:         a.Name,
		Organization: a.Organization,
		PdfUrl:       setNull(a.PdfUrl),
	}
}

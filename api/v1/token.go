package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// @Router /token/refresh [post]
// @Summary Refresh Token
// @Description Register Token
// @Tags refresh_token
// @ID RefreshToken
// @Accept json
// @Produce json
// @Param data body models.RenewRefreshTokenReq true "Data"
// @Success 200 {object} models.AccessTokenRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) RefreshToken(ctx *gin.Context) {
	var req models.RenewRefreshTokenReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		errResponse(ctx, http.StatusBadRequest, err)
		return
	}
	payload, err := h.grpcClient.AuthService().RefreshToken(context.Background(), &auth_service.RefreshTokenReq{
		RefreshToken: req.RefreshToken,
	})
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.Unauthenticated:
				errResponse(ctx, http.StatusUnauthorized, err)
				return
			case codes.NotFound:
				errResponse(ctx, http.StatusNotFound, err)
				return
			case codes.Unavailable:
				errResponse(ctx, http.StatusForbidden, err)
				return
			}
		}
		errResponse(ctx, http.StatusInternalServerError, err)
		return
	}
	ctx.JSON(http.StatusOK, models.AccessTokenRes{
		AccessToken: payload.AccessToken,
	})
}

// TODO: Add endpoint that renewes refresh token after 14 days
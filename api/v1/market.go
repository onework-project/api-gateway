package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
)

// @Security Bearer
// @Router /vacancies/markets/{id} [post]
// @Summary createVacancyMarket
// @Description While creating vacancy, also create market minimum one
// @Tags vacancy_market
// @ID CreateVacancyMarket
// @Accept json
// @Produce json
// @Param id path int true "VacancyID"
// @Param data body models.VacancyMarkets true "Data"
// @Success 201 {object} models.VacancyMarkets
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) CreateVacancyMarket(ctx *gin.Context) {
	var (
		inp     models.VacancyMarkets
		markets pbc.VacancyMarkets
	)
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	for _, v := range inp.Markets {
		markets.Markets = append(markets.Markets, &pbc.Market{
			VacancyId: id,
			Market:    v.Market,
		})
	}
	markets2, err := h.grpcClient.CompanyMarketService().Create(context.Background(), &markets)
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	markets3 := models.GetAllMarketsRes{
		Count: int64(len(markets2.Markets)),
	}
	for _, v := range markets2.Markets {
		markets3.Markets = append(markets3.Markets, &models.VacancyMarket{
			ID:        v.Id,
			VacancyID: v.VacancyId,
			Market:    v.Market,
		})
	}
	ctx.JSON(http.StatusCreated, markets3)
}

// @Router /vacancies/markets [get]
// @Summary getAllvacancieMarket
// @Description In order to get all vacancy markets give limit, page and vacancy id
// @Tags vacancy_market
// @ID GetAllVacanciesMarkets
// @Accept json
// @Produce json
// @Param data query models.GetAllVacanciesParams false "Data"
// @Success 200 {object} models.GetAllMarketsRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllVacanciesMarkets(ctx *gin.Context) {
	param, err := validateGetAllVacanciesParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	markets, err := h.grpcClient.CompanyMarketService().GetAll(context.Background(), &pbc.GetAllMarketsParams{
		Limit:     int32(param.Limit),
		Page:      int32(param.Page),
		VacancyId: param.VacancyID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllMarketsRes{
		Markets: make([]*models.VacancyMarket, 0),
		Count:   int64(markets.Count),
	}
	for _, v := range markets.Markets {
		s := parseVacancyMarket(v)
		response.Markets = append(response.Markets, &s)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseVacancyMarket(market *pbc.Market) models.VacancyMarket {
	return models.VacancyMarket{
		ID:        market.Id,
		VacancyID: market.VacancyId,
		Market:    market.Market,
	}
}

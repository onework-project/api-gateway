package v1

import (
	"context"
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/applicant_service"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
	"gitlab.com/onework-project/api-gateway/genproto/company_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handlerV1) GetApplicationInfo(res *models.ApplicantProfileRes, applicantId int64) {
	var wg sync.WaitGroup
	wg.Add(12)
	// * getting all academics of applicant
	go func() {
		defer wg.Done()
		academics, err := h.grpcClient.ApplicantAcademicService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all academics")
		}
		for _, v := range academics.Academics {
			a := parseApplicantAcademic(v)
			res.Application.AcademicTests = append(res.Application.AcademicTests, &a)
		}
	}()
	// * getting all work experience of applicant
	go func() {
		defer wg.Done()
		workExperiences, err := h.grpcClient.ApplicantWorkExperienceService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all work exeperiences")
		}
		for _, v := range workExperiences.WorkExps {
			a := parseApplicantWork(v)
			res.Application.WorkExperiences = append(res.Application.WorkExperiences, &a)
		}
	}()
	// * getting all awards of applicant
	go func() {
		defer wg.Done()
		awards, err := h.grpcClient.ApplicantAwardService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all awards")
		}
		for _, v := range awards.Awards {
			a := parseApplicantAward(v)
			res.Application.Awards = append(res.Application.Awards, &a)
		}
	}()
	// * getting all educations of applicant
	go func() {
		defer wg.Done()
		educations, err := h.grpcClient.ApplicantEducationService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all awards")
		}
		for _, v := range educations.Educations {
			e := parseApplicantEducation(v)
			res.Application.Educations = append(res.Application.Educations, &e)
		}
	}()
	// * getting all lanugages of applicant
	go func() {
		defer wg.Done()
		languages, err := h.grpcClient.ApplicantLanguageService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all awards")
		}
		for _, v := range languages.Languages {
			a := parseApplicantLanguage(v)
			res.Application.Languages = append(res.Application.Languages, &a)
		}
	}()

	// * getting all licences of applicant
	go func() {
		defer wg.Done()
		licences, err := h.grpcClient.ApplicantLicenceService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all awards")
		}
		for _, v := range licences.Licenses {
			a := parseApplicantLicence(v)
			res.Application.Licences = append(res.Application.Licences, &a)
		}
	}()

	// * getting all researches of applicant
	go func() {
		defer wg.Done()
		researches, err := h.grpcClient.ApplicantResearchService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all awards")
		}
		for _, v := range researches.Researches {
			a := parseApplicantResearch(v)
			res.Application.Researches = append(res.Application.Researches, &a)
		}
	}()

	// * getting all skills of applicant
	go func() {
		defer wg.Done()
		skills, err := h.grpcClient.ApplicantSkillService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all awards")
		}
		for _, v := range skills.Skills {
			a := parseApplicantSkill(v)
			res.Application.Skills = append(res.Application.Skills, &a)
		}
	}()

	// * getting all social_medias of applicant
	go func() {
		defer wg.Done()
		socialMedia, err := h.grpcClient.ApplicantSocialMediaService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all awards")
		}
		for _, v := range socialMedia.SocialMedias {
			a := parseApplicantSocialMedia(v)
			res.User.SocialMedias = append(res.User.SocialMedias, &a)
		}
	}()

	// * getting all blocked organizations of applicant
	go func() {
		defer wg.Done()
		blocked, err := h.grpcClient.ApplicantBlockedCompanyService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       100,
			Page:        1,
			ApplicantId: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all awards")
		}
		for _, v := range blocked.BlockedCompanies {
			a := parseApplicantBlockedCompany(v)
			res.User.BlockedOrganizations = append(res.User.BlockedOrganizations, &a)
		}
	}()

	// * getting applicant checked steps
	go func() {
		defer wg.Done()
		step, err := h.grpcClient.ApplicantStepService().Get(context.Background(), &applicant_service.IdRequest{
			Id: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get applicant checked step")
		} else {
			res.Application.CheckedSteps = models.Step{
				ID:           step.Id,
				ApplicantID:  step.ApplicantId,
				GeneralInfo:  step.GeneralInfo,
				Work:         step.Work,
				Education:    step.Education,
				AcademicTest: step.AcademicTest,
				Licence:      step.LicenceCertificates,
				Award:        step.Award,
				Research:     step.Research,
			}
		}
	}()

	// *getting applicant info from applicants table
	go func() {
		defer wg.Done()
		applicant, err := h.grpcClient.ApplicantService().Get(context.Background(), &applicant_service.IdRequest{
			Id: applicantId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get applicant info")
		} else {
			user, err := h.grpcClient.UserService().Get(context.Background(), &auth_service.GetByIdRequest{
				Id: applicantId,
			})
			if err != nil {
				h.logger.WithError(err).Error("failed to get user info")
			} else {
				fullName := applicant.FirstName + " " + applicant.LastName
				res.User = models.ApplicantUserRes{
					ID:           applicant.Id,
					FullName:     fullName,
					Bio:          applicant.About,
					LastStep:     applicant.LastStep,
					DateOfBirth:  applicant.DateOfBirth,
					Email:        applicant.Email,
					InvisibleAge: applicant.InvisibleAge,
					City:         applicant.City,
					Country:      applicant.Country,
					ImageUrl:     setNull(applicant.ImageUrl),
					Language:     user.Language,
					Role:         user.Type,
					PhoneNumber:  setNull(applicant.PhoneNumber),
					Slug:         applicant.Slug,
					Profession:   applicant.Speciality,
				}
			}
		}
	}()

	wg.Wait()
}

func (h *handlerV1) GetMemberInfo(res *models.MemberProfileRes, memberId int64) {
	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()
		user, err := h.grpcClient.UserService().Get(context.Background(), &auth_service.GetByIdRequest{
			Id: memberId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get user info")
		} else {
			res.User = models.UserRes{
				ID:        user.Id,
				Email:     user.Email,
				CompanyID: user.CompanyId,
				Type:      user.Type,
				Language:  user.Language,
				CreatedAt: user.CreatedAt,
			}
		}
	}()

	wg.Wait()
}

func (h *handlerV1) GetCompanyInfo(res *models.MemberProfileRes, companyId int64) {
	var wg sync.WaitGroup
	wg.Add(3)

	go func() {
		defer wg.Done()
		company, err := h.grpcClient.CompanyService().Get(context.Background(), &company_service.CompanyIDReq{Id: companyId})
		if err != nil {
			h.logger.WithError(err).Error("failed to get company info:", err)
		} else {
			res.CompanyInfo = &models.Company{
				ID:           company.Id,
				ImageUrl:     company.ImageUrl,
				CompanyName:  company.CompanyName,
				Email:        company.Email,
				PhoneNumber:  setNull(company.PhoneNumber),
				Country:      company.Country,
				City:         company.RegionState,
				WorkersCount: company.WorkersCount,
				Description:  company.Description,
				Posts:        company.Posts,
				CreatedAt:    company.CreatedAt,
				UpdatedAt:    company.UpdatedAt,
				Slug:         company.Slug,
			}
		}
	}()

	go func() {
		defer wg.Done()
		blockedApplicants, err := h.grpcClient.CompanyBlockedApplicantService().GetAll(context.Background(), &company_service.GetAllBlockedApplsParams{
			Limit:     10000,
			Page:      1,
			CompanyId: int32(companyId),
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get all blocked appplicants")
		}
		for _, v := range blockedApplicants.BlockedApplicants {
			res.Company.BlockedApplicants = append(res.Company.BlockedApplicants, &models.BlockedApplicant{
				ID:          v.Id,
				ApplicantID: v.ApplicantId,
				CompanyID:   v.CompanyId,
				ApplicantInfo: models.Applicant{
					ID:           v.ApplicantInfo.Id,
					FirstName:    v.ApplicantInfo.FirstName,
					LastName:     v.ApplicantInfo.LastName,
					Speciality:   v.ApplicantInfo.Speciality,
					Country:      v.ApplicantInfo.Country,
					City:         v.ApplicantInfo.City,
					DateOfBirth:  v.ApplicantInfo.DateOfBirth,
					InvisibleAge: v.ApplicantInfo.InvisibleAge,
					PhoneNumber:  setNull(v.ApplicantInfo.PhoneNumber),
					Email:        v.ApplicantInfo.Email,
					Bio:          v.ApplicantInfo.About,
					ImageUrl:     setNull(v.ApplicantInfo.ImageUrl),
					LastStep:     v.ApplicantInfo.LastStep,
					CreatedAt:    v.ApplicantInfo.CreatedAt,
					UpdatedAt:    v.ApplicantInfo.UpdatedAt,
					Slug:         v.ApplicantInfo.Slug,
				},
				CreatedAt: v.CreatedAt,
			})
		}
	}()
	go func() {
		defer wg.Done()
		members, err := h.grpcClient.CompanyMemberService().GetAllMembers(context.Background(), &company_service.GetAllMembersParams{
			Limit:     10000,
			Page:      1,
			CompanyId: companyId,
		})
		if err != nil {
			h.logger.WithError(err).Error("failed to get member")
		}

		for _, v := range members.Members {
			if res.User.ID == v.MemberId {
				res.UserCompanyType = setNull(v.UserType)
			}
			res.Company.Members = append(res.Company.Members, &models.MemberRes{
				ID:        v.Id,
				CompanyID: v.CompanyId,
				MemberID:  v.MemberId,
				MemberInfo: models.UserInfo{
					Email:    v.MemberInfo.Email,
					Type:     v.MemberInfo.Type,
					CreateAt: v.MemberInfo.CreatedAt,
				},
				UserType: v.UserType,
			})
		}
	}()

	wg.Wait()
}

func (h *handlerV1) getUserInfo(ctx *gin.Context) *auth_service.User {
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, ErrUnauthorized)
		return nil
	}
	user, err := h.grpcClient.UserService().Get(context.Background(), &auth_service.GetByIdRequest{Id: payload.UserID})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			ctx.AbortWithStatusJSON(http.StatusNotFound, ErrNotFound)
			return nil
		}
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, ErrInternalServer)
		return nil
	}

	return user
}

func (h *handlerV1) GetVacancyInfo(vacancyID int64) *company_service.Vacancy {
	var (
		vacancy *company_service.Vacancy
		err     error
	)
	vacancy, err = h.grpcClient.CompanyVacancyService().Get(context.Background(), &company_service.GetVacancyReq{
		Id: vacancyID,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get vacancy info")
	}

	return vacancy
}

func (h *handlerV1) GetApplicantInfo(applicantId int64) (*applicant_service.GetAllLanguages, *applicant_service.GetAllWorkExps, *applicant_service.GetAllEdus) {
	var (
		wg              sync.WaitGroup
		languages       *applicant_service.GetAllLanguages
		workExperiences *applicant_service.GetAllWorkExps
		educations      *applicant_service.GetAllEdus
		err             error
	)
	_ = err
	wg.Add(3)
	go func() {
		defer wg.Done()
		languages, err = h.grpcClient.ApplicantLanguageService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       1000,
			Page:        1,
			ApplicantId: applicantId,
		})
	}()
	go func() {
		defer wg.Done()
		workExperiences, err = h.grpcClient.ApplicantWorkExperienceService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       1000,
			Page:        1,
			ApplicantId: applicantId,
		})
	}()
	go func() {
		defer wg.Done()
		educations, err = h.grpcClient.ApplicantEducationService().GetAll(context.Background(), &applicant_service.GetAllParams{
			Limit:       1000,
			Page:        1,
			ApplicantId: applicantId,
		})
	}()

	wg.Wait()

	return languages, workExperiences, educations
}

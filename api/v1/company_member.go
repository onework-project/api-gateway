package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
	"gitlab.com/onework-project/api-gateway/genproto/company_service"
)

const (
	UserTypeAdmin = "admin"
	UserTypeOwner = "owner"
)

// @Security Bearer
// @Router /members/{id} [post]
// @Summary addMember
// @Description Companyga Member qoshish uchun member typedagi userni "id" isini kiritishi kerak boladi.
// @Tags member
// @ID AddMember
// @Produce json
// @Param id path int true "MemberID"
// @Success 201 {object} models.Member
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) AddMember(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	res, _ := h.grpcClient.CompanyMemberService().GetMember(context.Background(), &company_service.MemberId{Id: id})
	if res != nil {
		errResponse(ctx, http.StatusBadRequest, ErrMemberExists)
		return
	}

	userInfo := h.getUserInfo(ctx)
	member, err := h.grpcClient.CompanyMemberService().AddMember(context.Background(), &company_service.Member{
		UserType:  UserTypeAdmin,
		CompanyId: userInfo.CompanyId,
		MemberId:  id,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	_, err = h.grpcClient.UserService().AddCompanyId(context.Background(), &auth_service.AddCompanyIdRes{
		Id:        member.MemberId,
		CompanyId: member.CompanyId,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	ctx.JSON(http.StatusCreated, parseMemberModel(member))
}

// @Security Bearer
// @Router /members/{id} [get]
// @Summary getMember
// @Description Get Member will take information about member info
// @Tags member
// @ID GetMember
// @Produce json
// @Param id path int true "MemberID"
// @Success 200 {object} models.Member
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetMember(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	member, err := h.grpcClient.CompanyMemberService().GetMember(context.Background(), &company_service.MemberId{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseMemberModel(member))
}

// @Security Bearer
// @Router /members/{id} [put]
// @Summary exchangeMemberType
// @Description Only Owner can exchange type {types => owner and admin} to admin
// @Tags member
// @ID ExchangeMemberType
// @Produce json
// @Param id path int true "memberID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) ExchangeMemberType(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	if id == payload.UserID {
		errResponse(ctx, http.StatusForbidden, ErrNotAllowed)
		return
	}

	_, err = h.grpcClient.CompanyMemberService().ExchangeMemberType(context.Background(), &company_service.ExchangeType{
		OwnerId: payload.UserID,
		AdminId: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

// @Security Bearer
// @Router /members/{id} [delete]
// @Summary deleteMember
// @Description Only Owner type user in company can delete member.
// @Tags member
// @ID DeleteMember
// @Produce json
// @Param id path int true "memberID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) DeleteMember(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	_, err = h.grpcClient.CompanyMemberService().DeleteMember(context.Background(), &company_service.MemberId{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	_, err = h.grpcClient.UserService().DeleteCompanyID(context.Background(), &auth_service.GetByIdRequest{Id: id})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Security Bearer
// @Router /members [get]
// @Summary getAllMember
// @Description In order to get all members in company give company id in Parameters
// @Tags member
// @ID GetAllMembers
// @Produce json
// @Param params query models.GetAllMembersReq false "memberID"
// @Success 200 {object} models.GetAllMembersRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllMembers(ctx *gin.Context) {
	params, err := validateGetAllCompaniesMembers(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	members, err := h.grpcClient.CompanyMemberService().GetAllMembers(context.Background(), &company_service.GetAllMembersParams{
		Limit:     params.Limit,
		Page:      params.Page,
		CompanyId: params.CompanyID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	response := models.GetAllMembersRes{
		Members: make([]*models.Member, 0),
		Count:   members.Count,
	}
	for _, v := range members.Members {
		m := parseMemberModel(v)
		response.Members = append(response.Members, &m)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseMemberModel(m *company_service.Member) models.Member {
	return models.Member{
		ID:        m.Id,
		CompanyID: m.CompanyId,
		CompanyInfo: models.CompanyInfo{
			Image:        m.CompanyInfo.ImageUrl,
			CompanyName:  m.CompanyInfo.CompanyName,
			Email:        m.CompanyInfo.CompanyName,
			PhoneNumber:  m.CompanyInfo.PhoneNumber,
			Country:      m.CompanyInfo.Country,
			City:         m.CompanyInfo.RegionState,
			WorkersCount: m.CompanyInfo.WorkersCount,
			Description:  m.CompanyInfo.Description,
			CreatedAt:    m.CompanyInfo.CreatedAt,
			Slug:         m.CompanyInfo.Slug,
		},
		MemberID: m.MemberId,
		MemberInfo: models.UserInfo{
			Email:    m.MemberInfo.Email,
			Type:     m.MemberInfo.Type,
			CreateAt: m.MemberInfo.CreatedAt,
		},
		UserType: m.UserType,
	}
}

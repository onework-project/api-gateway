package v1

import (
	"context"
	"mime/multipart"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/onework-project/api-gateway/api/models"
)

type ImageReq struct {
	Image *multipart.FileHeader `form:"image" binding:"required"`
}

type PdfReq struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

type PdfDownloadReq struct {
	PdfUrl string `json:"pdf_url" binding:"required"`
}

type ImageDeleteReq struct {
	ImageUrl string `json:"image_url" binding:"required"`
}

var (
	imageSize = int64(5000000)
	pdfSize   = int64(32000000)
	JpgType   = ".jpg"
	JpegType  = ".jpeg"
	PngType   = ".png"
	PdfType   = ".pdf"
)

// @Security Bearer
// @Router /images [post]
// @Summary Image upload
// @Description Upload Only Images in type {".jpg", ".jpeg", ".png"} formats and image size should be 5 Mg
// @Tags file-upload
// @Produce json
// @Param image formData file true "Image"
// @Success 201 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) UploadImage(ctx *gin.Context) {
	var image ImageReq

	err := ctx.Request.ParseMultipartForm(5 << 20)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	if err = ctx.ShouldBind(&image); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	fileType := filepath.Ext(image.Image.Filename)
	if fileType != JpgType && fileType != JpegType && fileType != PngType {
		errResponse(ctx, http.StatusBadRequest, ErrFileType)
		return
	}
	if image.Image.Size >= imageSize {
		errResponse(ctx, http.StatusBadRequest, ErrSize)
		return
	}

	imageReader, err := image.Image.Open()
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, err)
		return
	}
	defer imageReader.Close()

	id := uuid.New()
	fileName := id.String() + fileType
	uploader := manager.NewUploader(h.cfgAws)

	// upload file to s3
	result, err := uploader.Upload(context.Background(), &s3.PutObjectInput{
		Bucket:      aws.String(h.cfg.AwsBucketOnework),
		Key:         aws.String(h.cfg.AwsBucketFileImages + fileName),
		Body:        imageReader,
		ACL:         "public-read",
		ContentType: aws.String(fileType),
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusCreated, models.ResponseSuccess{
		Success: result.Location,
	})
}

// @Security Bearer
// @Router /pdfs [post]
// @Summary Pdf upload
// @Description Upload Pdf Only that is maximum 32 Mb size
// @Tags file-upload
// @Produce json
// @Param file formData file true "Pdf"
// @Success 201 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) UploadPdf(ctx *gin.Context) {
	var pdf PdfReq

	err := ctx.Request.ParseMultipartForm(32 << 20)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	if err = ctx.ShouldBind(&pdf); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	fileType := filepath.Ext(pdf.File.Filename)
	if fileType != PdfType {
		errResponse(ctx, http.StatusBadRequest, ErrFileType)
		return
	}
	if pdf.File.Size >= pdfSize {
		errResponse(ctx, http.StatusBadRequest, ErrSize)
		return
	}

	fileReader, err := pdf.File.Open()
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, err)
		return
	}
	defer fileReader.Close()

	id := uuid.New()
	fileName := id.String() + fileType
	uploader := manager.NewUploader(h.cfgAws)

	// upload file to s3
	result, err := uploader.Upload(context.Background(), &s3.PutObjectInput{
		Bucket:      aws.String(h.cfg.AwsBucketOnework),
		Key:         aws.String(h.cfg.AwsBucketFilePdfs + fileName),
		Body:        fileReader,
		ACL:         "public-read",
		ContentType: aws.String(fileType),
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusCreated, models.ResponseSuccess{
		Success: result.Location,
	})
}

// @Security Bearer
// @Router /delete/pdfs [delete]
// @Summary Pdf delete
// @Description Delete Pdf By giving pdf url
// @Tags file-upload
// @Accept json
// @Produce json
// @Param pdfUrl body PdfDownloadReq true "PdfUrl"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) DeletePdf(ctx *gin.Context) {
	var req PdfDownloadReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	sl := strings.Split(req.PdfUrl, "/")[4]

	_, err := h.cfgAws.DeleteObject(context.Background(), &s3.DeleteObjectInput{
		Bucket: aws.String(h.cfg.AwsBucketOnework),
		Key:    aws.String(h.cfg.AwsBucketFilePdfs + sl),
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, err)
		return
	}
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, err)
		return
	}
	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Security Bearer
// @Router /delete/images [delete]
// @Summary Image delete
// @Description Delete Image by giving Image url
// @Tags file-upload
// @Accept json
// @Produce json
// @Param image body ImageDeleteReq true "ImageUrl"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) DeleteImage(ctx *gin.Context) {
	var req ImageDeleteReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	sl := strings.Split(req.ImageUrl, "/")[4]

	_, err := h.cfgAws.DeleteObject(context.Background(), &s3.DeleteObjectInput{
		Bucket: aws.String(h.cfg.AwsBucketOnework),
		Key:    aws.String(h.cfg.AwsBucketFileImages + sl),
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, err)
		return
	}
	
	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

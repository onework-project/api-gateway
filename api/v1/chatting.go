package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/chat_service"
)

// @Security Bearer
// @Router /chats/{id} [post]
// @Summary createChat
// @Description Chatni yaratish uchun user id kiritish lozim. Hozir cha faqat member type dagi user chatni yarata oladi applicant bilan.
// @Tags chat
// @ID CreateChat
// @Produce json
// @Param id path int true "UserID"
// @Success 201 {object} models.ChatRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) CreateChat(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	if id == payload.UserID {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	if payload.UserType != UserTypeMember {
		errResponse(ctx, http.StatusForbidden, ErrBadRequest)
		return
	}
	chat, err := h.grpcClient.ChatService().Create(context.Background(), &chat_service.CreateOrDeleteChatReq{
		FId: payload.UserID,
		SId: id,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseChatModel(chat))
}

// @Security Bearer
// @Router /chats/{id} [delete]
// @Summary deleteChat
// @Description Chatni o'chirish uchun chat id sini pathda berish lozim. chat ni ikkila taraf ham o'chira oladi.
// @Tags chat
// @ID DeleteChat
// @Produce json
// @Param id path int true "ChatID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) DeleteChat(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	_, err = h.grpcClient.ChatService().Delete(context.Background(), &chat_service.CreateOrDeleteChatReq{
		FId: id,
		SId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Security Bearer
// @Router /chats/{id} [get]
// @Summary getChat
// @Description Chat ma'lumotini olish uchun chat ni "id" isini path da berish kerak.
// @Tags chat
// @ID GetChat
// @Produce json
// @Param id path int true "ChatID"
// @Success 200 {object} models.ChatRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetChat(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	chat, err := h.grpcClient.ChatService().Get(context.Background(), &chat_service.GetChatIdReq{
		ChatId: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseChatModel(chat))
}

// @Security Bearer
// @Router /chats [get]
// @Summary getAllChat
// @Description Hamma chatlarni olish uchun user ni id isini berish kerak va limit va page ham.
// @Tags chat
// @ID GetAllChat
// @Produce json
// @Param params query models.GetAllChatsParams false "Params"
// @Success 200 {object} models.GetAllChats
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllChat(ctx *gin.Context) {
	params, err := validateGetAllChats(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	chats, err := h.grpcClient.ChatService().GetAll(context.Background(), &chat_service.GetAllChatsParams{
		Limit:  params.Limit,
		Page:   params.Page,
		UserId: params.UserID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	response := models.GetAllChats{
		Chats: make([]*models.ChatRes, 0),
		Count: chats.Count,
	}

	for _, v := range chats.Chats {
		c := parseChatModel(v)
		response.Chats = append(response.Chats, &c)
	}

	ctx.JSON(http.StatusOK, response)
}

// @Security Bearer
// @Router /messages [get]
// @Summary getAllChatMessages
// @Description chatdagi hamma massagelarni olish uchun chat id berilishi kerak. Search da esa chat id ni ichida qilish mumkun massageni topish uchun.
// @Tags chat
// @ID GetAllMessages
// @Produce json
// @Param params query models.GetAllChatMessagesParams false "Params"
// @Success 200 {object} models.GetAllChatsMessages
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllMessages(ctx *gin.Context) {
	params, err := validateGetAllChatsMessages(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	messages, err := h.grpcClient.MessageService().GetAll(context.Background(), &chat_service.GetAllChatsMessagesParams{
		Limit:  params.Limit,
		Page:   params.Page,
		ChatId: params.ChatID,
		Search: params.Search,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	response := models.GetAllChatsMessages{
		Messages: make([]*models.ChatMessage, 0),
		Count:    messages.Count,
	}

	for _, v := range messages.ChatMessages {
		c := parseChatMessageModel(v)
		response.Messages = append(response.Messages, &c)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseChatModel(chat *chat_service.Chat) models.ChatRes {
	return models.ChatRes{
		ID:       chat.Id,
		MemberID: chat.MemberId,
		CompanyInfo: models.ChatCompanyInfo{
			ID:   chat.CompanyInfo.Id,
			Name: chat.CompanyInfo.Name,
		},
		ApplicantID: chat.ApplicantId,
		ApplicantInfo: models.ChatApplicantInfo{
			ID:       chat.ApplicantInfo.Id,
			Fullname: chat.ApplicantInfo.Fullname,
		},
		CreatedAt: chat.CreatedAt,
	}
}

func parseChatMessageModel(m *chat_service.ChatMessage) models.ChatMessage {
	return models.ChatMessage{
		ID:        m.Id,
		ChatID:    m.ChatId,
		UserID:    m.UserId,
		Message:   m.Message,
		CreatedAt: m.CreatedAt,
	}
}

package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
)

// @Security Bearer
// @Router /companies [post]
// @Summary createCompany
// @Description After Member type successfully registered, can add company or join to company as admin.
// @Tags company
// @ID CreateCompany
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateCompany true "Data"
// @Success 201 {object} models.Company
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateCompany(ctx *gin.Context) {
	var inp models.CreateOrUpdateCompany
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		h.logger.WithError(err).Error("failed to bind")
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		h.logger.WithError(err).Error("failed to get payload")
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	company, err := h.grpcClient.CompanyService().Create(context.Background(), &pbc.CreateCompanyReq{
		ImageUrl:     inp.ImageUrl,
		CompanyName:  inp.CompanyName,
		Email:        inp.Email,
		PhoneNumber:  inp.PhoneNumber,
		Country:      inp.Country,
		RegionState:  inp.City,
		WorkersCount: inp.WorkersCount,
		Description:  inp.Description,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.CompanyMemberService().AddMember(context.Background(), &pbc.Member{
		CompanyId: company.Id,
		MemberId:  payload.UserID,
		UserType:  UserTypeOwner,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create member")
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.UserService().AddCompanyId(context.Background(), &auth_service.AddCompanyIdRes{
		Id:        payload.UserID,
		CompanyId: company.Id,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to add company id")
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseCompanyModel(company))
}

// @Router /companies/{id} [get]
// @Summary getCompany
// @Description In order to get company info give company id
// @Tags company
// @ID GetCompany
// @Produce json
// @Param id path int true "CompanyID"
// @Success 200 {object} models.Company
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetCompany(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	Company, err := h.grpcClient.CompanyService().Get(context.Background(), &pbc.CompanyIDReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseCompanyModel(Company))
}

// @Security Bearer
// @Router /companies/{id} [put]
// @Summary updateCompany
// @Description In order to update company info give it's "id" in path and in body fields to update
// @Tags company
// @ID UpdateCompany
// @Accept json
// @Produce json
// @Param id path int true "CompanyID"
// @Param data body models.CreateOrUpdateCompany true "Data"
// @Success 200 {object} models.Company
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) UpdateCompany(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	var inp models.CreateOrUpdateCompany
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	company, err := h.grpcClient.CompanyService().Update(context.Background(), &pbc.Company{
		Id:           id,
		ImageUrl:     inp.ImageUrl,
		CompanyName:  inp.CompanyName,
		Email:        inp.Email,
		PhoneNumber:  inp.PhoneNumber,
		Country:      inp.Country,
		RegionState:  inp.City,
		WorkersCount: inp.WorkersCount,
		Description:  inp.Description,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, parseCompanyModel(company))
}

// @Security Bearer
// @Router /companies/{id} [delete]
// @Summary deleteCompany
// @Description deleteCompany
// @Tags company
// @ID DeleteCompany
// @Accept json
// @Produce json
// @Param id path int true "CompanyID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) DeleteCompany(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	_, err = h.grpcClient.CompanyService().Delete(context.Background(), &pbc.CompanyIDReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /companies [get]
// @Summary getAllCompany
// @Description In order to get all companies info give limit, page and company name or location {city and countary}
// @Tags company
// @ID GetAllCompanies
// @Accept json
// @Produce json
// @Param params query models.GetAllCompaniesParams false "Params"
// @Success 200 {object} models.GetAllCompaniesRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllCompanies(ctx *gin.Context) {
	params, err := validateCompanyGetAllParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	companies, err := h.grpcClient.CompanyService().GetAll(context.Background(), &pbc.GetAllCompaniesParams{
		Limit:       int32(params.Limit),
		Page:        int32(params.Page),
		CompanyName: params.CompanyName,
		Location:    params.Location,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	response := models.GetAllCompaniesRes{
		Companies: make([]*models.Company, 0),
		Count:     int64(companies.Count),
	}
	for _, v := range companies.Companies {
		a := parseCompanyModel(v)
		response.Companies = append(response.Companies, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseCompanyModel(a *pbc.Company) models.Company {
	return models.Company{
		ID:           a.Id,
		ImageUrl:     a.ImageUrl,
		CompanyName:  a.CompanyName,
		Email:        a.Email,
		PhoneNumber:  setNull(a.PhoneNumber),
		Country:      a.Country,
		City:         a.RegionState,
		WorkersCount: a.WorkersCount,
		Description:  a.Description,
		Posts:        a.Posts,
		CreatedAt:    a.CreatedAt,
		UpdatedAt:    a.UpdatedAt,
		Slug:         a.Slug,
	}
}

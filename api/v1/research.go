package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/researches [post]
// @Summary createApplicantResearch
// @Description After creating applicant create research if applicant has, or not just skip it. last_step wil be research after them last_step should be completed make a request to updateapplicantstep api to make last_step = "completed"
// @Tags applicants_research
// @ID CreateApplicantResearch
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateApplicantResearch true "Data"
// @Success 201 {object} models.ApplicantResearch
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicantResearch(ctx *gin.Context) {
	var inp models.CreateOrUpdateApplicantResearch
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	research, err := h.grpcClient.ApplicantResearchService().Create(context.Background(), &pba.Research{
		ApplicantId:      payload.UserID,
		OrganizationName: inp.OrganizationName,
		Position:         inp.Position,
		Description:      inp.Description,
		Supervisor:       inp.SuperViser,
		SupervisorEmail:  inp.SuperViserEmail,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.ApplicantService().UpdateStepField(context.Background(), &pba.UpdateStepReq{
		Email: payload.Email,
		Arg:   ApplicantStepResearch,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantResearch(research))
}

// @Router /applicants/researches/{id} [get]
// @Summary getApplicantResearch
// @Description In order to get applicatn research give research id
// @Tags applicants_research
// @ID GetApplicantResearch
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ApplicantResearch
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetApplicantResearch(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	Research, err := h.grpcClient.ApplicantResearchService().Get(context.Background(), &pba.GetReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantResearch(Research))
}

// @Security Bearer
// @Router /applicants/researches/{id} [put]
// @Summary updateApplicantResearch
// @Description In order to update applicant's research givve research id and body to update
// @Tags applicants_research
// @ID UpdateApplicantResearch
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.CreateOrUpdateApplicantResearch true "Data"
// @Success 200 {object} models.ApplicantResearch
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateApplicantResearch(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.CreateOrUpdateApplicantResearch
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	Research, err := h.grpcClient.ApplicantResearchService().Update(context.Background(), &pba.Research{
		Id:               id,
		ApplicantId:      payload.UserID,
		OrganizationName: inp.OrganizationName,
		Position:         inp.Position,
		Description:      inp.Description,
		Supervisor:       inp.SuperViser,
		SupervisorEmail:  inp.SuperViserEmail,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantResearch(Research))
}

// @Security Bearer
// @Router /applicants/researches/{id} [delete]
// @Summary deleteApplicantResearch
// @Description In order to delete applicant's research give research id
// @Tags applicants_research
// @ID DeleteApplicantResearch
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteApplicantResearch(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	_, err = h.grpcClient.ApplicantResearchService().Delete(context.Background(), &pba.DeleteOrGetReq{
		Id:      id,
		ThingId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /applicants/researches [get]
// @Summary getAllApplicantResearch
// @Description In order to get all applicant's researches give limit, page and applicant id to get all.
// @Tags applicants_research
// @ID GetAllApplicantsResearchs
// @Accept json
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsResearches
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsResearchs(ctx *gin.Context) {
	params, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	researches, err := h.grpcClient.ApplicantResearchService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       params.Limit,
		Page:        params.Page,
		ApplicantId: params.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsResearches{
		Researches: make([]*models.ApplicantResearch, 0),
		Count:      researches.Count,
	}
	for _, v := range researches.Researches {
		a := parseApplicantResearch(v)
		response.Researches = append(response.Researches, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantResearch(a *pba.Research) models.ApplicantResearch {
	return models.ApplicantResearch{
		ID:               a.Id,
		ApplicantID:      a.ApplicantId,
		OrganizationName: a.OrganizationName,
		Position:         a.Position,
		Description:      a.Description,
		SuperViser:       a.Supervisor,
		SuperViserEmail:  a.SupervisorEmail,
	}
}

package v1

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"sync"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
)

// @Security Bearer
// @Router /vacancies [post]
// @Summary createVacancy
// @Description Create Vacancy By giving filling fields of body
// @Tags vacancy
// @ID CreateVacancy
// @Accept json
// @Produce json
// @Param data body models.CreateOrUpdateVacancy true "Data"
// @Success 200 {object} models.Vacancy
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateVacancy(ctx *gin.Context) {
	var inp models.CreateOrUpdateVacancy
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	user := h.getUserInfo(ctx)
	vacancy, err := h.grpcClient.CompanyVacancyService().Create(context.Background(), &pbc.Vacancy{
		CompanyId:      user.CompanyId,
		Title:          inp.Title,
		Deadline:       inp.Deadline,
		EmploymentForm: inp.EmploymentForm,
		EmploymentType: inp.EmploymentType,
		Country:        inp.Country,
		City:           inp.City,
		SalaryMax:      float32(inp.SalaryMax),
		SalaryMin:      float32(inp.SalaryMin),
		Currency:       inp.Currency,
		SalaryPeriod:   inp.SalaryPeriod,
		Education:      inp.Education,
		Experience:     inp.Experience,
		Description:    inp.Description,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create vacancy")
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseVacancyModel(vacancy))
}

// @Router /vacancies/{id} [get]
// @Summary getVacancy
// @Description In order to get vacancy info give vacancy "id"
// @Tags vacancy
// @ID GetVacancy
// @Produce json
// @Param id path int true "VacancyID"
// @Success 200 {object} models.Vacancy
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetVacancy(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	_, err = h.grpcClient.CompanyVacancyService().AddView(context.Background(), &pbc.AddViewReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	vacancy, err := h.grpcClient.CompanyVacancyService().Get(context.Background(), &pbc.GetVacancyReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseVacancyModel(vacancy))
}

// @Router /vacancies/active [get]
// @Summary getAllActiveVacancy
// @Description This Api gets All Active Vacancies by giving parametrs
// @Tags vacancy
// @ID GetAllActiveVacancies
// @Accept json
// @Produce json
// @Param params query models.GetAllVacanciesParamsReq false "Params"
// @Success 200 {object} models.GetAllVacanciesRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllActiveVacancies(ctx *gin.Context) {
	params, err := validateGetAllActiveVacancies(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	log.Println(params)

	vacanies, err := h.grpcClient.CompanyVacancyService().GetAllActive(context.Background(), &pbc.GetAllVacanciesParams{
		Limit:             int32(params.Limit),
		Page:              int32(params.Page),
		CompanyId:         params.CompanyID,
		Title:             params.Title,
		Market:            params.Market,
		Location:          params.Location,
		EmploymentForm:    params.EmploymentForm,
		EmploymentType:    params.EmploymentType,
		TimeRange:         params.TimeRange,
		YearsOfExperience: params.YearsOfExperience,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	response := models.GetAllVacanciesRes{
		Vacancies: make([]*models.Vacancy, 0),
		Count:     int64(vacanies.Count),
	}
	for _, v := range vacanies.Vacancies {
		va := parseVacancyModel(v)
		response.Vacancies = append(response.Vacancies, &va)
	}

	ctx.JSON(http.StatusOK, response)
}

// @Security Bearer
// @Router /vacancies/disactive [get]
// @Summary getAllDisactiveVacancy
// @Description This API is for getting All Disactive vacancies of company by giving "CompanyId" In Parametrs
// @Tags vacancy
// @ID GetAllDisactiveVacancy
// @Accept json
// @Produce json
// @Param params query models.GetAllBlockedApplicantsParams false "Params"
// @Success 200 {object} models.GetAllVacanciesRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllDisactiveVacancy(ctx *gin.Context) {
	params, err := validateGetAllBlockedApplParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	vacanies, err := h.grpcClient.CompanyVacancyService().GetAllDisactive(context.Background(), &pbc.GetAllDisactiveParams{
		Limit:     params.Limit,
		Page:      params.Page,
		CompanyId: params.CompanyID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	response := models.GetAllVacanciesRes{
		Vacancies: make([]*models.Vacancy, 0),
		Count:     int64(vacanies.Count),
	}
	for _, v := range vacanies.Vacancies {
		va := parseVacancyModel(v)
		response.Vacancies = append(response.Vacancies, &va)
	}

	ctx.JSON(http.StatusOK, response)
}

// @Security Bearer
// @Router /vacancies/activate-or-deactivate/{id} [put]
// @Summary activateOrDeactivate
// @Description Company Member Can Activate or Deactivate vacancy
// @Tags vacancy
// @ID ActivateOrDeactivate
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.ActivateOrDeactivateReq true "Data"
// @Success 200 {object} models.Vacancy
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) ActivateOrDeactivate(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.ActivateOrDeactivateReq
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	user := h.getUserInfo(ctx)
	vacancy, err := h.grpcClient.CompanyVacancyService().ActivateOrDisActivate(context.Background(), &pbc.ActivateOrDisactivateReq{
		Id:        id,
		CompanyId: user.CompanyId,
		Smth:      inp.Argument,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, parseVacancyModel(vacancy))
}

// @Security Bearer
// @Router /vacancies/{id} [put]
// @Summary updateVacancy
// @Description Update Vacancy Info
// @Tags vacancy
// @ID UpdateVacancy
// @Accept json
// @Produce json
// @Param id path int true "VacancyID"
// @Param data body models.CreateOrUpdateVacancy false "Data"
// @Success 200 {object} models.Vacancy
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateVacancy(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	var inp models.CreateOrUpdateVacancy
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	user := h.getUserInfo(ctx)

	vacancy, err := h.grpcClient.CompanyVacancyService().Update(context.Background(), &pbc.Vacancy{
		Id:             id,
		CompanyId:      user.CompanyId,
		Title:          inp.Title,
		Deadline:       inp.Deadline,
		EmploymentForm: inp.EmploymentForm,
		EmploymentType: inp.EmploymentType,
		Country:        inp.Country,
		City:           inp.City,
		SalaryMax:      float32(inp.SalaryMax),
		SalaryMin:      float32(inp.SalaryMin),
		Currency:       inp.Currency,
		SalaryPeriod:   inp.SalaryPeriod,
		Education:      inp.Education,
		Experience:     inp.Experience,
		Description:    inp.Description,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, parseVacancyModel(vacancy))
}

// @Security Bearer
// @Router /vacancies/{id} [delete]
// @Summary deleteVacancy
// @Description Delete Vacancy By giving vacancy "id" in path
// @Tags vacancy
// @ID DeleteVacancy
// @Produce json
// @Param id path int true "VacancyID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteVacancy(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	user := h.getUserInfo(ctx)

	_, err = h.grpcClient.CompanyVacancyService().Delete(context.Background(), &pbc.VacancyIDReq{
		Id:        id,
		CompanyId: user.CompanyId,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}
	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

func parseVacancyModel(v *pbc.Vacancy) models.Vacancy {
	vacancy := models.Vacancy{
		ID:        v.Id,
		CompanyID: v.CompanyId,
		CompanyInfo: models.CompanyInfo{
			Image:        v.CompanyInfo.ImageUrl,
			CompanyName:  v.CompanyInfo.CompanyName,
			Email:        v.CompanyInfo.Email,
			PhoneNumber:  v.CompanyInfo.PhoneNumber,
			Country:      v.CompanyInfo.Country,
			City:         v.CompanyInfo.RegionState,
			WorkersCount: v.CompanyInfo.WorkersCount,
			Description:  v.CompanyInfo.Description,
			CreatedAt:    v.CompanyInfo.CreatedAt,
			Slug:         v.CompanyInfo.Slug,
		},
		Title:          v.Title,
		Deadline:       v.Description,
		EmploymentForm: v.EmploymentForm,
		EmploymentType: v.EmploymentType,
		Country:        v.Country,
		City:           v.City,
		SalaryMax:      float64(v.SalaryMax),
		SalaryMin:      float64(v.SalaryMin),
		Currency:       v.Currency,
		SalaryPeriod:   v.SalaryPeriod,
		Education:      setNull(v.Education),
		Experience:     v.Experience,
		Description:    v.Description,
		ViewsCount:     v.ViewsCount,
		IsActive:       v.IsActive,
		CreatedAt:      v.CreatedAt,
		UpdatedAt:      v.UpdatedAt,
		Slug:           v.Slug,
	}

	var wg sync.WaitGroup
	wg.Add(3)

	go func() {
		defer wg.Done()
		for _, j := range v.Skills {
			a := parseVacancySkill(j)
			vacancy.Skills = append(vacancy.Skills, &a)
		}
	}()

	go func() {
		defer wg.Done()
		for _, j := range v.Markets {
			a := parseVacancyMarket(j)
			vacancy.Markets = append(vacancy.Markets, &a)
		}
	}()

	go func() {
		defer wg.Done()
		for _, j := range v.Languages {
			a := parseVacancyLanguage(j)
			vacancy.Languages = append(vacancy.Languages, &a)
		}
	}()

	wg.Wait()

	return vacancy
}

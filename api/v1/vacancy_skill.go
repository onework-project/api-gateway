package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
)

// @Security Bearer
// @Router /vacancies/skills/{id} [post]
// @Summary createVacancySkill
// @Description Create Vacancy Skill while creating vacancy
// @Tags vacancy_skill
// @ID CreateVacancySkill
// @Accept json
// @Produce json
// @Param data body models.VacancySkills true "Data"
// @Param id path int true "VacancyID"
// @Success 201 {object} models.VacancySkills
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) CreateVacancySkill(ctx *gin.Context) {
	var (
		inp    models.VacancySkills
		skills pbc.VacancySkills
	)
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	for _, v := range inp.Skills {
		skills.Skills = append(skills.Skills, &pbc.ComSkill{
			VacancyId: id,
			Name:      v.Skill,
		})
	}
	skills2, err := h.grpcClient.CompanySkillService().Create(context.Background(), &skills)
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	skills3 := models.GetAllVacanciesSkills{
		Count: int64(len(skills2.Skills)),
	}
	for _, v := range skills2.Skills {
		skills3.Skills = append(skills3.Skills, &models.VacancySkill{
			ID:        v.Id,
			VacancyID: v.VacancyId,
			Skill:     v.Name,
		})
	}
	ctx.JSON(http.StatusCreated, skills3)
}

// @Router /vacancies/skills [get]
// @Summary getAllVacancySkill
// @Description Get All vacancy skill by giving vacancy "id"
// @Tags vacancy_skill
// @ID GetAllVacanciesSkills
// @Accept json
// @Produce json
// @Param data query models.GetAllVacanciesParams false "Data"
// @Success 200 {object} models.GetAllApplicantsSkills
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllVacanciesSkills(ctx *gin.Context) {
	param, err := validateGetAllVacanciesParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	skills, err := h.grpcClient.CompanySkillService().GetAll(context.Background(), &pbc.ComGetAllSkillsParams{
		Limit:     int32(param.Limit),
		Page:      int32(param.Page),
		VacancyId: param.VacancyID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllVacanciesSkills{
		Skills: make([]*models.VacancySkill, 0),
		Count:  int64(skills.Count),
	}
	for _, v := range skills.Skills {
		s := parseVacancySkill(v)
		response.Skills = append(response.Skills, &s)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseVacancySkill(skill *pbc.ComSkill) models.VacancySkill {
	return models.VacancySkill{
		ID:        skill.Id,
		VacancyID: skill.VacancyId,
		Skill:     skill.Name,
	}
}

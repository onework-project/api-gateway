package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/awards [post]
// @Summary createApplicantAward
// @Description Create Award For Applicant
// @Tags applicants_award
// @ID CreateApplicantAward
// @Accept json
// @Produce json
// @Param data body models.CreateApplicantAward true "Data"
// @Success 201 {object} models.ApplicantAward
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) CreateApplicantAward(ctx *gin.Context) {
	var inp models.CreateApplicantAward
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	award, err := h.grpcClient.ApplicantAwardService().Create(context.Background(), &pba.Award{
		AwardName:    inp.AwardName,
		Organization: inp.Organization,
		PdfUrl:       inp.PdfUrl,
		ApplicantId:  payload.UserID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	_, err = h.grpcClient.ApplicantService().UpdateStepField(context.Background(), &pba.UpdateStepReq{
		Email: payload.Email,
		Arg:   ApplicantStepAward,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantAward(award))
}

// @Router /applicants/awards/{id} [get]
// @Summary getApplicantAward
// @Description Get Applicant's Award by giving award's "id"
// @Tags applicants_award
// @ID GetApplicantAward
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ApplicantAward
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
func (h *handlerV1) GetApplicantAward(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	award, err := h.grpcClient.ApplicantAwardService().Get(context.Background(), &pba.GetReq{
		Id: id,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantAward(award))
}

// @Security Bearer
// @Router /applicants/awards/{id} [put]
// @Summary updateApplicantAward
// @Description Update Applicant's Award by giving in path award id and in body update fields
// @Tags applicants_award
// @ID UpdateApplicantAward
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param data body models.CreateApplicantAward true "Data"
// @Success 200 {object} models.ApplicantAward
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateApplicantAward(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	var inp models.CreateApplicantAward
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	award, err := h.grpcClient.ApplicantAwardService().Update(context.Background(), &pba.Award{
		Id:           id,
		ApplicantId:  payload.UserID,
		AwardName:    inp.AwardName,
		Organization: inp.Organization,
		PdfUrl:       inp.PdfUrl,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, parseApplicantAward(award))
}

// @Security Bearer
// @Router /applicants/awards/{id} [delete]
// @Summary deleteApplicantAward
// @Description Delete Applicant's Award, Only Applicant can delete his award
// @Tags applicants_award
// @ID DeleteApplicantAward
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteApplicantAward(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	_, err = h.grpcClient.ApplicantAwardService().Delete(context.Background(), &pba.DeleteOrGetReq{
		Id:      id,
		ThingId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /applicants/awards [get]
// @Summary getAllApplicantAward
// @Description Get All Applicant's awards that applicant have by giving applicant "id"
// @Tags applicants_award
// @ID GetAllApplicantsAwards
// @Accept json
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsAwards
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsAwards(ctx *gin.Context) {
	params, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	awards, err := h.grpcClient.ApplicantAwardService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       params.Limit,
		Page:        params.Page,
		ApplicantId: params.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsAwards{
		Awards: make([]*models.ApplicantAward, 0),
		Count:  awards.Count,
	}
	for _, v := range awards.Awards {
		a := parseApplicantAward(v)
		response.Awards = append(response.Awards, &a)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantAward(a *pba.Award) models.ApplicantAward {
	return models.ApplicantAward{
		ID:           a.Id,
		ApplicantID:  a.ApplicantId,
		AwardName:    a.AwardName,
		Organization: a.Organization,
		PdfUrl:       setNull(a.PdfUrl),
	}
}

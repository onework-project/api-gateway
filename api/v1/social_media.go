package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/medias [post]
// @Summary createApplicantSocialMedia
// @Description Create Social Media for Applicant type user -> social_medias {website,linkedin,facebook,github,instagram,telegram,youtube}
// @Tags applicants_social_media
// @ID CreateApplicantSocialMedia
// @Accept json
// @Produce json
// @Param data body models.ApplicantSocialMedias true "Data"
// @Success 201 {object} models.GetAllApplicantSocialMedias
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicantSocialMedia(ctx *gin.Context) {
	var (
		inp          models.ApplicantSocialMedias
		socialMedias pba.ApplicantSocialMedias
	)
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	for _, v := range inp.SocialMedias {
		socialMedias.SocialMedias = append(socialMedias.SocialMedias, &pba.SocialMedia{
			ApplicantId: payload.UserID,
			Name:        v.Name,
			Url:         v.Url,
		})
	}
	socialMedias2, err := h.grpcClient.ApplicantSocialMediaService().Create(context.Background(), &socialMedias)
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	socialMedias3 := models.GetAllApplicantSocialMedias{
		Count: int64(len(socialMedias2.SocialMedias)),
	}
	for _, v := range socialMedias2.SocialMedias {
		socialMedias3.SocialMedias = append(socialMedias3.SocialMedias, &models.ApplicantSocialMedia{
			ID:          v.Id,
			ApplicantID: v.ApplicantId,
			Name:        v.Name,
			Url:         v.Url,
		})
	}

	ctx.JSON(http.StatusCreated, socialMedias3)
}

// @Router /applicants/medias [get]
// @Summary getAllApplicantSocialMedia
// @Description Get All Social Media of Applicant by giving Applicant's "id"
// @Tags applicants_social_media
// @ID GetAllApplicantSocialMedia
// @Produce json
// @Param params query models.GetAllApplicantsThingsParams false "Param"
// @Success 200 {object} models.GetAllApplicantSocialMedias
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantSocialMedias(ctx *gin.Context) {
	params, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	socialMedias, err := h.grpcClient.ApplicantSocialMediaService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       params.Limit,
		Page:        params.Page,
		ApplicantId: params.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	medias := models.GetAllApplicantSocialMedias{
		SocialMedias: make([]*models.ApplicantSocialMedia, 0),
		Count:        socialMedias.Count,
	}
	for _, v := range socialMedias.SocialMedias {
		s := parseApplicantSocialMedia(v)
		medias.SocialMedias = append(medias.SocialMedias, &s)
	}

	ctx.JSON(http.StatusOK, medias)
}

func parseApplicantSocialMedia(s *pba.SocialMedia) models.ApplicantSocialMedia {
	return models.ApplicantSocialMedia{
		ID:          s.Id,
		ApplicantID: s.ApplicantId,
		Name:        s.Name,
		Url:         s.Url,
	}
}

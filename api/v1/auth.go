package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/applicant_service"
	"gitlab.com/onework-project/api-gateway/genproto/auth_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	ErrIncorrectPassword = "incorrect_password"
	ErrIncorrectUserType = "incorrect_user_type"
)

// @Router /auth/sign-up [post]
// @Summary Register member or applicant
// @Description user type = {member, applicant}
// @Tags register
// @ID SignUp
// @Accept json
// @Produce json
// @Param data body models.RegisterRequest true "Data"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) SignUp(ctx *gin.Context) {
	var inp models.RegisterRequest

	if err := ctx.ShouldBindJSON(&inp); err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	if !validatePassword(inp.Password) {
		errResponse(ctx, http.StatusBadRequest, ErrWeakPassword)
		return
	}

	user, _ := h.grpcClient.UserService().GetByEmail(context.Background(), &auth_service.GetByEmailRequest{
		Email: inp.Email,
	})
	if user != nil {
		h.logger.Error("applicant exists")
		errResponse(ctx, http.StatusBadRequest, ErrEmailExists)
		return
	}
	_, err := h.grpcClient.AuthService().Register(context.Background(), &auth_service.RegisterReq{
		Email:    inp.Email,
		Password: inp.Password,
		UserType: inp.UserType,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to sign up")
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

// @Router /auth/login [post]
// @Summary logIn
// @Description Loginda da faqat userni ma'lumoti qaytadi
// @Tags register
// @ID Login
// @Accept json
// @Produce json
// @Param login body models.LoginRequest true "Login"
// @Success 200 {object} models.LoginResponse
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) Login(ctx *gin.Context) {
	var inp models.LoginRequest
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		h.logger.WithError(err).Error("failed to bind JSON")
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	authResponse, err := h.grpcClient.AuthService().Login(context.Background(), &auth_service.LoginReq{
		Email:     inp.Email,
		Password:  inp.Password,
		UserAgent: ctx.Request.UserAgent(),
		ClientIp:  ctx.ClientIP(),
		FcmToken:  inp.FcmToken,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to login")
		if v, ok := status.FromError(err); ok {
			switch v.Code() {
			case codes.InvalidArgument:
				errResponse(ctx, http.StatusBadRequest, ErrWrongEmailOrPassword)
				return
			case codes.NotFound:
				errResponse(ctx, http.StatusBadRequest, ErrWrongEmailOrPassword)
				return
			}
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	user, err := h.grpcClient.UserService().Get(context.Background(), &auth_service.GetByIdRequest{Id: authResponse.UserId})
	if err != nil {
		if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
			errResponse(ctx, http.StatusNotFound, ErrNotFound)
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, models.LoginResponse{
		AccessToken:  authResponse.AccessToken,
		RefreshToken: authResponse.RefreshToken,
		User: models.UserRes{
			ID:        user.Id,
			Email:     user.Email,
			Type:      user.Type,
			CompanyID: user.CompanyId,
			Language:  user.Language,
			CreatedAt: user.CreatedAt,
		},
	})
}

// @Security Bearer
// @Router /auth/logout [post]
// @Summary logOut
// @Description Log out qilish uchun access token bolishi shart, shunda log out boladi,
// @Tags register
// @ID Logout
// @Produce json
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
// @Failure 403 {object} models.ResponseError
func (h *handlerV1) Logout(ctx *gin.Context) {
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		h.logger.WithError(err).Error("failed to get payload in update password")
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	_, err = h.grpcClient.SessionService().Delete(context.Background(), &auth_service.DeleteSessionReq{
		Email:    payload.Email,
		ClientIp: ctx.ClientIP(),
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

// @Router /auth/verify [post]
// @Summary verifyEmail
// @Description Registratsiya qilgandan keyin verification code jonatiladi codeni shu yerga registratsiya dan otgan email bilan birga body da jonatilsin ma'lumot
// @Tags register
// @ID VerifyEmail
// @Accept json
// @Produce json
// @Param data body models.VerifyEmailRequest true "Data"
// @Success 200 {object} models.TokenRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) VerifyEmail(ctx *gin.Context) {
	var inp models.VerifyEmailRequest

	if err := ctx.ShouldBindJSON(&inp); err != nil {
		h.logger.WithError(err).Error("failed to bind JSON")
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	token, err := h.grpcClient.AuthService().VerifyEmail(context.Background(), &auth_service.VerifyReq{
		Email:     inp.Email,
		Code:      inp.Code,
		UserAgent: ctx.Request.UserAgent(),
		ClientIp:  ctx.ClientIP(),
		FcmToken:  inp.FcmToken,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to verify applicant email")
		if s, ok := status.FromError(err); ok {
			switch s.Code() {
			case codes.DeadlineExceeded:
				errResponse(ctx, http.StatusBadRequest, ErrCodeExpired)
				return
			case codes.Unknown:
				errResponse(ctx, http.StatusBadRequest, ErrUnknownUser)
				return
			}
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, models.TokenRes{
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
	})
}

// @Router /password/recovery [post]
// @Summary forgoPasswordRecovery
// @Description Bu apida user login qilginicha kodini esdan chiqarib qoygan bo'lsa yangilab olsa boladi, email kiritilish lozim bodyda
// @ID ForgotPassword
// @Tags password
// @Accept json
// @Produce json
// @Param data body models.EmailRequest true "Data"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) ForgotPassword(ctx *gin.Context) {
	var inp models.EmailRequest

	if err := ctx.ShouldBindJSON(&inp); err != nil {
		h.logger.WithError(err).Error("failed to bind JSON")
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	_, err := h.grpcClient.AuthService().ForgotPassword(context.Background(), &auth_service.EmailRequest{
		Email: inp.Email,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

// @Router /password/verify [post]
// @Summary verifyForgotPassoword
// @Description Forgot passwordga request berib bolib code muvafaqqiyatli yuborilgandan keyin shu yerga email va code ni kiritsin "body" da
// @Tags password
// @ID VerifyForgotPassoword
// @Accept json
// @Produce json
// @Param data body models.VerifyRequest true "Data"
// @Success 200 {object} models.AccessTokenRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) VerifyForgotPassoword(ctx *gin.Context) {
	var inp models.VerifyRequest

	if err := ctx.ShouldBindJSON(&inp); err != nil {
		h.logger.WithError(err).Error("failed to bind JSON")
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	accessToken, err := h.grpcClient.AuthService().VerifyForgotPassword(context.Background(), &auth_service.VerifyReq{
		Email: inp.Email,
		Code:  inp.Code,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to verify applicant email")
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, models.AccessTokenRes{
		AccessToken: accessToken.AccessToken,
	})
}

// @Security Bearer
// @Router /password/update [post]
// @Summary Update password
// @Description Emailni verify qilgandan kegin yangi password kiritsa ham boladi.
// @Tags password
// @ID UpdatePassword
// @Accept json
// @Produce json
// @Param data body models.UpdatePasswordRequest true "Data"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdatePassword(ctx *gin.Context) {
	var inp models.UpdatePasswordRequest

	if err := ctx.ShouldBindJSON(&inp); err != nil {
		h.logger.WithError(err).Error("failed to bind JSON")
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		h.logger.WithError(err).Error("failed to get payload in update password")
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	_, err = h.grpcClient.AuthService().UpdatePassword(context.Background(), &auth_service.UpdatePasswordReq{
		UserId:   payload.UserID,
		Password: inp.Password,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to update password")
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: "updated",
	})
}

// @Security Bearer
// @Router /email/update [put]
// @Summary updateEmail
// @Description Yangi email ni "body" da kiritishi lozim. Keyin kod jo'natiladi osha emailga
// @Tags email
// @ID UpdateEmail
// @Accept json
// @Produce json
// @Param email body models.EmailRequest true "Email"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateEmail(ctx *gin.Context) {
	var email models.EmailRequest
	if err := ctx.ShouldBindJSON(&email); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	res, _ := h.grpcClient.UserService().GetByEmail(context.Background(), &auth_service.GetByEmailRequest{
		Email: email.Email,
	})
	if res != nil {
		errResponse(ctx, http.StatusBadRequest, ErrEmailExists)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	_, err = h.grpcClient.AuthService().UpdateEmail(context.Background(), &auth_service.UpdateEmailReq{
		Id:    payload.UserID,
		Email: email.Email,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Success,
	})
}

// @Security Bearer
// @Router /email/verify [post]
// @Summary verifyUpdateEmail
// @Description Email verify qilish uchun email ni va code ni shu yerga kiritishi kerak va muvaffaqiyatli otsa email ozgaradi va login page ga otvorishi kerak.
// @Tags email
// @ID VerifyUpdateEmail
// @Accept json
// @Produce json
// @Param data body models.VerifyRequest true "Data"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) VerifyUpdateEmail(ctx *gin.Context) {
	var inp models.VerifyRequest

	if err := ctx.ShouldBindJSON(&inp); err != nil {
		h.logger.WithError(err).Error("failed to bind JSON")
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}

	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		h.logger.WithError(err).Error("failed to get payload in update email")
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	user, err := h.grpcClient.AuthService().VerifyUpdateEmail(context.Background(), &auth_service.VerifyEmailReq{
		NewEmail: inp.Email,
		Code:     inp.Code,
		OldEmail: payload.Email,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to verify applicant email")
		if v, ok := status.FromError(err); ok {
			switch v.Code() {
			case codes.DeadlineExceeded:
				errResponse(ctx, http.StatusBadRequest, ErrCodeExpired)
				return
			case codes.Unknown:
				errResponse(ctx, http.StatusBadRequest, ErrIncorrectCode)
				return
			}
		}
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	if user.Type == UserTypeApplicant {
		_, err = h.grpcClient.ApplicantService().UpdateEmail(context.Background(), &applicant_service.UpdateApplicantEmailReq{
			Email: inp.Email,
			Id:    payload.UserID,
		})
		if err != nil {
			errorHandler(err, ctx)
			return
		}
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Login,
	})
}

package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/languages [post]
// @Summary createApplicantLanguage
// @Description After creating Applicant add language, minimum one language required
// @Tags applicants_language
// @ID CreateApplicantLanguage
// @Accept json
// @Produce json
// @Param data body models.Languages true "Data"
// @Success 201 {object} models.GetAllApplicantsLanguages
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicantLanguage(ctx *gin.Context) {
	var (
		inp       models.Languages
		languages pba.Languages
	)
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}
	for _, v := range inp.Languages {
		languages.Languages = append(languages.Languages, &pba.Language{
			ApplicantId: payload.UserID,
			Language:    v.Language,
			Level:       v.Level,
		})
	}
	languages2, err := h.grpcClient.ApplicantLanguageService().Create(context.Background(), &languages)
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	languages3 := models.GetAllApplicantsLanguages{
		Count: int64(len(languages2.Languages)),
	}
	for _, v := range languages2.Languages {
		languages3.Languages = append(languages3.Languages, &models.ApplicantLanguage{
			ID:          v.Id,
			ApplicantID: v.ApplicantId,
			Language:    v.Language,
			Level:       v.Level,
		})
	}
	ctx.JSON(http.StatusCreated, languages3)
}

// @Router /applicants/languages [get]
// @Summary getAllApplicantLanguage
// @Description In order get all Applicant's language give applicant's "id"
// @Tags applicants_language
// @ID GetAllApplicantsLanguages
// @Accept json
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsLanguages
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsLanguages(ctx *gin.Context) {
	param, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	Languages, err := h.grpcClient.ApplicantLanguageService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       param.Limit,
		Page:        param.Page,
		ApplicantId: param.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsLanguages{
		Languages: make([]*models.ApplicantLanguage, 0),
		Count:     Languages.Count,
	}
	for _, v := range Languages.Languages {
		s := parseApplicantLanguage(v)
		response.Languages = append(response.Languages, &s)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantLanguage(language *pba.Language) models.ApplicantLanguage {
	return models.ApplicantLanguage{
		ID:          language.Id,
		ApplicantID: language.ApplicantId,
		Language:    language.Language,
		Level:       language.Level,
	}
}

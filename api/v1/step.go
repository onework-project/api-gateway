package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/steps [put]
// @Summary upfateApplicantStep
// @Description steps = {general_info,work,education,academic_tests,licences_certificates,awards,research}
// @Tags applicants_step
// @ID UpdateApplicantStep
// @Accept json
// @Produce json
// @Param data body models.UpdateApplicantStep true "Data"
// @Success 200 {object} models.Step
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) UpdateApplicantStep(ctx *gin.Context) {
	var inp models.UpdateApplicantStep
	if err := ctx.ShouldBindJSON(&inp); err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	step, err := h.grpcClient.ApplicantStepService().UpdateField(context.Background(), &applicant_service.UpdateStepField{
		Field:       inp.Field,
		Arg:         inp.Step,
		ApplicantId: payload.UserID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, models.Step{
		ID:           step.Id,
		ApplicantID:  step.ApplicantId,
		GeneralInfo:  step.GeneralInfo,
		Work:         step.Work,
		Education:    step.Education,
		AcademicTest: step.AcademicTest,
		Licence:      step.LicenceCertificates,
		Award:        step.Award,
		Research:     step.Research,
	})
}

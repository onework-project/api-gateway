package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
)

// @Security Bearer
// @Router /applicants/blocked-companies/{id} [post]
// @Summary createApplicantBlockedCompany
// @Description Create Blocked Company By giving company "id"
// @Tags applicants_blocked_company
// @ID CreateApplicantBlockedCompany
// @Accept json
// @Produce json
// @Param id path int true "CompanyID"
// @Success 201 {object} models.ApplicantBlockedCompany
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) CreateApplicantBlockedCompany(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	blockedCompany, err := h.grpcClient.ApplicantBlockedCompanyService().Create(context.Background(), &pba.BlockedCompany{
		ApplicantId: payload.UserID,
		CompanyId:   id,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusCreated, parseApplicantBlockedCompany(blockedCompany))
}

// @Security Bearer
// @Router /applicants/blocked-companies/{id} [delete]
// @Summary deleteApplicantBlockedCompany
// @Description delete Applicant's blocked company by giving "id" not company id
// @Tags applicants_blocked_company
// @ID DeleteApplicantBlockedCompany
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseSuccess
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Failure 401 {object} models.ResponseError
func (h *handlerV1) DeleteApplicantBlockedCompany(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
	}
	payload, err := h.GetAuthPayload(ctx)
	if err != nil {
		errResponse(ctx, http.StatusUnauthorized, ErrUnauthorized)
		return
	}

	_, err = h.grpcClient.ApplicantBlockedCompanyService().Delete(context.Background(), &pba.DeleteOrGetReq{
		Id:      id,
		ThingId: payload.UserID,
	})
	if err != nil {
		errorHandler(err, ctx)
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseSuccess{
		Success: Deleted,
	})
}

// @Router /applicants/blocked-companies [get]
// @Summary getAllApplicantBlockedCompany
// @Description Get All Applicant's Blocked Company List By giving applicant "id"
// @Tags applicants_blocked_company
// @ID GetAllApplicantsBlockedCompanies
// @Accept json
// @Produce json
// @Param data query models.GetAllApplicantsThingsParams false "Data"
// @Success 200 {object} models.GetAllApplicantsBlockedCompanies
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
func (h *handlerV1) GetAllApplicantsBlockedCompanies(ctx *gin.Context) {
	param, err := validateGetAllApplicantParams(ctx)
	if err != nil {
		errResponse(ctx, http.StatusBadRequest, ErrBadRequest)
		return
	}
	blockedCompanies, err := h.grpcClient.ApplicantBlockedCompanyService().GetAll(context.Background(), &pba.GetAllParams{
		Limit:       param.Limit,
		Page:        param.Page,
		ApplicantId: param.ApplicantID,
	})
	if err != nil {
		errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
		return
	}
	response := models.GetAllApplicantsBlockedCompanies{
		BlockedCompanies: make([]*models.ApplicantBlockedCompany, 0),
		Count:            blockedCompanies.Count,
	}
	for _, v := range blockedCompanies.BlockedCompanies {
		s := parseApplicantBlockedCompany(v)
		response.BlockedCompanies = append(response.BlockedCompanies, &s)
	}

	ctx.JSON(http.StatusOK, response)
}

func parseApplicantBlockedCompany(blockedCompany *pba.BlockedCompany) models.ApplicantBlockedCompany {
	return models.ApplicantBlockedCompany{
		ID:          blockedCompany.Id,
		ApplicantID: blockedCompany.ApplicantId,
		CompanyID:   blockedCompany.CompanyId,
		CompanyInfo: models.CompanyInfo{
			Image:        blockedCompany.CompanyInfo.ImageUrl,
			CompanyName:  blockedCompany.CompanyInfo.CompanyName,
			Email:        blockedCompany.CompanyInfo.Email,
			PhoneNumber:  blockedCompany.CompanyInfo.PhoneNumber,
			Country:      blockedCompany.CompanyInfo.Country,
			City:         blockedCompany.CompanyInfo.RegionState,
			WorkersCount: blockedCompany.CompanyInfo.WorkersCount,
			Description:  blockedCompany.CompanyInfo.Description,
			CreatedAt:    blockedCompany.CompanyInfo.CreatedAt,
			Slug:         blockedCompany.CompanyInfo.Slug,
		},
		CreatedAt: blockedCompany.CreatedAt,
	}
}

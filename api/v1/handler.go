package v1

import (
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/gin-gonic/gin"
	"gitlab.com/onework-project/api-gateway/api/models"
	"gitlab.com/onework-project/api-gateway/config"
	grpcPkg "gitlab.com/onework-project/api-gateway/pkg/grpc_client"
	"gitlab.com/onework-project/api-gateway/pkg/logging"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	ErrWrongEmailOrPassword = errors.New("INVALID_EMAIL_OR_PASSWORD")
	ErrInternalServer       = errors.New("INTERNAL_SERVER_ERROR")
	ErrNotFound             = errors.New("NOT_FOUND")
	ErrUnknownUser          = errors.New("UNKNOWN_USER")
	ErrEmailExists          = errors.New("EMAIL_EXISTS")
	ErrMemberExists         = errors.New("MEMBER_IN_COMPANY")
	ErrIncorrectCode        = errors.New("INCORRECT_VERIFICATION_CODE")
	ErrCodeExpired          = errors.New("VERIFICATION_CODE_IS_EXPIRED")
	ErrForbidden            = errors.New("FORBIDDEN")
	ErrWeakPassword         = errors.New("WEAK_PASSWORD")
	ErrBadRequest           = errors.New("BAD_REQUEST")
	ErrFailedDowload        = errors.New("FAILED_TO_DOWNLOAD")
	ErrSize                 = errors.New("INVALID_SIZE")
	ErrFileType             = errors.New("WRONG_TYPE")
	ErrUnauthorized         = errors.New("UNAUTHORIZED")
	ErrMethodNotAllowed     = errors.New("METHOD_NOT_ALLOWED")
	ErrNotAllowed           = errors.New("NOT_ALLOWED")
)

var (
	Success = "SUCCESS"
	Deleted = "DELETED"
	Login   = "LOGIN"
)

const (
	RegisterCodeKey   = "register_code_"
	ForgotPasswordKey = "forgot_password_key_"
)

const (
	UserTypeApplicant  = "applicant"
	UserTypeMember     = "member"
	UserTypeSuperAdmin = "superadmin"
)

type handlerV1 struct {
	cfg        *config.Config
	grpcClient grpcPkg.GrpcClientI
	logger     *logging.Logger
	cfgAws     *s3.Client
	tracer     trace.Tracer
}

type HandlerV1Options struct {
	Cfg        *config.Config
	GrpcClient grpcPkg.GrpcClientI
	Logger     *logging.Logger
	CfgAws     *s3.Client
	Tracer     trace.Tracer
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:        options.Cfg,
		grpcClient: options.GrpcClient,
		logger:     options.Logger,
		cfgAws:     options.CfgAws,
		tracer:     options.Tracer,
	}
}

func errResponse(ctx *gin.Context, resp int, err error) {
	ctx.JSON(resp, models.ResponseError{
		Status: "fail",
		Error:  err.Error(),
	})
}

func setNull(s string) *string {
	if s != "" {
		return &s
	}
	return nil
}

func validateGetAllParams(ctx *gin.Context) (*models.GetAllParams, error) {
	var (
		limit int64 = 10
		page  int64 = 1
		err   error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllParams{
		Limit:  limit,
		Page:   page,
		Search: ctx.Query("search"),
	}, nil
}

func validateGetAllSessionsParams(ctx *gin.Context) (*models.GetAllSessionsParams, error) {
	var (
		limit int64 = 10
		page  int64 = 1
		err   error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllSessionsParams{
		Limit: limit,
		Page:  page,
		Email: ctx.Query("email"),
	}, nil
}

func validateGetAllApplicantsParams(ctx *gin.Context) (*models.GetAllApplicantsParams, error) {
	var (
		limit int64 = 10
		page  int64 = 1
		err   error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllApplicantsParams{
		Limit:      limit,
		Page:       page,
		Languages:  ctx.QueryArray("languages"),
		Educations: ctx.QueryArray("educations"),
		Licences:   ctx.QueryArray("licences"),
		Skills:     ctx.QueryArray("skills"),
	}, nil
}

func validateGetAllChatsMessages(ctx *gin.Context) (*models.GetAllChatMessagesParams, error) {
	var (
		limit  int64 = 10
		page   int64 = 1
		chatID int64
		err    error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("chat_id") != "" {
		chatID, err = strconv.ParseInt(ctx.Query("chat_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	return &models.GetAllChatMessagesParams{
		Limit:  limit,
		Page:   page,
		ChatID: chatID,
		Search: ctx.Query("search"),
	}, nil
}

func validateGetAllCompaniesSm(ctx *gin.Context) (*models.GetAllCompaniesSm, error) {
	var (
		limit     int64 = 10
		page      int64 = 1
		companyId int64
		err       error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("company_id") != "" {
		companyId, err = strconv.ParseInt(ctx.Query("company_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllCompaniesSm{
		Limit:     limit,
		Page:      page,
		CompanyID: companyId,
	}, nil
}

func validateGetAllCompaniesMembers(ctx *gin.Context) (*models.GetAllMembersReq, error) {
	var (
		limit     int64 = 10
		page      int64 = 1
		companyId int64
		err       error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("company_id") != "" {
		companyId, err = strconv.ParseInt(ctx.Query("company_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllMembersReq{
		Limit:     limit,
		Page:      page,
		CompanyID: companyId,
	}, nil
}

func validateGetAllChats(ctx *gin.Context) (*models.GetAllChatsParams, error) {
	var (
		limit  int64 = 10
		page   int64 = 1
		userId int64
		err    error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("user_id") != "" {
		userId, err = strconv.ParseInt(ctx.Query("user_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllChatsParams{
		Limit:  limit,
		Page:   page,
		UserID: userId,
	}, nil
}

func validateCompanyGetAllParams(ctx *gin.Context) (*models.GetAllCompaniesParams, error) {
	var (
		limit int64 = 10
		page  int64 = 1
		err   error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllCompaniesParams{
		Limit:       limit,
		Page:        page,
		CompanyName: ctx.Query("company_name"),
		Location:    ctx.Query("location"),
	}, nil
}

func validateGetAllApplicantParams(ctx *gin.Context) (*models.GetAllApplicantsThingsParams, error) {
	var (
		limit       int64 = 10
		page        int64 = 1
		applicantId int64
		err         error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("applicant_id") != "" {
		applicantId, err = strconv.ParseInt(ctx.Query("applicant_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllApplicantsThingsParams{
		Limit:       limit,
		Page:        page,
		ApplicantID: applicantId,
	}, nil
}

func validateGetAllVacanciesParams(ctx *gin.Context) (*models.GetAllVacanciesParams, error) {
	var (
		limit     int64 = 10
		page      int64 = 1
		vacancyId int64
		err       error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("vacancy_id") != "" {
		vacancyId, err = strconv.ParseInt(ctx.Query("vacancy_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllVacanciesParams{
		Limit:     limit,
		Page:      page,
		VacancyID: vacancyId,
	}, nil
}

func validateGetAllActiveVacancies(ctx *gin.Context) (*models.GetAllVacanciesParamsReq, error) {
	var (
		limit      int64 = 10
		page       int64 = 1
		experience int64 = -1
		companyId  int64
		err        error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		log.Println(err)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		log.Println(err)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("company_id") != "" {
		companyId, err = strconv.ParseInt(ctx.Query("company_id"), 10, 64)
		log.Println(err)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("years_of_experience") != "" {
		experience, err = strconv.ParseInt(ctx.Query("years_of_experience"), 10, 64)
		log.Println(err)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllVacanciesParamsReq{
		Limit:             limit,
		Page:              page,
		CompanyID:         companyId,
		Title:             ctx.Query("title"),
		EmploymentForm:    ctx.Query("employment_form"),
		Market:            ctx.Query("market"),
		EmploymentType:    ctx.Query("employment_type"),
		Location:          ctx.Query("location"),
		TimeRange:         ctx.Query("time_range"),
		YearsOfExperience: experience,
	}, nil
}

func validateGetAllBlockedApplParams(ctx *gin.Context) (*models.GetAllBlockedApplicantsParams, error) {
	var (
		limit     int64 = 10
		page      int64 = 1
		companyId int64
		err       error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("company_id") != "" {
		companyId, err = strconv.ParseInt(ctx.Query("company_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllBlockedApplicantsParams{
		Limit:     limit,
		Page:      page,
		CompanyID: companyId,
	}, nil
}

func validateGetAllApplicationsParams(ctx *gin.Context) (*models.GetAllApplicationsParams, error) {
	var (
		limit       int64 = 10
		page        int64 = 1
		applicantID int64
		vacancyID   int64
		err         error
	)
	if ctx.Query("limit") != "" {
		limit, err = strconv.ParseInt(ctx.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("page") != "" {
		page, err = strconv.ParseInt(ctx.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if ctx.Query("applicant_id") != "" {
		applicantID, err = strconv.ParseInt(ctx.Query("applicant_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if ctx.Query("vacancy_id") != "" {
		vacancyID, err = strconv.ParseInt(ctx.Query("vacancy_id"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllApplicationsParams{
		Limit:       limit,
		Page:        page,
		ApplicantID: applicantID,
		VacancyID:   vacancyID,
	}, nil
}

func validatePassword(password string) bool {
	var capitalLetter, smallLetter, number, symbol bool
	for i := 0; i < len(password); i++ {
		if password[i] >= 65 && password[i] <= 90 {
			capitalLetter = true
		} else if password[i] >= 97 && password[i] <= 122 {
			smallLetter = true
		} else if password[i] >= 48 && password[i] <= 57 {
			number = true
		} else {
			symbol = true
		}
	}
	return capitalLetter && smallLetter && number && symbol
}

func errorHandler(err error, ctx *gin.Context) {
	if v, ok := status.FromError(err); ok && v.Code() == codes.NotFound {
		errResponse(ctx, http.StatusNotFound, ErrNotFound)
		return
	}
	errResponse(ctx, http.StatusInternalServerError, ErrInternalServer)
}

func (h *handlerV1) CheckAll(res *models.ApplicationCheck, id int64, userID int64) {
	var (
		applicantExp int64
	)
	res.Languages.NotHaveLanguages = make(map[string]string)
	aLanguages, workExperiences, educations := h.GetApplicantInfo(userID)
	vacancy := h.GetVacancyInfo(id)
	// * -------- checking applicant and vacancy's languages -------
	for _, v := range vacancy.Languages {
		res.Languages.NotHaveLanguages[v.Language] = v.Level
	}
	for _, v := range aLanguages.Languages {
		delete(res.Languages.NotHaveLanguages, v.Language)
	}
	if len(res.Languages.NotHaveLanguages) == 0 {
		res.Languages.IsMatched = true
	}
	// * ------------- end checking languages --------------------
	// *
	// * -------- checking applicant and vacancy's education -------
	res.Education.RequiredEducation = setNull(vacancy.Education)
	for _, v := range educations.Educations {
		if v.DegreeLevel == vacancy.Education {
			res.Education.IsMatched = true
			res.Education.RequiredEducation = setNull(vacancy.Education)
			break
		}
	}
	// * --------- end check educations ----------
	// *
	// * -------- checking applicant and vacancy's experience -------
	for _, v := range workExperiences.WorkExps {
		if v.EndYear != "" {
			start, err := strconv.Atoi(v.StartYear)
			if err != nil {
				h.logger.WithError(err).Error("failed to parse start year")
			}
			end, err := strconv.Atoi(v.EndYear)
			if err != nil {
				h.logger.WithError(err).Error("failed to parse end year")
			}
			applicantExp += int64(end - start)
		} else {
			start, err := strconv.Atoi(v.StartYear)
			if err != nil {
				h.logger.WithError(err).Error("failed to parse start year")
			}
			applicantExp += int64(time.Now().Year() - start)
		}
	}
	res.Experience.ApplicantExperience = applicantExp
	res.Experience.VacancyExperience = vacancy.Experience
	if vacancy.Experience <= applicantExp {
		res.Experience.IsMatched = true
	}
	// * --------- end check experience ----------

	res.IsGood = res.Experience.IsMatched && res.Education.IsMatched && res.Languages.IsMatched
}

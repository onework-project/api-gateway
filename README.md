# Onework Api Gateway
<img src = "./logo/onework.svg" align="right" width="245">

## Summary

* [Description](#description)
* [Dependencies](#dependencies)
* [Setup](#setup)
* [Swagger](#swagger)
* [Running](#running)


## Description
The 'api-gateway' is responsible for request comes from an frontend, handle them and response to the request.

## Dependencies

[GoLang (v1.20.3)](https://golang.org/) - Go is an open source programming language that makes it easy to build simple, reliable and efficient software.

Loading and updating packages:
```bash
$ make tidy
```

Running project linter and checking [golangci-lint](https://golangci-lint.run/usage/install/):
```bash
$ make linter
```

## Setup

Pulling proto files:
```bash
$ make pull-sub-module
```

Generating proto files:
```bash
$ make proto-gen
```

## Environment Variables
```bash
$ touch ./.env
$ cp sample.env .env
```
Open .env file, fix it as writen

## Swagger

Generating documentation:
```bash
$ make swag-init
```

*Swagger UI:: http://localhost:8000/swagger/index.html*

## Running

Starting Project:
* Before running api-gateway, run all other microservices...
```bash
$ make run 
```
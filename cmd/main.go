package main

import (
	"context"
	"log"
	"strings"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"gitlab.com/onework-project/api-gateway/api"
	_ "gitlab.com/onework-project/api-gateway/api/docs"
	configEnv "gitlab.com/onework-project/api-gateway/config"
	grpcPkg "gitlab.com/onework-project/api-gateway/pkg/grpc_client"
	"gitlab.com/onework-project/api-gateway/pkg/jaeger"
	"gitlab.com/onework-project/api-gateway/pkg/logging"
	"go.opentelemetry.io/otel"
)

func main() {
	s := "https://onework-medias.s3.us-east-1.amazonaws.com/pdfs/1e3b679a-00d5-476d-a150-8ed54a3609b1.pdf"
	sl := strings.Split(s, "/")
	log.Println(sl)
	cfg := configEnv.Load(".")
	logging.Init()
	log := logging.GetLogger()

	// initialize aws  credentials
	creds := credentials.NewStaticCredentialsProvider(cfg.AwsAccessKeyID, cfg.AwsSecretAccessKey, "")

	cfgAws, err := config.LoadDefaultConfig(context.TODO(), config.WithCredentialsProvider(creds), config.WithRegion(cfg.AwsRegion))

	if err != nil {
		log.WithError(err).Fatal(err)
	}

	clientAws := s3.NewFromConfig(cfgAws)

	tp, err := jaeger.InitJaeger(&cfg)
	if err != nil {
		log.Fatal("cannot create tracer", err)
	}

	otel.SetTracerProvider(tp)
	tr := tp.Tracer("main-otel")
	log.Info("Opentracing connected")

	grpcConn, err := grpcPkg.New(&cfg, tp)
	if err != nil {
		log.Fatalf("failed to get grpc connettion: %v", err)
	}

	apiServer := api.New(&api.RoutetOptions{
		Cfg:        &cfg,
		GrpcClient: grpcConn,
		Logger:     &log,
		CfgAws:     clientAws,
		Tracer:     tr,
	})
	err = apiServer.Run(cfg.HttpPort)
	if err != nil {
		log.Fatalf("failed to run server: %v", err)
	}
}

FROM golang:1.19.1-alpine3.16 as builder

WORKDIR /onework

COPY . .


RUN go build -o main cmd/main.go

FROM alpine:3.16

WORKDIR /onework

COPY --from=builder /onework/main .

EXPOSE 8000

CMD [ "/onework/main" ]
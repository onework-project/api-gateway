package grpc_client

import (
	"fmt"

	"gitlab.com/onework-project/api-gateway/config"
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
	pbu "gitlab.com/onework-project/api-gateway/genproto/auth_service"
	pbm "gitlab.com/onework-project/api-gateway/genproto/chat_service"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
)

//go:generate mockgen -source ../../genproto/user_service/user_service_grpc.pb.go -package mock_grpc -destination ./mock_grpc/user_service_grpc.gen.go
//go:generate mockgen -source ../../genproto/user_service/auth_service_grpc.pb.go -package mock_grpc -destination ./mock_grpc/auth_service_grpc.gen.go

type GrpcClientI interface {
	// * auth service
	UserService() pbu.UserServiceClient
	SetUserService(u pbu.UserServiceClient)
	AuthService() pbu.AuthServiceClient
	SetAuthService(a pbu.AuthServiceClient)
	SessionService() pbu.SessionServiceClient
	SetSessionService(a pbu.SessionServiceClient)

	// * applicant service
	ApplicantService() pba.ApplicantServiceClient
	SetApplicantService(a pba.ApplicantServiceClient)
	ApplicantSocialMediaService() pba.SocialMediaServiceClient
	SetApplicantSocialMediaService(a pba.SocialMediaServiceClient)
	ApplicantSkillService() pba.SkillServiceClient
	SetApplicantSkillService(a pba.SkillServiceClient)
	ApplicantLanguageService() pba.LanguageServiceClient
	SetApplicantLanguageService(a pba.LanguageServiceClient)
	ApplicantAcademicService() pba.AcademicServiceClient
	SetApplicantAcademicService(a pba.AcademicServiceClient)
	ApplicantAwardService() pba.AwardServiceClient
	SetApplicantAwardService(a pba.AwardServiceClient)
	ApplicantEducationService() pba.EducationServiceClient
	SetApplicantEducationService(a pba.EducationServiceClient)
	ApplicantLicenceService() pba.LicenseServiceClient
	SetApplicantLicenceService(a pba.LicenseServiceClient)
	ApplicantResearchService() pba.ResearchServiceClient
	SetApplicantResearchService(a pba.ResearchServiceClient)
	ApplicantBlockedCompanyService() pba.BlockedCompanyServiceClient
	SetApplicantBlockedCompanyService(a pba.BlockedCompanyServiceClient)
	ApplicantSavedJobService() pba.SavedJobServiceClient
	SetApplicantSavedJobService(a pba.SavedJobServiceClient)
	ApplicantWorkExperienceService() pba.WorkExpServiceClient
	SetApplicantWorkExperienceService(a pba.WorkExpServiceClient)
	ApplicantStepService() pba.StepServiceClient
	SetApplicantStepService(a pba.StepServiceClient)

	// *company service
	CompanyService() pbc.CompanyServiceClient
	SetCompanyService(c pbc.CompanyServiceClient)
	CompanyMemberService() pbc.CompanyMemberServiceClient
	SetCompanyMemberService(c pbc.CompanyMemberServiceClient)
	CompanyLanguageService() pbc.CLanguageServiceClient
	SetCompanyLanguageService(c pbc.CLanguageServiceClient)
	CompanySkillService() pbc.ComSkillServiceClient
	SetCompanySkillService(c pbc.ComSkillServiceClient)
	CompanyMarketService() pbc.MarketServiceClient
	SetCompanyMarketService(c pbc.MarketServiceClient)
	CompanySocialMediaService() pbc.SMServiceClient
	SetCompanySocialMediaService(c pbc.SMServiceClient)
	CompanyVacancyService() pbc.VacancyServiceClient
	SetCompanyVacancyService(c pbc.VacancyServiceClient)
	CompanyApplicationService() pbc.ApplicationServiceClient
	SetCompanyApplicationService(c pbc.ApplicationServiceClient)
	CompanyBlockedApplicantService() pbc.BlockedApplicantServiceClient
	SetCompanyBlockedApplicantService(c pbc.BlockedApplicantServiceClient)

	// * chat service
	MessageService() pbm.ChatMessageServiceClient
	ChatService() pbm.ChatServiceClient
}

type GrpcClient struct {
	cfg         *config.Config
	connections map[string]interface{}
}

func New(cfg *config.Config, tp trace.TracerProvider) (GrpcClientI, error) {
	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.AuthServiceHost, cfg.AuthServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connAuthService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.AuthServiceHost, cfg.AuthServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connSessionService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.AuthServiceHost, cfg.AuthServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(
			otelgrpc.UnaryClientInterceptor(
				otelgrpc.WithTracerProvider(tp),
				otelgrpc.WithPropagators(propagation.NewCompositeTextMapPropagator(propagation.Baggage{}, propagation.TraceContext{})),
			),
		),
		grpc.WithStreamInterceptor(
			otelgrpc.StreamClientInterceptor(
				otelgrpc.WithTracerProvider(tp),
				otelgrpc.WithPropagators(propagation.NewCompositeTextMapPropagator(propagation.Baggage{}, propagation.TraceContext{})),
			),
		),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantSocialMediaService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantSkillService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantLanguageService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantAcademicService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantAwardService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantEducationService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantLicenceService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantResearchService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantBlockedCompanyService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantSavedJobService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantWorkExperienceService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connApplicantStepService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connCompanyService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connCompanyLanguageService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connCompanyMarketService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connCompanySkillService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connCompanySocialMediaService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connCompanyVacancyService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connCompanyBlockedApplicantService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connCompanyApplicationService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connCompanyMemberService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}

	// * chat service
	connChatMessageService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ChatServiceHost, cfg.ChatServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("chat service dial host: %s port:%s err: %v",
			cfg.ChatServiceHost, cfg.ChatServiceGrpcPort, err)
	}
	connChatService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ChatServiceHost, cfg.ChatServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("chat service dial host: %s port:%s err: %v",
			cfg.ChatServiceHost, cfg.ChatServiceGrpcPort, err)
	}
	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service":                      pbu.NewUserServiceClient(connUserService),
			"auth_service":                      pbu.NewAuthServiceClient(connAuthService),
			"session_service":                   pbu.NewSessionServiceClient(connSessionService),
			"applicant_service":                 pba.NewApplicantServiceClient(connApplicantService),
			"applicant_social_media_service":    pba.NewSocialMediaServiceClient(connApplicantSocialMediaService),
			"applicant_skill_service":           pba.NewSkillServiceClient(connApplicantSkillService),
			"applicant_language_service":        pba.NewLanguageServiceClient(connApplicantLanguageService),
			"applicant_academic_service":        pba.NewAcademicServiceClient(connApplicantAcademicService),
			"applicant_award_service":           pba.NewAwardServiceClient(connApplicantAwardService),
			"applicant_education_service":       pba.NewEducationServiceClient(connApplicantEducationService),
			"applicant_licence_service":         pba.NewLicenseServiceClient(connApplicantLicenceService),
			"applicant_research_service":        pba.NewResearchServiceClient(connApplicantResearchService),
			"applicant_blocked_company_service": pba.NewBlockedCompanyServiceClient(connApplicantBlockedCompanyService),
			"applicant_saved_job_service":       pba.NewSavedJobServiceClient(connApplicantSavedJobService),
			"applicant_work_experience_service": pba.NewWorkExpServiceClient(connApplicantWorkExperienceService),
			"applicant_step_service":            pba.NewStepServiceClient(connApplicantStepService),
			"company_service":                   pbc.NewCompanyServiceClient(connCompanyService),
			"company_member_service":            pbc.NewCompanyMemberServiceClient(connCompanyMemberService),
			"company_language_service":          pbc.NewCLanguageServiceClient(connCompanyLanguageService),
			"company_skill_service":             pbc.NewComSkillServiceClient(connCompanySkillService),
			"company_market_service":            pbc.NewMarketServiceClient(connCompanyMarketService),
			"company_social_media_service":      pbc.NewSMServiceClient(connCompanySocialMediaService),
			"company_vacancy_service":           pbc.NewVacancyServiceClient(connCompanyVacancyService),
			"company_application_service":       pbc.NewApplicationServiceClient(connCompanyApplicationService),
			"company_blocked_applicant_service": pbc.NewBlockedApplicantServiceClient(connCompanyBlockedApplicantService),
			"message_service":                   pbm.NewChatMessageServiceClient(connChatMessageService),
			"chat_service":                      pbm.NewChatServiceClient(connChatService),
		},
	}, nil
}

func (g *GrpcClient) UserService() pbu.UserServiceClient {
	return g.connections["user_service"].(pbu.UserServiceClient)
}

func (g *GrpcClient) AuthService() pbu.AuthServiceClient {
	return g.connections["auth_service"].(pbu.AuthServiceClient)
}

func (g *GrpcClient) SessionService() pbu.SessionServiceClient {
	return g.connections["session_service"].(pbu.SessionServiceClient)
}

func (g *GrpcClient) ApplicantService() pba.ApplicantServiceClient {
	return g.connections["applicant_service"].(pba.ApplicantServiceClient)
}

func (g *GrpcClient) ApplicantSocialMediaService() pba.SocialMediaServiceClient {
	return g.connections["applicant_social_media_service"].(pba.SocialMediaServiceClient)
}

func (g *GrpcClient) ApplicantSkillService() pba.SkillServiceClient {
	return g.connections["applicant_skill_service"].(pba.SkillServiceClient)
}

func (g *GrpcClient) ApplicantLanguageService() pba.LanguageServiceClient {
	return g.connections["applicant_language_service"].(pba.LanguageServiceClient)
}

func (g *GrpcClient) ApplicantAcademicService() pba.AcademicServiceClient {
	return g.connections["applicant_academic_service"].(pba.AcademicServiceClient)
}

func (g *GrpcClient) ApplicantAwardService() pba.AwardServiceClient {
	return g.connections["applicant_award_service"].(pba.AwardServiceClient)
}

func (g *GrpcClient) ApplicantEducationService() pba.EducationServiceClient {
	return g.connections["applicant_education_service"].(pba.EducationServiceClient)
}

func (g *GrpcClient) ApplicantLicenceService() pba.LicenseServiceClient {
	return g.connections["applicant_licence_service"].(pba.LicenseServiceClient)
}

func (g *GrpcClient) ApplicantResearchService() pba.ResearchServiceClient {
	return g.connections["applicant_research_service"].(pba.ResearchServiceClient)
}

func (g *GrpcClient) ApplicantBlockedCompanyService() pba.BlockedCompanyServiceClient {
	return g.connections["applicant_blocked_company_service"].(pba.BlockedCompanyServiceClient)
}

func (g *GrpcClient) ApplicantSavedJobService() pba.SavedJobServiceClient {
	return g.connections["applicant_saved_job_service"].(pba.SavedJobServiceClient)
}

func (g *GrpcClient) ApplicantWorkExperienceService() pba.WorkExpServiceClient {
	return g.connections["applicant_work_experience_service"].(pba.WorkExpServiceClient)
}

func (g *GrpcClient) ApplicantStepService() pba.StepServiceClient {
	return g.connections["applicant_step_service"].(pba.StepServiceClient)
}

func (g *GrpcClient) CompanyService() pbc.CompanyServiceClient {
	return g.connections["company_service"].(pbc.CompanyServiceClient)
}

func (g *GrpcClient) CompanyMemberService() pbc.CompanyMemberServiceClient {
	return g.connections["company_member_service"].(pbc.CompanyMemberServiceClient)
}

func (g *GrpcClient) CompanyLanguageService() pbc.CLanguageServiceClient {
	return g.connections["company_language_service"].(pbc.CLanguageServiceClient)
}

func (g *GrpcClient) CompanySkillService() pbc.ComSkillServiceClient {
	return g.connections["company_skill_service"].(pbc.ComSkillServiceClient)
}

func (g *GrpcClient) CompanyMarketService() pbc.MarketServiceClient {
	return g.connections["company_market_service"].(pbc.MarketServiceClient)
}

func (g *GrpcClient) CompanySocialMediaService() pbc.SMServiceClient {
	return g.connections["company_social_media_service"].(pbc.SMServiceClient)
}

func (g *GrpcClient) CompanyVacancyService() pbc.VacancyServiceClient {
	return g.connections["company_vacancy_service"].(pbc.VacancyServiceClient)
}

func (g *GrpcClient) CompanyApplicationService() pbc.ApplicationServiceClient {
	return g.connections["company_application_service"].(pbc.ApplicationServiceClient)
}

func (g *GrpcClient) CompanyBlockedApplicantService() pbc.BlockedApplicantServiceClient {
	return g.connections["company_blocked_applicant_service"].(pbc.BlockedApplicantServiceClient)
}

func (g *GrpcClient) MessageService() pbm.ChatMessageServiceClient {
	return g.connections["message_service"].(pbm.ChatMessageServiceClient)
}

func (g *GrpcClient) ChatService() pbm.ChatServiceClient {
	return g.connections["chat_service"].(pbm.ChatServiceClient)
}

package grpc_client

import (
	pba "gitlab.com/onework-project/api-gateway/genproto/applicant_service"
	pbu "gitlab.com/onework-project/api-gateway/genproto/auth_service"
	pbc "gitlab.com/onework-project/api-gateway/genproto/company_service"
)

func (g *GrpcClient) SetUserService(u pbu.UserServiceClient) {
	g.connections["user_service"] = u
}

func (g *GrpcClient) SetAuthService(u pbu.AuthServiceClient) {
	g.connections["auth_service"] = u
}

func (g *GrpcClient) SetSessionService(s pbu.SessionServiceClient) {
	g.connections["session_service"] = s
}

func (g *GrpcClient) SetApplicantService(a pba.ApplicantServiceClient) {
	g.connections["applicant_service"] = a
}

func (g *GrpcClient) SetApplicantSocialMediaService(a pba.SocialMediaServiceClient) {
	g.connections["applicant_social_media_service"] = a
}

func (g *GrpcClient) SetApplicantSkillService(s pba.SkillServiceClient) {
	g.connections["applicant_skill_service"] = s
}

func (g *GrpcClient) SetApplicantAcademicService(s pba.AcademicServiceClient) {
	g.connections["applicant_skill_service"] = s
}

func (g *GrpcClient) SetApplicantLanguageService(s pba.LanguageServiceClient) {
	g.connections["applicant_language_service"] = s
}

func (g *GrpcClient) SetApplicantAwardService(s pba.AwardServiceClient) {
	g.connections["applicant_award_service"] = s
}

func (g *GrpcClient) SetApplicantEducationService(a pba.EducationServiceClient) {
	g.connections["applicant_education_service"] = a
}

func (g *GrpcClient) SetApplicantLicenceService(a pba.LicenseServiceClient) {
	g.connections["applicant_licence_service"] = a
}

func (g *GrpcClient) SetApplicantResearchService(a pba.ResearchServiceClient) {
	g.connections["applicant_research_service"] = a
}

func (g *GrpcClient) SetApplicantBlockedCompanyService(a pba.BlockedCompanyServiceClient) {
	g.connections["applicant_blocked_company_service"] = a
}

func (g *GrpcClient) SetApplicantSavedJobService(a pba.SavedJobServiceClient) {
	g.connections["applicant_saved_job_service"] = a
}

func (g *GrpcClient) SetApplicantWorkExperienceService(a pba.WorkExpServiceClient) {
	g.connections["applicant_work_experience_service"] = a
}

func (g *GrpcClient) SetApplicantStepService(a pba.StepServiceClient) {
	g.connections["applicant_step_service"] = a
}

func (g *GrpcClient) SetCompanyService(c pbc.CompanyServiceClient) {
	g.connections["company_service"] = c
}

func (g *GrpcClient) SetCompanyMemberService(c pbc.CompanyMemberServiceClient) {
	g.connections["company_member_service"] = c
}

func (g *GrpcClient) SetCompanyLanguageService(c pbc.CLanguageServiceClient) {
	g.connections["company_language_service"] = c
}

func (g *GrpcClient) SetCompanySkillService(c pbc.ComSkillServiceClient) {
	g.connections["company_skill_service"] = c
}

func (g *GrpcClient) SetCompanyMarketService(c pbc.MarketServiceClient) {
	g.connections["company_market_service"] = c
}

func (g *GrpcClient) SetCompanySocialMediaService(c pbc.SMServiceClient) {
	g.connections["company_social_media_service"] = c
}

func (g *GrpcClient) SetCompanyVacancyService(c pbc.VacancyServiceClient) {
	g.connections["company_vacancy_service"] = c
}

func (g *GrpcClient) SetCompanyApplicationService(c pbc.ApplicationServiceClient) {
	g.connections["company_application_service"] = c
}

func (g *GrpcClient) SetCompanyBlockedApplicantService(c pbc.BlockedApplicantServiceClient) {
	g.connections["company_application_service"] = c
}

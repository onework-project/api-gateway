package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	HttpPort                 string
	AwsRegion                string
	AwsAccessKeyID           string
	AwsSecretAccessKey       string
	AwsBucketOnework         string
	AwsBucketFileImages      string
	AwsBucketFilePdfs        string
	ApplicantServiceGrpcPort string
	ApplicantServiceHost     string
	CompanyServiceGrpcPort   string
	CompanyServiceHost       string
	AuthServiceGrpcPort      string
	AuthServiceHost          string
	ChatServiceGrpcPort      string
	ChatServiceHost          string
	Jaeger                   Jaeger
}

type Jaeger struct {
	Host        string
	ServiceName string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		HttpPort:                 conf.GetString("HTTP_PORT"),
		AwsRegion:                conf.GetString("AWS_REGION"),
		AwsAccessKeyID:           conf.GetString("AWS_ACCESS_KEY_ID"),
		AwsSecretAccessKey:       conf.GetString("AWS_SECRET_ACCESS_KEY"),
		AwsBucketOnework:         conf.GetString("AWS_BUCKET_1"),
		AwsBucketFileImages:      conf.GetString("AWS_BUCKET_FILE_IMAGES"),
		AwsBucketFilePdfs:        conf.GetString("AWS_BUCKET_FILE_PDFS"),
		ApplicantServiceGrpcPort: conf.GetString("APPLICANT_SERVICE_GRPC_PORT"),
		ApplicantServiceHost:     conf.GetString("APPLICANT_SERVICE_HOST"),
		CompanyServiceGrpcPort:   conf.GetString("COMPANY_SERVICE_GRPC_PORT"),
		CompanyServiceHost:       conf.GetString("COMPANY_SERVICE_HOST"),
		AuthServiceGrpcPort:      conf.GetString("AUTH_SERVICE_GRPC_PORT"),
		AuthServiceHost:          conf.GetString("AUTH_SERVICE_HOST"),
		ChatServiceHost:          conf.GetString("CHAT_SERVICE_HOST"),
		ChatServiceGrpcPort:      conf.GetString("CHAT_SERVICE_GRPC_PORT"),
		Jaeger: Jaeger{
			Host:        conf.GetString("JAEGER_HOST"),
			ServiceName: conf.GetString("JAEGER_SERVICE_NAME"),
		},
	}
	return cfg
}
